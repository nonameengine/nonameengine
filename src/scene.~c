#include <stdlib.h>
#include <stdio.h>
#include <math.h>

#include "system/config.h"
#include "system/settings.h"

#include "collision.h"
#include "flying_camera.h"
#include "scene.h"
#include "visual.h"
#include "qtree.h"

#include "main.h"

Map *Scene = 0;

float draw_distance, progressive_min, progressive_max;

QTree *objects_qtree;
QTree *collision_qtree;

unsigned objects_to_render;
Object **render_queue;

int test_collision(CollisionObject *object1, CollisionObject *object2)
{
    if(object1->type == COLLISION_BOX)
    {
        if(object2->type == COLLISION_SPHERE)
        {
            if(BBoxIntersectBSphere(object1->bbox,object2->bsphere) != OUTSIDE)
                return 1;
        }
        else
        {
            if(BBoxIntersectBBox(object1->bbox,object2->bbox) != OUTSIDE)
                return 1;
        }
    }
    else
    {
        if(object2->type == COLLISION_SPHERE)
        {
            if(BSphereIntersectBSphere(object1->bsphere,object2->bsphere) != OUTSIDE)    
                return 1;
        }
        else
        {
            if(BBoxIntersectBSphere(object2->bbox,object1->bsphere) != OUTSIDE)            
                return 1;
        }
    }

    return 0;
}

const CollisionObject *walk_collision_qtree(QTree *node, CollisionObject *o)
{
    int i;

    if(o->type == COLLISION_SPHERE)
    {
        if(BBoxIntersectBSphere(node->bbox, o->bsphere) == OUTSIDE)
            return 0;
    }
    else
    {
        if(BBoxIntersectBBox(node->bbox, o->bbox) == OUTSIDE)
            return 0;
    }
    
    if(node->type == QTREE_LEAF)
    {    
        for(i = 0; i < node->object_count; i++)
        {
            if(test_collision(node->collision_objects[i],o) != 0)
                return node->collision_objects[i];
        }

        return 0;
    }

    for(i = 0; i < 4; i++)
    {
        const CollisionObject *obj = walk_collision_qtree(&node->branches[i],o);

        if(obj)
            return obj;
    }

    return 0;
}

const CollisionObject *TestCollision(CollisionObject o)
{
    DynamicObject *d;

    if(collision_qtree)
    {
        const CollisionObject *obj = walk_collision_qtree(collision_qtree,&o);

        if(obj)
            return obj;
    }

    d = Scene->dynamics;

    while(d)
    {
        if(d->collision.type != COLLISION_NONE)
        {
            if(test_collision(&d->collision,&o) != 0)
                return &d->collision;
        }

        d = d->next;
    }

    return 0;
}

void UpdateObjectBounding(Object *o)
{
    Matrix4x4 mv;
    float radius;

    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();

    glTranslatef(o->position.x,o->position.y,o->position.z);
    glRotatef(o->rotation.x,1.0,0.0,0.0);
    glRotatef(o->rotation.y,0.0,1.0,0.0);
    glRotatef(o->rotation.z,0.0,0.0,1.0);
    glScalef(o->scale.x,o->scale.y,o->scale.z);

    glGetFloatv(GL_MODELVIEW_MATRIX,(float*)&mv);

    /*sphere*/
    o->bsphere.center = Vector3f_transform(GetVisual(o->model)->bsphere.center,mv);

    radius = GetVisual(o->model)->bsphere.radius;

    if(o->scale.x > o->scale.y && o->scale.x > o->scale.z)
        radius *= o->scale.x;
    else if(o->scale.y > o->scale.z)
        radius *= o->scale.y;
    else
        radius *= o->scale.z;

    o->bsphere.radius = radius;

    /*box*/
    o->bbox = TransformBBox(mv,GetVisual(o->model)->bbox);
}

int sort_visuals(const void *p1, const void *p2)
{
    const Object *o1 = *(const Object**)p1;
    const Object *o2 = *(const Object**)p2;

    /* if model uses only one texture, this may reduce calls to glTexImage2D */
    if(o1->model > o2->model)
        return 1;
    if(o1->model < o2->model)
        return -1;

    return 0;
}

int sort_blending(const void *p1, const void *p2)
{
    const Object *o1 = *(const Object**)p1;
    const Object *o2 = *(const Object**)p2;
    Vector3f p;
    
    float dist1, dist2;

    p = Vector3f_normalize(Vector3f_sub(Camera.position,o1->bsphere.center));
    p = Vector3f_add(Vector3f_muln(p,-o1->bsphere.radius),o1->bsphere.center);

    dist1 = Vector3f_distance_to(Camera.position,p);

    p = Vector3f_normalize(Vector3f_sub(Camera.position,o2->bsphere.center));
    p = Vector3f_add(Vector3f_muln(p,-o2->bsphere.radius),o2->bsphere.center);

    dist2 = Vector3f_distance_to(Camera.position,p);

    if(dist1 < dist2)
        return 1;
    if(dist1 > dist2)
        return -1;
    
    return 0;
}

void render_object(Object *o, int stage)
{
    Visual *v;
    float min, max;
    float dist, quality;

    v = ((ResourceModel*)o->model->data)->visual;

    if(!v->surfaces)
        return;

    glMatrixMode(GL_MODELVIEW);
    glPushMatrix();
    
    glTranslatef(o->position.x,o->position.y,o->position.z);
    glRotatef(o->rotation.x,1.0,0.0,0.0);
    glRotatef(o->rotation.y,0.0,1.0,0.0);
    glRotatef(o->rotation.z,0.0,0.0,1.0);
    glScalef(o->scale.x,o->scale.y,o->scale.z);

    min = o->bsphere.radius * progressive_min;
    max = o->bsphere.radius * progressive_max;

    dist = Vector3f_distance_to(Camera.position,
    Vector3f_add(o->position,v->bsphere.center)) - min;
    
    /*clamp*/
    if(dist < 0) dist = 0;
    if(dist > max-min) dist = max-min;

    quality = dist / (max-min);

    DrawVisual(v,quality,stage);

    glPopMatrix();
}

void render_all(int stage)
{
    Object *o;
    unsigned i;
    int j, l;
    
    for(i = 0; i < objects_to_render; i++)
    {
        o = render_queue[i];

        if(!o->model)
            continue;

        /* select lights */
        if(Scene->light_count)
        {
            l = 0;
            
            for(j = 0; j < 8; j++)
                glDisable(GL_LIGHT0+j);
            
            for(j = 0; j < 8 && l < Scene->light_count; l++)
            {
                BoundingSphere bs;
    
                bs.center = Vector4f_xyz(Scene->lights[l].position);
                bs.radius = Scene->lights[l].range;
    
                if(BBoxIntersectBSphere(o->bbox,bs) == OUTSIDE)
                    continue;
                
                glEnable(GL_LIGHT0+j);
    
                glLightfv(GL_LIGHT0+j,GL_AMBIENT,(float*)&Scene->lights[l].ambient);
                glLightfv(GL_LIGHT0+j,GL_DIFFUSE,(float*)&Scene->lights[l].diffuse);
                glLightfv(GL_LIGHT0+j,GL_SPECULAR,(float*)&Scene->lights[l].specular);
                glLightfv(GL_LIGHT0+j,GL_POSITION,(float*)&Scene->lights[l].position);
    
                glLightf(GL_LIGHT0+j,GL_CONSTANT_ATTENUATION,Scene->lights[l].constant_attenuation);
                glLightf(GL_LIGHT0+j,GL_LINEAR_ATTENUATION,Scene->lights[l].linear_attenuation);
                glLightf(GL_LIGHT0+j,GL_QUADRATIC_ATTENUATION,Scene->lights[l].quadratic_attenuation);
/*    
                glLightfv(GL_LIGHT0+j,GL_SPOT_DIRECTION,(float*)&Scene->lights[i].spot_direction);
                glLightf(GL_LIGHT0+j,GL_SPOT_EXPONENT,Scene->lights[i].spot_exponent);
                glLightf(GL_LIGHT0+j,GL_SPOT_CUTOFF,Scene->lights[i].spot_cutoff);
*/    
                j++;
            }        
  
            render_object(o, stage);
        }
        else
            render_object(o, stage);        
    }
}

int LoadScene(char *file_name)
{
    Config *cfg;
    Config *array, *cur;
    int i;

    jsval rval;

    /* maybe create special function for setting this values? */
    draw_distance = GetFloatSetting("draw_distance",180);
    progressive_min = GetFloatSetting("progressive_min",2);
    progressive_max = GetFloatSetting("progressive_max",5);

    if(Scene != 0)
        FreeScene();

    cfg = LoadConfig(file_name);

    if(!cfg)
    {
        printf("! Can't load map config '%s'\n",file_name);
        return 1;
    }

    Scene = malloc(sizeof(Map));

    Scene->exit.min = Vector3f_set(0,0,0);
    Scene->exit.max = Vector3f_set(0,0,0);
    
    array = GetConfigObjectItem(cfg,"exit");

    if(array)
    {
        Config *pos, *scale;
        Vector3f p, s;

        pos = GetConfigObjectItem(array,"position");
        scale = GetConfigObjectItem(array,"scale");

        if(pos && scale)
        {
            p = GetConfigVector3f(pos);
            s = GetConfigVector3f(scale);

            Scene->exit.min = Vector3f_sub(p,s);
            Scene->exit.max = Vector3f_add(p,s);
        }
    }

    array = GetConfigObjectItem(cfg,"static_meshes");

    if(!array)
    {
        printf("! Can't find 'static_meshes' section in map config '%s'\n",file_name);
        DeleteConfig(cfg);

        return 1;
    }

    objects_qtree = CreateQTree(650.f,3);

    Scene->object_count = GetConfigArraySize(array);
    Scene->objects = malloc(sizeof(Object) * Scene->object_count);
    cur = array->child;

    for(i = 0; i < Scene->object_count;)
    {
        Config *pos,*rot,*scale,*model;

        pos = GetConfigObjectItem(cur,"position");
        rot = GetConfigObjectItem(cur,"rotation");
        scale = GetConfigObjectItem(cur,"scale");
        model = GetConfigObjectItem(cur,"model");

        if(!pos || !rot || !scale || !model)
        {
            printf("Invalid static object '%s' description\n",cur->name ? cur->name : "(none)");
            Scene->object_count--;
            cur = cur->next;
            continue;
        }

        Scene->objects[i].position = GetConfigVector3f(pos);
        Scene->objects[i].rotation = GetConfigVector3f(rot);        
        Scene->objects[i].scale = GetConfigVector3f(scale);

        Scene->objects[i].model = GetResource(model->valuestring,RT_MODEL);

        UpdateObjectBounding(&Scene->objects[i]);
        AddStaticObjectToQTree(objects_qtree,&Scene->objects[i]);
        
        cur = cur->next;
        i++;
    }

    render_queue = malloc(sizeof(Object*) * RENDER_QUEUE_CAPACITY);

    array = GetConfigObjectItem(cfg,"collision");

    if(!array)
    {
        Scene->collision = 0;
        Scene->collision_count = 0;
    }
    else
    {
        Scene->collision_count = GetConfigArraySize(array);
        Scene->collision = malloc(sizeof(CollisionObject) * Scene->collision_count);

        collision_qtree = CreateQTree(650.f,3);
    
        cur = array->child;
    
        for(i = 0; i < Scene->collision_count;)
        {
            Vector3f vp, vs;
            Config *type, *position, *scale;
    
            type = GetConfigObjectItem(cur,"type");
            position = GetConfigObjectItem(cur,"position");
            scale = GetConfigObjectItem(cur,"scale");
    
            if(!type || !position || !scale)
            {
                printf("Invalid collision object description!\n");
                Scene->collision_count--;
                cur = cur->next;
                continue;
            }
    
            vp = GetConfigVector3f(position);
            vs = GetConfigVector3f(scale);

            Scene->collision[i].type = COLLISION_NONE;
    
            if(type->valueint == 0)
                Scene->collision[i].type = COLLISION_BOX;
            if(type->valueint == 1)
                Scene->collision[i].type = COLLISION_SPHERE;
    
            if(type->valueint == 0)
            {
                Scene->collision[i].bbox.min = Vector3f_sub(vp,Vector3f_divn(vs,2));
                Scene->collision[i].bbox.max = Vector3f_add(vp,Vector3f_divn(vs,2));
            }
            if(type->valueint == 1)
            {
                Scene->collision[i].bsphere.center = vp;
                Scene->collision[i].bsphere.radius = vs.x > vs.y && vs.x > vs.z ? vs.x : vs.y > vs.z ? vs.y : vs.z;
            }

            AddCollisionObjectToQTree(collision_qtree,&Scene->collision[i]);
    
            cur = cur->next;
            i++;
        }
    }
    
    array = GetConfigObjectItem(cfg,"lights");

    if(!array)
    {
        Scene->light_count = 0;
        Scene->lights = 0;
    }
    else
    {
        Scene->light_count = GetConfigArraySize(array);
        Scene->lights = malloc(sizeof(Light) * Scene->light_count);

        cur = array->child;

        for(i = 0; i < Scene->light_count;)
        {
            Config *ambient, *diffuse, *specular, *position;
            Config *constant_attenuation, *linear_attenuation, *quadratic_attenuation;

            Config *spot_direction, *spot_exponent, *spot_cutoff;
            
            Config *range, *brightness;

            ambient = GetConfigObjectItem(cur,"ambient");
            diffuse = GetConfigObjectItem(cur,"diffuse");
            specular = GetConfigObjectItem(cur,"specular");
            position = GetConfigObjectItem(cur,"position");

            constant_attenuation = GetConfigObjectItem(cur,"constant_attenuation");
            linear_attenuation = GetConfigObjectItem(cur,"linear_attenuation");
            quadratic_attenuation = GetConfigObjectItem(cur,"quadratic_attenuation");

            spot_direction = GetConfigObjectItem(cur,"spot_direction");
            spot_exponent = GetConfigObjectItem(cur,"spot_exponent");
            spot_cutoff = GetConfigObjectItem(cur,"spot_cutoff");

            range = GetConfigObjectItem(cur,"range");
            brightness = GetConfigObjectItem(cur,"brightness");

            if(!ambient || !diffuse || !specular || !position ||
            !constant_attenuation || !linear_attenuation || !quadratic_attenuation || !range)
            {
                printf("Invalid light '%s' description!\n",cur->name ? cur->name : "(none)");
                Scene->light_count--;
                cur = cur->next;
                continue;
            }

            Scene->lights[i].ambient = GetConfigVector4f(ambient);
            Scene->lights[i].diffuse = GetConfigVector4f(diffuse);
            Scene->lights[i].specular = GetConfigVector4f(specular);
            Scene->lights[i].position = GetConfigVector4f(position);

            Scene->lights[i].constant_attenuation = constant_attenuation->valuefloat;
            Scene->lights[i].linear_attenuation = linear_attenuation->valuefloat;
            Scene->lights[i].quadratic_attenuation = quadratic_attenuation->valuefloat;

            Scene->lights[i].range = range->valuefloat;

            if(brightness)
                Scene->lights[i].diffuse = Vector4f_muln(Scene->lights[i].diffuse,
                brightness->valuefloat);

            if(spot_direction)
                Scene->lights[i].spot_direction = GetConfigVector3f(spot_direction);
            else
                Scene->lights[i].spot_direction = Vector3f_set(0,0,-1);
                
            if(spot_exponent)
                Scene->lights[i].spot_exponent = spot_exponent->valuefloat;
            else
                Scene->lights[i].spot_exponent = 0.0f;
                
            if(spot_cutoff)
                Scene->lights[i].spot_cutoff = spot_cutoff->valuefloat;
            else
                Scene->lights[i].spot_cutoff = 180.0f;

            cur = cur->next;
            i++;
        }
    }

    Scene->dynamics = 0;

    array = GetConfigObjectItem(cfg,"script_file");

    if(array && array->valuestring)
    {
        Scene->script_object = JS_DefineObject(
        JS_ScriptContext,JS_GlobalObject,"level",
        0,0,0);

        JS_AddRoot(JS_ScriptContext,&Scene->script_object);
        ExecuteScript(Scene->script_object,array->valuestring);
    }
    else
        Scene->script_object = 0;  

    DeleteConfig(cfg);

    if(Scene->script_object)
        JS_CallFunctionName(JS_ScriptContext,Scene->script_object,
        "on_load",0,0,&rval);

    return 0;
}

void DrawScene(void)
{
    jsval rval;
    DynamicObject *d;
      
    if(!Scene)
            return;

    poly_count = 0;
    objects_to_render = 0;

    UpdateCameraFrustum();
    WalkStaticObjectsQTree(objects_qtree,1,render_queue,&objects_to_render);

    d = Scene->dynamics;

    while(d && objects_to_render < RENDER_QUEUE_CAPACITY-1)
    {
        if(FrustumContainsBSphere(Camera.frustum,d->base.bsphere))
        if(FrustumContainsBBox(Camera.frustum,d->base.bbox))
            render_queue[objects_to_render++] = &d->base;

        d = d->next;
    }

    if(Scene->light_count)
        glEnable(GL_LIGHTING);

    /* first stage - without any surfaces who need blending */
#ifndef USE_OPENGL_VERSION_1_1
    qsort(render_queue,objects_to_render,sizeof(Object*),sort_visuals);
#endif
    render_all(0);

    /* second stage - only surfaces who need blending */
    /*
    glEnable(GL_BLEND);
    glDepthMask(GL_FALSE);
    qsort(render_queue,objects_to_render,sizeof(Object*),sort_blending);

    render_all(1);

    glDepthMask(GL_TRUE);
    glDisable(GL_BLEND);
    */
 
    glDisable(GL_LIGHTING);

    if(Scene->script_object)
    {
        JS_CallFunctionName(JS_ScriptContext,Scene->script_object,
        "on_frame",0,0,&rval);
        JS_GC(JS_ScriptContext);
    }
}

void FreeScene(void)
{
    jsval rval;
    int i;

    if(!Scene)
        return;

    if(Scene->script_object)
    {
        JS_CallFunctionName(JS_ScriptContext,Scene->script_object,
        "on_exit",0,0,&rval);
        JS_RemoveRoot(JS_ScriptContext,&Scene->script_object);
        JS_DeleteProperty(JS_ScriptContext,JS_GlobalObject,"level");
    }

    DestroyQTree(objects_qtree);
    if(collision_qtree)
        DestroyQTree(collision_qtree);

    for(i = 0; i < Scene->object_count; i++)
    {
        ReleaseResource(Scene->objects[i].model);
    }

    free(render_queue);

    free(Scene->collision);
    free(Scene->objects);
    free(Scene->lights);
    free(Scene);

    Scene = 0;
}
