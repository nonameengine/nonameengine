#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <ctype.h>
#include <stdarg.h>
#include <math.h>
#include "config.h"
#include "vfs.h"

#ifdef _MSC_VER
#define vsnprintf(buf,size,format,v) vsprintf(buf,format,v)
#endif

#define skip(str) while(isspace(*str)) str++

char *errorptr;

typedef struct buffer_string
{
        char *data;
        int length;
        int data_size;
} buffer_string;

void string_append(buffer_string *buf, char *format, ...)
{
        char str[4096];
        va_list v;

        va_start(v,format);
        buf->length += vsnprintf(str,4096,format,v);
        va_end(v);

        if((buf->length + 1) > buf->data_size)
        {
                buf->data_size += 4096;
                if(!buf->data)
                {
                        buf->data = malloc(buf->data_size);
                        buf->data[0] = '\0';
                }
                else
                        buf->data = realloc(buf->data,buf->data_size);
        }

        strcat(buf->data,str);
}

void trim(char *str)
{
        char *c = str, *cc;

        if(!str)
                        return;

        while(*c != '\0')
        {
                if(*c == '\'') /* strings */
                {
                    c++;

                    while(*c != '\'' && *c != '\0')
                        c++;

                    if(*c == '\'')
                        c++;
                }
                
                if(*c == '\"')
                {
                    c++;

                    while(*c != '\"' && *c != '\0')
                        c++;

                    if(*c == '\"')
                        c++;
                }
                
                if(*c =='/') /* comments */
                {
                        cc = c+1;

                        if(*cc == '*')
                        {
                                while(*cc != '\0')
                                {                                  
                                        if(*cc == '*' && cc[1] == '/')
                                        {
                                                cc=cc+2;
                                                break;
                                        }

                                        cc++;                   
                                }

                                while(c != cc)
                                {
                                        if(*c != '\n')
                                                *c = ' ';
                                        c++;
                                }
                        }
                        else if(*cc == '/')
                        {
                                while(*cc != '\r' && *cc != '\n' && *cc != '\0')
                                        cc++;
                
                                while(c <= cc)
                                {
                                        if(*c != '\n')
                                                *c = ' ';
                                        c++;
                                }
                        }
                }
                
                if(*c == 0x1A)
                        *c = '\0';
                        
                c++;
        }
}

int is_number(char *str)
{
        while(*str != '\0')
        {
                if(!isdigit(*str) && *str != '.' && *str != '-' && *str != '+') return 0;
                str++;
        }

        return 1;
}

Config *parse_value(char **str);
Config *parse_object(char **str);
Config *parse_array(char **str);
Config *parse_variable(char **str);

Config *parse_value(char **str)
{
        Config *val;
        char *c = *str, *cc;

        char q;

        if(*c == '{')
        {
                c++; /* skip { */

                skip(c);

                val = parse_object(&c);
                val->type = CT_OBJECT;

                c++; /* skip } */
        }
        else if(*c == '(')
        {
                c++; /* skip ( */

                skip(c);

                val = parse_array(&c);
                val->type = CT_ARRAY;

                c++; /* skip ) */
        }
        else if(q = *c, q == '\"' || q == '\'') 
        {
                val = malloc(sizeof(Config));
                memset(val,0,sizeof(Config));
                c++; /* skip " */
                cc = c;

                while(*cc != q && *cc != '\0')
                        cc++;

                val->type = CT_STRING;

                if(*cc == '\0')
                {
                        errorptr = c;
                        free(val);

                        return 0;
                }
                else
                {
                        *cc = '\0';

                        val->valuestring = strdup(c);

                        c = cc+1; /* skip this string */
                }
        }
        else
        {
                val = malloc(sizeof(Config));
                memset(val,0,sizeof(Config));
                cc = c;

                while(!isspace(*cc) && *cc != ',' && *cc != ';' &&
                *cc != ')' && *cc != '}' && *cc != '\0')
                        cc++;

                val->valuestring = malloc(cc - c + 1);
                memcpy(val->valuestring,c,cc-c);
                val->valuestring[cc-c] = '\0';

                if(stricmp(val->valuestring,"true") == 0)
                {
                        val->type = CT_BOOL;
                        val->valueint = 1;
                        val->valuefloat = 1;
                }
                else if(stricmp(val->valuestring,"false") == 0)
                {
                        val->type = CT_BOOL;
                        val->valueint = 0;
                        val->valuefloat = 0;
                }
                else if(is_number(val->valuestring))
                {
                        val->type = CT_NUMBER;
                        val->valueint = atoi(val->valuestring);
                        val->valuefloat = atof(val->valuestring);
                }
                else
                {
                        errorptr = c;
                        free(val);

                        return 0;
                }

                c = cc;

                skip(c);
        }

        *str = c;

        return val;
}

Config *parse_object(char **str)
{
        Config *val = malloc(sizeof(Config));
        Config *cur = 0;
        char *c = *str;

        memset(val,0,sizeof(Config));

        while(*c != '}' && *c != '\0')
        {
                skip(c);

                if(cur == 0)
                {
                        cur = val->child = parse_variable(&c);
                }
                else
                {
                        cur->next = parse_variable(&c);
                        cur = cur->next;
                }

                if(cur == 0)
                        break;

                skip(c);
                if(*c == ',' || *c == ';') c++;
                skip(c);
        }

        if(*c == '\0')
                errorptr = *str;

        *str = c;

        return val;
}

Config *parse_array(char **str)
{
        Config *val = malloc(sizeof(Config));
        Config *cur = 0;
        char *c = *str;

        memset(val,0,sizeof(Config));

        while(*c != ')' && *c != '\0')
        {
                skip(c);

                if(cur == 0)
                {
                        cur = val->child = parse_value(&c);
                }
                else
                {
                        cur->next = parse_value(&c);
                        cur = cur->next;
                }

                if(cur == 0)
                        break;

                cur->name = 0;

                skip(c);
                if(*c == ',' || *c == ';') c++;
                skip(c);
        }

        if(*c == '\0')
                errorptr = *str;

        *str = c;

        return val;
}

Config *parse_variable(char **str)
{
        Config *val;
        char *c = *str, *cc, *key;

        skip(c);

        cc = c;

        while(!isspace(*cc) && *cc != '=' && *cc != ',' && *cc != '}' && *cc != '\0')
                cc++;

        if(cc-c == 0)
        {
            errorptr = c;
            return 0;
        }

        key = malloc(cc-c + 1);
        memcpy(key,c,cc-c);
        key[cc-c] = '\0';

        c = cc;

        skip(c);

        if(*c == '=')
        {
                c++; /* skip = */

                skip(c);

                val = parse_value(&c);

                if(val)
                        val->name = key;

                skip(c);
        }
        else
        {
                errorptr = c;
                free(key);

                return 0;
        }

        *str = c;

        return val;
}

static int depth = 0;

static void print_object(Config *cfg, buffer_string *buf, int formatted);
static void print_array(Config *cfg, buffer_string *buf, int formatted);
static void print_variable(Config *cfg, buffer_string *buf, int formatted);

static void print_object(Config *cfg, buffer_string *buf, int formatted)
{
        Config *c = cfg->child;

        while(c)
        {
                print_variable(c,buf,formatted);
                c = c->next;

                if(c)
                        string_append(buf, formatted ? ",\n" : ",");
        }
}

static void print_array(Config *cfg, buffer_string *buf, int formatted)
{
        Config *c = cfg->child;

        while(c)
        {
                print_variable(c,buf,c->type == CT_OBJECT ? formatted : 0);
                c = c->next;

                if(c)
                        string_append(buf,formatted ? ", " : ",");
        }
}

static void print_variable(Config *cfg, buffer_string *buf, int formatted)
{
        int i;

        if(formatted)
                for(i = 0; i < depth; i++)
                        string_append(buf,"\t");

        if(cfg->name)
                        string_append(buf,formatted ? "%s = " : "%s=",cfg->name);

        switch(cfg->type)
        {
                case CT_ARRAY:
                        string_append(buf,"(");

                        print_array(cfg,buf,formatted);

                        string_append(buf,")");
                break;
                case CT_OBJECT:
                        string_append(buf,"{");

                        if(formatted)
                                string_append(buf,"\n");

                        depth++;

                        print_object(cfg,buf,formatted);

                        depth--;

                        if(formatted)
                        {
                                string_append(buf,"\n");
                                for(i = 0; i < depth; i++)
                                        string_append(buf,"\t");
                        }

                        string_append(buf,"}");
                break;
                case CT_NUMBER:
                        if(cfg->valuefloat - floor(cfg->valuefloat) != 0)
                                        string_append(buf,"%f",cfg->valuefloat);
                        else
                                        string_append(buf,"%d",cfg->valueint);
                break;
                case CT_STRING:
                        string_append(buf,"\"%s\"",cfg->valuestring);
                break;
                case CT_BOOL:
                        string_append(buf, cfg->valueint ? "true" : "false");
                break;
        }
}

Config *LoadConfig(char *file_name)
{
    Config *out;
    char *buffer;
    int file_length;
    File *f;

    f = FileOpen(file_name,"rb");

    if(!f)
        return 0;

    FileSeek(f,0,SEEK_END);
    file_length = FileTell(f);
    FileSeek(f,0,SEEK_SET);

    buffer = malloc(file_length + 1);

    FileRead(buffer,file_length,1,f);
    buffer[file_length] = '\0';
    FileClose(f);

    out = ParseConfig(buffer);

    if(errorptr != 0)
    {
        int line = 1,chr = 1;
        int i,error = errorptr - buffer;

        for(i = 0; i < error; i++, chr++)
        {
            if(buffer[i] == '\n')
            {
                line++;
                chr = 1;
            }
        }

        printf("! Config '%s' parsing failed! Error at %d:%d\n",file_name,line,chr);

        if(out)
            DeleteConfig(out);
        out = 0;
    }

    free(buffer);

    return out;
}

int SaveConfig(Config *cfg, char *file_name, int formatted)
{
    char *str;
    File *f = FileOpen(file_name,"wb");

    if(!f)
        return 1;

    str = PrintConfig(cfg,formatted);
    FileWrite(str,strlen(str),1,f);
    free(str);

    FileClose(f);

    return 0;
}

Config *ParseConfig(char *str)
{
        Config *root = malloc(sizeof(Config));
        Config *cur = 0;

        memset(root,0,sizeof(Config));

        root->type = CT_OBJECT;

        errorptr = 0;

        trim(str);

        while(*str != '\0')
        {
                if(cur == 0)
                {
                        cur = root->child = parse_variable(&str);
                }
                else
                {
                        cur->next = parse_variable(&str);
                        cur = cur->next;
                }

                if(cur == 0)
                        break;
                        
                skip(str);
                if(*str == ',' || *str == ';') str++;
                skip(str);
        }

        return root;
}

char *PrintConfig(Config *cfg, int formatted)
{
        buffer_string buf = {0,0,0};

        if(cfg->type == CT_OBJECT)
                print_object(cfg,&buf,formatted);
        else
                print_variable(cfg,&buf,formatted);

        return buf.data;
}

void DeleteConfig(Config *cfg)
{
        Config *c, *next;

        if(cfg->type == CT_OBJECT || cfg->type == CT_ARRAY)
        {
                c = cfg->child;

                while(c)
                {
                        next = c->next;
                        DeleteConfig(c);
                                                c = next;
                }
        }
        else
        {
                free(cfg->valuestring);
        }

        free(cfg->name);
        free(cfg);
}

Config *CreateConfigArray(void)
{
        Config *item = malloc(sizeof(Config));
        item->type = CT_ARRAY;
        item->child = 0;

        return item;
}

Config *CreateConfigObject(void)
{
        Config *item = malloc(sizeof(Config));
        item->type = CT_OBJECT;
        item->child = 0;

        return item;
}

Config *CreateConfigNumber(float num)
{
        Config *item = malloc(sizeof(Config));
        item->type = CT_NUMBER;
        item->valueint = (int)num;
        item->valuefloat = num;

        return item;
}

Config *CreateConfigString(char *str)
{
        Config *item = malloc(sizeof(Config));
        item->type = CT_STRING;
        item->valuestring = strdup(str);

        return item;
}

Config *CreateConfigBool(int b)
{
        Config *item = malloc(sizeof(Config));
        item->type = CT_BOOL;
        item->valueint = b;
        item->valuefloat = b;

        return item;
}

void AddConfigItemToArray(Config *array, Config *item)
{
        Config *c = array->child;

        if(!c)
                array->child = item;
        else
        {
                while(c->next)
                        c = c->next;

                c->next = item;
        }
}

void AddConfigItemToObject(Config *object, char *name, Config *item)
{
        Config *c = object->child;

        item->name = strdup(name);

        if(!c)
        {
                object->child = item;
        }
        else
        {
                while(c->next)
                        c = c->next;

                c->next = item;
        }
}

void InsertConfigItemToArray(Config *array, Config *item, int which)
{
        Config *c = array->child;

        if(!c)
        {
                array->child = item;
        }
        else
        {
                while(c->next && which--)
                {
                        c = c->next;
                }

                item->prev = c->prev;
                item->next = c;

                if(c->prev)
                        c->prev->next = item;

		c->prev = item;
        }
}

void ReplaceConfigItemInArray(Config *array, int which, Config *item)
{
        Config *c = array->child;

        if(!c)
        {
                array->child = item;
        }
        else
        {
                while(c && which--)
                {
                        c = c->next;
                }

                if(!c)
                        return;
                if(c->prev)
                        c->prev->next = item;

                item->prev = c->prev;
                item->next = c->next;

                if(c->next)
                        c->next->prev = item;

                DeleteConfig(c);
        }
}

void ReplaceConfigItemInObject(Config *object, char *name, Config *item)
{
        Config *c = object->child;

        if(c)
        {
                while(c)
                {
                        if(stricmp(name,c->name) == 0)
                                break;

                        c = c->next;
                }

                if(c)
                {
                        if(c->prev)
                                c->prev->next = item;

                        item->prev = c->prev;
                        item->next = c->next;
                        
                        if(c->next)
                                c->next->prev = item;

                        DeleteConfig(c);
                }
        }
}

Config *GetConfigArrayItem(Config *arr, int which)
{
        Config *c = arr->child;

        while(c && which--)
                c = c->next;

        return c;
}

Config *GetConfigObjectItem(Config *object, char *name)
{
        Config *c = object->child;

        while(c)
        {
                if(stricmp(c->name,name) == 0)
                        break;

                c = c->next;
        }

        return c;
}

int GetConfigArraySize(Config *arr)
{
        int i = 0;
        Config *c = arr->child;
        
        while(c)
        {
                c = c->next;
                i++;
        }

        return i;
}

Config *DetachConfigItemFromArray(Config *arr, int which)
{
        Config *c = arr->child;

        while(c && which--)
                c = c->next;

        if(c)
        {
                if(c->prev)
                        c->prev->next = c->next;
                if(c->next)
                        c->next->prev = c->prev;
        }

        return c;
}

void DeleteConfigItemFromArray(Config *arr, int which)
{
        Config *c = DetachConfigItemFromArray(arr,which);

        if(c)
                DeleteConfig(c);
}

Config *DetachConfigItemFromObject(Config *object, char *name)
{
        Config *c = object->child;

        while(c)
        {
                if(stricmp(c->name,name) == 0)
                        break;
                c = c->next;
        }

        if(c)
        {
                if(c->prev)
                        c->prev->next = c->next;
                if(c->next)
                        c->next->prev = c->prev;
        }

        return c;
}

void DeleteConfigItemFromObject(Config *object, char *name)
{
        Config *c = DetachConfigItemFromObject(object,name);

        if(c)
                DeleteConfig(c);
}

char *GetConfigErrorPtr(void)
{
        return errorptr;
}

Config *CreateConfigVector2f(Vector2f vec)
{
    Config *c = CreateConfigArray();

    AddConfigItemToArray(c,CreateConfigNumber(vec.x));
    AddConfigItemToArray(c,CreateConfigNumber(vec.y));

    return c;
}

Config *CreateConfigVector3f(Vector3f vec)
{
    Config *c = CreateConfigArray();

    AddConfigItemToArray(c,CreateConfigNumber(vec.x));
    AddConfigItemToArray(c,CreateConfigNumber(vec.y));
    AddConfigItemToArray(c,CreateConfigNumber(vec.z));

    return c;
}

Config *CreateConfigVector4f(Vector4f vec)
{
    Config *c = CreateConfigArray();

    AddConfigItemToArray(c,CreateConfigNumber(vec.x));
    AddConfigItemToArray(c,CreateConfigNumber(vec.y));
    AddConfigItemToArray(c,CreateConfigNumber(vec.z));
    AddConfigItemToArray(c,CreateConfigNumber(vec.w));

    return c;
}

Vector2f GetConfigVector2f(Config *item)
{
    Vector2f vec;
    Config *x, *y;

    x = GetConfigArrayItem(item,0);
    y = GetConfigArrayItem(item,1);
    
    memset(&vec,0,sizeof(Vector2f));

    if(x)
        vec.x = x->valuefloat;

    if(y)
        vec.y = y->valuefloat;

    return vec;
}

Vector3f GetConfigVector3f(Config *item)
{
    Vector3f vec;
    Config *x, *y, *z;

    x = GetConfigArrayItem(item,0);
    y = GetConfigArrayItem(item,1);
    z = GetConfigArrayItem(item,2);
    
    memset(&vec,0,sizeof(Vector3f));

    if(x)
        vec.x = x->valuefloat;

    if(y)
        vec.y = y->valuefloat;

    if(z)
        vec.z = z->valuefloat;

    return vec;
}

Vector4f GetConfigVector4f(Config *item)
{
    Vector4f vec;
    Config *x, *y, *z, *w;

    x = GetConfigArrayItem(item,0);
    y = GetConfigArrayItem(item,1);
    z = GetConfigArrayItem(item,2);
    w = GetConfigArrayItem(item,3);
    
    memset(&vec,0,sizeof(Vector4f));

    if(x)
        vec.x = x->valuefloat;

    if(y)
        vec.y = y->valuefloat;

    if(z)
        vec.z = z->valuefloat;

    if(w)
        vec.w = w->valuefloat;

    return vec;
}
