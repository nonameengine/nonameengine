#ifndef CONFIG_H
#define CONFIG_H

#include "vector.h"

enum ConfigType
{
        CT_OBJECT,
        CT_ARRAY,
        CT_STRING,
        CT_NUMBER,
        CT_BOOL
};

typedef struct Config
{
        char *name;
        int type;

        char *valuestring;
        int valueint;
        float valuefloat;

        struct Config *child;
        struct Config *prev,*next;
} Config;

Config *LoadConfig(char *file_name);
int SaveConfig(Config *cfg, char *file_name, int formatted);

Config *ParseConfig(char *str);
char *PrintConfig(Config *cfg, int formatted);
void DeleteConfig(Config *cfg);

Config *CreateConfigArray(void);
Config *CreateConfigObject(void);
Config *CreateConfigNumber(float num);
Config *CreateConfigString(char *str);
Config *CreateConfigBool(int value);

Config *CreateConfigVector2f(Vector2f vec);
Config *CreateConfigVector3f(Vector3f vec);
Config *CreateConfigVector4f(Vector4f vec);

void AddConfigItemToArray(Config *array, Config *item);
void AddConfigItemToObject(Config *object, char *name, Config *item);
void InsertConfigItemToArray(Config *array, Config *item, int which);

void ReplaceConfigItemInArray(Config *array, int which, Config *newitem);
void ReplaceConfigItemInObject(Config *object, char *name, Config *newitem);

Config *GetConfigArrayItem(Config *array, int id);
Config *GetConfigObjectItem(Config *object, char *name);
int GetConfigArraySize(Config *arr);

void DeleteConfigItemFromArray(Config *array, int which);
Config *DetachConfigItemFromArray(Config *array, int which);
void DeleteConfigItemFromObject(Config *object, char *name);
Config *DetachConfigItemFromObject(Config *object, char *name);

Vector2f GetConfigVector2f(Config *item);
Vector3f GetConfigVector3f(Config *item);
Vector4f GetConfigVector4f(Config *item);

char* GetConfigErrorPtr(void);

#endif

