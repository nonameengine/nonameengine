#ifndef SETTINGS_H
#define SETTINGS_H

#include "vector.h"

int LoadSettings(char *file_name);
void FreeSettings(void);

int GetIntSetting(char *name, int def);
float GetFloatSetting(char *name, float def);
char *GetStringSetting(char *name);

Vector2f GetVector2fSetting(char *name, Vector2f def);
Vector3f GetVector3fSetting(char *name, Vector3f def);
Vector4f GetVector4fSetting(char *name, Vector4f def);

#endif
