#include <stdlib.h>
#include <string.h>
#include "types.h"
#include "vfs.h"

int archive_count = 0;
unzFile archives[16];

static char *fix_path(const char *path)
{
    char *unix_path = strdup(path), *str = unix_path;
    
    while(*str++ != '\0')
    {
        if(*str == '\\')
            *str = '/';
    }

    return unix_path;
}

void InitVFS(void)
{
    u8 i;
    char name[32];

    for(i = 0x0; i < 0x10; i++)
    {
        sprintf(name,"data%hhX.zip",i);

        archives[archive_count] = unzOpen(name);
        
        if(archives[archive_count] != 0)
        {
            printf("loaded archive '%s'\n",name);
            archive_count++;
        }
        else
            break;
    }
}

void FreeVFS(void)
{
    while(archive_count--)
    {
        if(unzClose(archives[archive_count]) != UNZ_OK)
            printf("error closing archive!\n");
    }
}

File *FileOpen(const char *path, const char *mode)
{
    int i;
    char *unix_path;
    
    File *f = malloc(sizeof(struct File));
    memset(f,0,sizeof(struct File));

    f->handle = fopen(path,mode);

    if(f->handle)
        return f;

    if(strchr(mode,'w'))
    {
        printf("writing to archive not supported\n");
        free(f);
        return 0;
    }

    unix_path = fix_path(path);

    for(i = 0; i < archive_count; i++)
    {
        if(unzLocateFile(archives[i],unix_path,2) == UNZ_OK &&
         unzOpenCurrentFile(archives[i]) == UNZ_OK)
        {
            unz_file_info fi;
            
            unzGetCurrentFileInfo(archives[i],&fi,0,0,0,0,0,0);

            f->data = malloc(fi.uncompressed_size);

            if(f->data == 0)
            {
                printf("out of memory!\n");
                exit(-1);
            }
            
            f->length = fi.uncompressed_size;
            f->position = 0;

            unzReadCurrentFile(archives[i],f->data,f->length);

            unzCloseCurrentFile(archives[i]);
        }
    }

    free(unix_path);

    if(f->data == 0)
    {
        free(f);
        return 0;
    }
    else
        return f;
}

void FileClose(File *f)
{
    if(f->handle)
        fclose(f->handle);

    if(f->data)
        free(f->data);

    free(f);
    return;
}

size_t FileRead(void *buf, size_t size, size_t count, File *f)
{
    u8 *buffer = buf;
    size_t i, j, k;
        
    if(f->handle)
        return fread(buf,size,count,f->handle);

    for(i = 0, k = 0; i < count; i++, k++)
    {
        for(j = 0; j < size; j++)
		{
			if(f->position == f->length) /*EOF!*/
				return k;
        
            *buffer++ = f->data[f->position++];
		}
    }

    return k;
}

size_t FileWrite(void *buf, size_t size, size_t count, File *f)
{
    if(f->handle)
        return fwrite(buf,size,count,f->handle);
    else
    {
        printf("writing to archive not supported!\n");
        return 0;
    }
}

int FileSeek(File *f, long int offset, int where)
{
    if(f->handle)
        return fseek(f->handle, offset, where);

    switch(where)
    {
        case SEEK_SET:
            if(offset < 0)
                return 1;
            f->position = offset;
        break;
        case SEEK_CUR:
            if(offset < 0 && (unsigned)-offset > f->position)
                f->position = 0;
            else
            {
                f->position += offset;

                if(f->position > f->length)
                    f->position = f->length;
            }
        break;
        case SEEK_END:
            if(offset > 0)
                return 1;
            f->position = f->length += offset;
        break;
        default:
            return 1;
    }

    return 1;
}

size_t FileTell(File *f)
{
    if(f->handle)
        return ftell(f->handle);

    return f->position;
}

int FileEOF(File *f)
{
    if(f->handle)
        return feof(f->handle);

    return f->position > f->length;
}
