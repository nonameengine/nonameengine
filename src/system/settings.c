#include "config.h"
#include "settings.h"

Config *settings;

int LoadSettings(char *file_name)
{
        settings = LoadConfig(file_name);
        
        return settings != 0;
}

void FreeSettings(void)
{
        if(settings)
                DeleteConfig(settings);
}

int GetIntSetting(char *name, int def)
{
        Config *s;
        
        if(settings)
        {
                s = GetConfigObjectItem(settings,name);
                if(s)
                        return s->valueint;
        }
        
        return def;
}

float GetFloatSetting(char *name, float def)
{
        Config *s;
        
        if(settings)
        {
                s = GetConfigObjectItem(settings,name);
                if(s)
                        return s->valuefloat;
        }
        
        return def;
}

char *GetStringSetting(char *name)
{
        Config *s;
        
        if(settings)
        {
                s = GetConfigObjectItem(settings,name);
                if(s)
                        return s->valuestring;
        }
        
        return 0;
}

Vector2f GetVector2fSetting(char *name, Vector2f def)
{
    Config *s;

    if(settings)
    {
        s = GetConfigObjectItem(settings,name);
        if(s)
            return GetConfigVector2f(s);
    }

    return def;
}

Vector3f GetVector3fSetting(char *name, Vector3f def)
{
    Config *s;

    if(settings)
    {
        s = GetConfigObjectItem(settings,name);
        if(s)
            return GetConfigVector3f(s);
    }

    return def;
}

Vector4f GetVector4fSetting(char *name, Vector4f def)
{
    Config *s;

    if(settings)
    {
        s = GetConfigObjectItem(settings,name);
        if(s)
            return GetConfigVector4f(s);
    }

    return def;
}
