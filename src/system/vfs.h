#ifndef VFS_H
#define VFS_H

#include <stdio.h>
#include <minizip/unzip.h>
#include "../types.h"

typedef struct File
{
    u8 *data;
    size_t length, position;
    FILE *handle;
} File;

void InitVFS(void);
void FreeVFS(void);

File *FileOpen(const char *path, const char *mode);
void FileClose(File *f);

size_t FileRead(void *buf, size_t size, size_t count, File *f);
size_t FileWrite(void *buf, size_t size, size_t count, File *f);

int  FileSeek(File *f, long int offset, int where);
size_t FileTell(File *f);

int FileEOF(File *f);

#endif
