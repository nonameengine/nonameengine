#include <math.h>
#include "vector.h"

Vector2f Vector2f_set(float x, float y)
{
    Vector2f vec;

    vec.x = x;
    vec.y = y;

    return vec;
}

Vector2f Vector2f_add(Vector2f vec1, Vector2f vec2)
{
    vec1.x += vec2.x;
    vec1.y += vec2.y;

    return vec1;
}

Vector2f Vector2f_addn(Vector2f v, float n)
{
    v.x += n;
    v.y += n;

    return v;
}

Vector2f Vector2f_sub(Vector2f vec1, Vector2f vec2)
{
    vec1.x -= vec2.x;
    vec1.y -= vec2.y;

    return vec1;
}

Vector2f Vector2f_subn(Vector2f v, float n)
{
    v.x -= n;
    v.y -= n;

    return v;
}

Vector2f Vector2f_mul(Vector2f vec1, Vector2f vec2)
{
    vec1.x *= vec2.x;
    vec1.y *= vec2.y;

    return vec1;
}

Vector2f Vector2f_muln(Vector2f v, float n)
{
    v.x *= n;
    v.y *= n;

    return v;
}

Vector2f Vector2f_div(Vector2f vec1, Vector2f vec2)
{
    if(vec2.x != 0.0)       
        vec1.x /= vec2.x;
    else
        vec1.x = 0.0;

    if(vec2.y != 0.0)
        vec1.y /= vec2.y;
    else
        vec1.y = 0.0;

    return vec1;
}

Vector2f Vector2f_divn(Vector2f v, float n)
{
    if(n == 0.0f)
        return Vector2f_set(0.0,0.0);

    v.x /= n;
    v.y /= n;

    return v;
}

Vector2f Vector2f_normalize(Vector2f vec)
{
    float square_magnitude = sqrt((vec.x * vec.x) + (vec.y * vec.y));

    if(square_magnitude)
    {
            vec.x /= square_magnitude;
            vec.y /= square_magnitude;
    }

    return vec;
}

Vector3f Vector3f_set(float x, float y, float z)
{
    Vector3f vec;

    vec.x = x;
    vec.y = y;
    vec.z = z;

    return vec;
}

Vector3f Vector3f_add(Vector3f vec1, Vector3f vec2)
{
    vec1.x += vec2.x;
    vec1.y += vec2.y;
    vec1.z += vec2.z;

    return vec1;
}

Vector3f Vector3f_addn(Vector3f vec, float n)
{
    vec.x += n;
    vec.y += n;
    vec.z += n;

    return vec;
}

Vector3f Vector3f_sub(Vector3f vec1, Vector3f vec2)
{
    vec1.x -= vec2.x;
    vec1.y -= vec2.y;
    vec1.z -= vec2.z;

    return vec1;
}

Vector3f Vector3f_subn(Vector3f vec, float n)
{
    vec.x -= n;
    vec.y -= n;
    vec.z -= n;

    return vec;
}

Vector3f Vector3f_mul(Vector3f vec1, Vector3f vec2)
{
    vec1.x *= vec2.x;
    vec1.y *= vec2.y;
    vec1.z *= vec2.z;

    return vec1;
}

Vector3f Vector3f_muln(Vector3f vec, float n)
{
    vec.x *= n;
    vec.y *= n;
    vec.z *= n;

    return vec;
}

Vector3f Vector3f_div(Vector3f vec1, Vector3f vec2)
{
    if(vec2.x != 0.0)      
        vec1.x /= vec2.x;
    else
        vec1.x = 0.0;

    if(vec2.y != 0.0)    
        vec1.y /= vec2.y;
    else
        vec1.y = 0.0;

    if(vec2.z != 0.0)    
        vec1.z /= vec2.z;
    else
        vec1.z = 0.0;

    return vec1;
}

Vector3f Vector3f_divn(Vector3f vec, float n)
{
    if(n == 0.0)
        return Vector3f_set(0.0,0.0,0.0);

    vec.x /= n;
    vec.y /= n;
    vec.z /= n;

    return vec;
}

Vector3f Vector3f_max(Vector3f v1, Vector3f v2)
{
    if(v1.x > v2.x)
        v1.x = v2.x;
    if(v1.y > v2.y)
        v1.y = v2.y;
    if(v1.z > v2.z)
        v1.z = v2.z;

    return v1;
}

Vector3f Vector3f_min(Vector3f v1, Vector3f v2)
{
    if(v1.x < v2.x)
        v1.x = v2.x;
    if(v1.y < v2.y)
        v1.y = v2.y;
    if(v1.z < v2.z)
        v1.z = v2.z;

    return v1;
}

Vector3f Vector3f_normalize(Vector3f vec)
{
    float square_magnitude = sqrt((vec.x * vec.x) + (vec.y * vec.y) + (vec.z * vec.z));

    if(square_magnitude)
    {
            vec.x /= square_magnitude;
            vec.y /= square_magnitude;
            vec.z /= square_magnitude;
    }

    return vec;
}

float Vector3f_distance_to_sqrt(Vector3f v1, Vector3f v2)
{
    return (v1.x-v2.x)*(v1.x-v2.x) + (v1.y-v2.y)*(v1.y-v2.y) + (v1.z-v2.z)*(v1.z-v2.z);
}

float Vector3f_distance_to(Vector3f v1, Vector3f v2)
{
    return sqrt(Vector3f_distance_to_sqrt(v1,v2));
}

Vector3f Vector3f_transform(Vector3f vec, Matrix4x4 mat)
{
    Vector3f out;
    float w = mat.m[0][3] * vec.x + mat.m[1][3] * vec.y + mat.m[2][3] * vec.z + mat.m[3][3];
    
    if(w != 0.0)
    {
        out.x = (vec.x * mat.m[0][0] + vec.y * mat.m[1][0] + vec.z * mat.m[2][0] + mat.m[3][0]) / w;
        out.y = (vec.x * mat.m[0][1] + vec.y * mat.m[1][1] + vec.z * mat.m[2][1] + mat.m[3][1]) / w;
        out.z = (vec.x * mat.m[0][2] + vec.y * mat.m[1][2] + vec.z * mat.m[2][2] + mat.m[3][2]) / w;
    }
    
    return out;
}

float Vector3f_dot(Vector3f v1, Vector3f v2)
{
    return v1.x * v2.x + v1.y * v2.y + v1.z * v2.z;
}

Vector4f Vector4f_set(float x, float y, float z, float w)
{
    Vector4f vec;

    vec.x = x;
    vec.y = y;
    vec.z = z;
    vec.w = w;

    return vec;
}

Vector4f Vector4f_add(Vector4f vec1, Vector4f vec2)
{
    vec1.x += vec2.x;
    vec1.y += vec2.y;
    vec1.z += vec2.z;
    vec1.w += vec2.w;

    return vec1;
}

Vector4f Vector4f_addn(Vector4f v, float n)
{
    v.x += n;
    v.y += n;
    v.z += n;
    v.w += n;

    return v;
}

Vector4f Vector4f_sub(Vector4f vec1, Vector4f vec2)
{
    vec1.x -= vec2.x;
    vec1.y -= vec2.y;
    vec1.z -= vec2.z;
    vec1.w -= vec2.w;

    return vec1;
}

Vector4f Vector4f_subn(Vector4f v, float n)
{
    v.x -= n;
    v.y -= n;
    v.z -= n;
    v.w -= n;

    return v;
}

Vector4f Vector4f_mul(Vector4f vec1, Vector4f vec2)
{
    vec1.x *= vec2.x;
    vec1.y *= vec2.y;
    vec1.z *= vec2.z;
    vec1.w *= vec2.w;

    return vec1;
}

Vector4f Vector4f_muln(Vector4f v, float n)
{
    v.x *= n;
    v.y *= n;
    v.z *= n;
    v.w *= n;

    return v;
}

Vector4f Vector4f_div(Vector4f vec1, Vector4f vec2)
{
    if(vec2.x != 0.0)    
        vec1.x /= vec2.x;
    else
        vec1.x = 0.0;

    if(vec2.y != 0.0)    
        vec1.y /= vec2.y;
    else
        vec1.y = 0.0;

    if(vec2.z != 0.0)    
        vec1.z /= vec2.z;
    else
        vec1.z = 0.0;

    if(vec2.w != 0.0)    
        vec1.w /= vec2.w;
    else
        vec1.w = 0.0;

    return vec1;
}

Vector4f Vector4f_divn(Vector4f v, float n)
{
    if(n == 0.0)
        return Vector4f_set(0.0,0.0,0.0,0.0);

    v.x /= n;
    v.y /= n;
    v.z /= n;
    v.w /= n;

    return v;
}

Vector4f Vector4f_normalize(Vector4f vec)
{
    float square_magnitude = sqrt((vec.x * vec.x) + (vec.y * vec.y) + (vec.z * vec.z) + (vec.w * vec.w));

    if(square_magnitude)
    {
            vec.x /= square_magnitude;
            vec.y /= square_magnitude;
            vec.z /= square_magnitude;
            vec.w /= square_magnitude;
    }

    return vec;
}

Vector3f Vector4f_xyz(Vector4f vec)
{
    return Vector3f_set(vec.x,vec.y,vec.z);
}

Matrix4x4 Matrix4x4_add(Matrix4x4 m1, Matrix4x4 m2)
{
    int i,j;

    for(i = 0; i < 4; i++)
        for(j = 0; j < 4; j++)
            m1.m[i][j] += m2.m[i][j];

    return m1;
}

Matrix4x4 Matrix4x4_sub(Matrix4x4 m1, Matrix4x4 m2)
{
    int i,j;

    for(i = 0; i < 4; i++)
        for(j = 0; j < 4; j++)
            m1.m[i][j] -= m2.m[i][j];

    return m1;
}

Matrix4x4 Matrix4x4_muln(Matrix4x4 m1, float n)
{
    int i,j;

    for(i = 0; i < 4; i++)
        for(j = 0; j < 4; j++)
            m1.m[i][j] *= n;

    return m1;
}

Matrix4x4 Matrix4x4_mul(Matrix4x4 m1, Matrix4x4 m2)
{
    int i,j;
    Matrix4x4 out;

    for(i = 0; i < 4; i++)
        for(j = 0; j < 4; j++)
        {
            out.m[i][j] = m1.m[i][0]*m2.m[0][j] + m1.m[i][1]*m2.m[1][j] + m1.m[i][2]*m2.m[2][j] + m1.m[i][3]*m2.m[3][j];
        }
        
    return out;
}

Matrix4x4 Matrix4x4_transpose(Matrix4x4 m1)
{
    Matrix4x4 m2 = m1;
    int i,j;

    for(i = 0; i < 4; i++)
        for(j = 0; j < 4; j++)
            m1.m[i][j] = m2.m[j][i];

    return m1; 
}
