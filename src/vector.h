#ifndef VECTOR_H
#define VECTOR_H

typedef struct Vector2f
{
    union
    {
        struct
        {
            float x, y;
        };
        struct
        {
            float l, a;
        };
    };
} Vector2f;

typedef struct Vector3f
{
    union
    {
        struct
        {
            float x, y, z;
        };
        struct
        {
            float r, g, b;
        };
    };
} Vector3f;

typedef struct Vector4f
{
    union
    {
        struct
        {
            float x, y, z, w;
        };
        struct
        {
            float r, g, b, a;
        };
    };
} Vector4f;

typedef struct Matrix4x4
{
    union
    {
        float m[4][4];
        Vector4f v[4];
    };
} Matrix4x4;

Vector2f Vector2f_set(float x, float y);
Vector2f Vector2f_add(Vector2f,Vector2f);
Vector2f Vector2f_addn(Vector2f,float);
Vector2f Vector2f_sub(Vector2f,Vector2f);
Vector2f Vector2f_subn(Vector2f,float);
Vector2f Vector2f_mul(Vector2f,Vector2f);
Vector2f Vector2f_muln(Vector2f,float);
Vector2f Vector2f_div(Vector2f,Vector2f);
Vector2f Vector2f_divn(Vector2f,float);
Vector2f Vector2f_normalize(Vector2f);



Vector3f Vector3f_set(float x, float y, float z);
Vector3f Vector3f_add(Vector3f,Vector3f);
Vector3f Vector3f_addn(Vector3f,float n);
Vector3f Vector3f_sub(Vector3f,Vector3f);
Vector3f Vector3f_subn(Vector3f,float n);
Vector3f Vector3f_mul(Vector3f,Vector3f);
Vector3f Vector3f_muln(Vector3f,float n);
Vector3f Vector3f_div(Vector3f,Vector3f);
Vector3f Vector3f_divn(Vector3f,float n);
Vector3f Vector3f_normalize(Vector3f);
Vector3f Vector3f_max(Vector3f,Vector3f);
Vector3f Vector3f_min(Vector3f,Vector3f);

float Vector3f_distance_to_sqrt(Vector3f,Vector3f);
float Vector3f_distance_to(Vector3f,Vector3f);

float Vector3f_dot(Vector3f,Vector3f);

Vector3f Vector3f_transform(Vector3f,Matrix4x4);



Vector4f Vector4f_set(float x, float y, float z, float w);
Vector4f Vector4f_add(Vector4f,Vector4f);
Vector4f Vecotr4f_addn(Vector4f,float);
Vector4f Vector4f_sub(Vector4f,Vector4f);
Vector4f Vector4f_subn(Vector4f,float);
Vector4f Vector4f_mul(Vector4f,Vector4f);
Vector4f Vector4f_muln(Vector4f,float);
Vector4f Vector4f_div(Vector4f,Vector4f);
Vector4f Vector4f_divn(Vector4f,float);
Vector4f Vector4f_normalize(Vector4f);

Vector3f Vector4f_xyz(Vector4f);



Matrix4x4 Matrix4x4_add(Matrix4x4,Matrix4x4);
Matrix4x4 Matrix4x4_sub(Matrix4x4,Matrix4x4);
Matrix4x4 Matrix4x4_muln(Matrix4x4,float);
Matrix4x4 Matrix4x4_mul(Matrix4x4,Matrix4x4);
Matrix4x4 Matrix4x4_transpose(Matrix4x4);

#endif
