#ifndef TGA_H
#define TGA_H

#include "../types.h"

enum TgaImageType
{
    TGA_NOIMAGE         = 0,
    TGA_INDEXED         = 1,
    TGA_TRUECOLOR       = 2,
    TGA_GRAYSCALE       = 3,
    TGA_INDEXED_RLE     = 9,
    TGA_TRUECOLOR_RLE   = 10,
    TGA_GRAYSCALE_RLE   = 11
};

#pragma pack(push,1)
typedef struct TgaHeader
{
    u8 id_length;
    u8 color_map_type;
    u8 image_type;

    u16 color_map_origin;
    u16 color_map_length;
    u8 color_map_entry_size;

    u16 x_origin;
    u16 y_origin;

    u16 width;
    u16 height;

    u8 pixel_size;
    u8 image_descriptor;
} TgaHeader;
#pragma pack(pop)

typedef struct TgaImage
{
        TgaHeader header;
        u8 *palette;
        u8 *pixels;
} TgaImage;

int LoadTGA(pcstr filename, TgaImage *out);
void SaveTGA(pcstr filename, TgaImage *img);

void FlipVertical(u8 *data, u16 width, u16 height, int bpp);
void FlipHorizontal(u8 *data, u16 width, u16 height, int bpp);

void UnpackIndexedImage(u8 *data, u8 *palette, u8 *out, int bpp, u32 count); 
void SwapRandBComponents(u8 *p, int bpp, u32 count);

#endif
