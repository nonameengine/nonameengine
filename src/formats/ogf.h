#ifndef OGF_H
#define OGF_H

#include "visual.h"

Visual *LoadOGF(const char *path);

#endif
