#ifdef _WIN32
#include <windows.h>
#endif
#include <string.h>
#include <stdlib.h>
#include <GL/gl.h>
#include <stdio.h>
#include "ogf.h"
#include "types.h"
#include "vector.h"
#include "resource_manager.h"
#include "system/vfs.h"
#include "../utils.h"

enum OgfVertexFormat
{
    OGF_VF_NOSKIN = 0x00000112,
    OGF_VF_SKIN_1 = 1*0x12071980,
    OGF_VF_SKIN_2 = 2*0x12071980

    /* 12.07.1980 - oles birthday :) */
};

enum OgfModelType
{
    OGF_MT_NORMAL       = 0x0,
    OGF_MT_HIERRARHY    = 0x1,
    OGF_MT_PROGRESSIVE  = 0x2,
    OGF_MT_SKELETON_ANIM = 0x3,
    OGF_MT_SKELETON_GEOMDEF_PM = 0x4,
    OGF_MT_SKELETON_GEOMDEF_ST = 0x5
};

enum OgfChunkIds
{
    OGF_HEADER          = 0x1,
    OGF_TEXTURE         = 0x2,
    OGF_VERTICES        = 0x3,
    OGF_INDICES         = 0x4,
    OGF_SWIDATA         = 0x6,
    OGF_CHILDREN        = 0x9,
    OGF_COMMENT         = 0x12
};

/* it is compliant with internal format */
typedef struct OgfVertex
{
    Vector3f point;
    Vector3f normal;
    Vector2f texcoord;    
} OgfVertex;

#pragma pack(push,1)
typedef struct OgfBBox
{
    Vector3f min;
    Vector3f max;
} OgfBBox;

typedef struct OgfBSphere
{
    Vector3f center;
    float radius;
} OgfBSphere;

typedef struct OgfHeader
{
    u8 version;
    u8 type;
    u16 shader_id;

    OgfBBox bb;
    OgfBSphere bs;
} OgfHeader;
#pragma pack(pop)

int load_surface(Visual *v, int progressive, File *f)
{
    Surface *s;
    u32 chunk, size;

    chunk = r_u32(f);
    size = r_u32(f);

    v->surface_count++;
    v->surfaces = realloc(v->surfaces,
    v->surface_count * sizeof(Surface));

    s = &v->surfaces[v->surface_count-1];

    if(chunk == OGF_TEXTURE)
    {
        char texture[256], shader[256];

        r_string(texture,256,f);
        r_string(shader,256,f);

        s->texture = GetResource(texture,RT_TEXTURE);
        
        /* default "shader" */
        s->render_mode = RM_DRAW|RM_LIGHTING;
        s->draw_mode = DM_DEFAULT;

        if(strstr(shader,"leaf") || strstr(shader,"aref"))
        {
            s->render_mode = RM_ALPHA_TEST|RM_DRAW|RM_LIGHTING;
        }
        if(strstr(shader,"trans"))
        {
            s->render_mode = RM_BLEND|RM_LIGHTING;
            s->blend_func = BF_BLEND;
        }
        if(strstr(shader,"def_add"))
        {
            s->render_mode = RM_BLEND;
            s->blend_func = BF_ADD;
        }
        if(strstr(shader,"def_refl"))
        {
            s->render_mode = RM_BLEND|RM_DRAW|RM_LIGHTING;
            s->blend_func = BF_BLEND;
            s->draw_mode = DM_SPHERE_MAP;
        }

        s->material.ambient = Vector4f_set(1,1,1,1);
        s->material.diffuse = Vector4f_set(1,1,1,1);
        s->material.specular = Vector4f_set(0,0,0,1);

        if(strcmp(shader,"selflight") == 0)
            s->material.emission = Vector4f_set(1,1,1,1);
        else if(strcmp(shader,"mushroom") == 0)
            s->material.emission = Vector4f_set(0.1,0.1,0.1,1);
        else
            s->material.emission = Vector4f_set(0,0,0,1);

        s->material.shininess = 0.0f;    
    }
    else
        return 1;

    chunk = r_u32(f);
    size = r_u32(f);

    if(chunk == OGF_VERTICES)
    {
        OgfVertex *vertices;
        u32 vertex_format;
        u32 count, i;

        vertex_format = r_u32(f);
        count = r_u32(f);

        vertices = malloc(sizeof(OgfVertex) * count);

        switch(vertex_format)
        {
            case OGF_VF_NOSKIN:
                for(i = 0; i < count; i++)
                {
                    OgfVertex vtx;

                    FileRead(&vtx,sizeof(OgfVertex),1,f);
                    
                    vtx.point.z = -vtx.point.z;
                    vtx.normal.z = -vtx.normal.z;

                    vertices[i] = vtx;
                }
            break;

            case OGF_VF_SKIN_1:
                for(i = 0; i < count; i++)
                {
                    OgfVertex vtx;

                    FileRead(&vtx.point,sizeof(Vector3f),1,f);
                    FileRead(&vtx.normal,sizeof(Vector3f),1,f);
                    FileSeek(f,sizeof(Vector3f) * 2,SEEK_CUR); /* tangent and binormal */
                    FileRead(&vtx.texcoord,sizeof(Vector2f),1,f);
                    FileSeek(f,sizeof(u32),SEEK_CUR); /* bone id */
                    
                    vtx.point.z = -vtx.point.z;
                    vtx.normal.z = -vtx.normal.z;

                    vertices[i] = vtx;
                }
            break;

            case OGF_VF_SKIN_2:
                for(i = 0; i < count; i++)
                {
                    OgfVertex vtx;

                    FileSeek(f,sizeof(u16) * 2,SEEK_CUR); /* bone ids */
                    FileRead(&vtx.point,sizeof(Vector3f),1,f);
                    FileRead(&vtx.normal,sizeof(Vector3f),1,f);
                    FileSeek(f,sizeof(Vector3f) * 2,SEEK_CUR); /* tangent and binormal */
                    FileSeek(f,sizeof(float),SEEK_CUR); /* bone weight */
                    FileRead(&vtx.texcoord,sizeof(Vector2f),1,f);

                    vtx.point.x = -vtx.point.z;
                    vtx.normal.x = -vtx.normal.z;
                    
                    vertices[i] = vtx;
                    
                }
            break;

            default:
                printf("unknown vertex format.\n");
                return 1;
        }

        s->vertices = (u8*)vertices;
    }
    else
        return 1;

    chunk = r_u32(f);
    size = r_u32(f);

    if(chunk == OGF_INDICES)
    {
        u16 *indices;
        u16 v1, v2, v3;    
        u32 count, i;

        count = r_u32(f);
        
        if(count < 3 || count % 3)
            return 1;
         
        indices = malloc(sizeof(short) * count);

        for(i = 0; i < count;)
        {
            v1 = r_u16(f);
            v2 = r_u16(f);
            v3 = r_u16(f);
            
            indices[i++] = v3;
            indices[i++] = v2;
            indices[i++] = v1;
        }

        s->indices = (u8*)indices;
        s->face_count = count / 3;
    }
    else
        return 1;

    if(progressive)
    {
        chunk = r_u32(f);
        size = r_u32(f);
        
        if(chunk == OGF_SWIDATA)
        {
            u32 count, i;
    
            FileSeek(f,sizeof(u32) * 4,SEEK_CUR); /*skip reserved*/
            count = r_u32(f);
    
            s->swis = malloc(count * sizeof(SlideWindow));
    
            for(i = 0; i < count; i++)
            {
                s->swis[i].offset = r_u32(f);
                s->swis[i].face_count = r_u16(f);
    
                FileSeek(f,sizeof(u16),SEEK_CUR); /*num verts*/
            }
    
            s->sw_count = count;
        }
    }
    else
    {
        s->sw_count = 0;
        s->swis = 0;
    }
    
    s->type = GL_TRIANGLES;
    s->index_type = GL_UNSIGNED_SHORT;
    s->vertex_format = VF_POSITION|VF_NORMAL|VF_TEXCOORD;

    return 0;
}

int load_hierrarhy(Visual *v, File *f)
{
    u32 chunk, size, start, id = 0;
    OgfHeader hdr;

    chunk = r_u32(f);
    size = r_u32(f);

    if(chunk == OGF_CHILDREN)
    {
        start = FileTell(f);

        while(FileTell(f) < start + size)
        {
            if(r_u32(f) != id)
                return 1;

            FileSeek(f,sizeof(u32),SEEK_CUR);

            if(r_u32(f) != OGF_HEADER)
                return 1;
            if(r_u32(f) != sizeof(OgfHeader))
                return 1;
            
            FileRead(&hdr,sizeof(OgfHeader),1,f);

            if(hdr.type == OGF_MT_NORMAL || hdr.type == OGF_MT_PROGRESSIVE)
            {
                if(load_surface(v,hdr.type == OGF_MT_PROGRESSIVE,f) != 0)
                    return 1;
            }
            else
            {
                printf("Unsupported model type.\n");
                return 1;
            }

            id++;
        }
    }
    else
        return 1;

    return 0;
}

Visual *LoadOGF(const char *path)
{
    OgfHeader hdr;
    File *f = FileOpen(path,"rb");
    Visual *out;

    if(!f)
    {
        printf("Can't open file '%s'!\n",path);
        return 0;
    }

    if(r_u32(f) != OGF_HEADER)
        return 0;
    if(r_u32(f) != sizeof(OgfHeader))
        return 0;
      
    FileRead(&hdr,sizeof(OgfHeader),1,f);

    if(hdr.version != 4)
    {
        printf("Unsupported OGF version.\n");
        return 0;
    }

    if(hdr.type != OGF_MT_NORMAL &&
    hdr.type != OGF_MT_HIERRARHY &&
    hdr.type != OGF_MT_PROGRESSIVE)
    {
        printf("Unsupported model type.\n");
        return 0;
    }

    out = malloc(sizeof(Visual));
    memset(out,0,sizeof(Visual));

    if(r_u32(f) == OGF_COMMENT)
        FileSeek(f,r_u32(f),SEEK_CUR);
    else
        FileSeek(f,-(int)sizeof(u32),SEEK_CUR);

    if(hdr.type == OGF_MT_NORMAL ||
     hdr.type == OGF_MT_PROGRESSIVE)
    {
        if(load_surface(out,hdr.type == OGF_MT_PROGRESSIVE,f) != 0)
            printf("Error loading OGF '%s'\n",path);
    }        
    else if(hdr.type == OGF_MT_HIERRARHY)
    {
        if(load_hierrarhy(out,f) != 0)
            printf("Error loading OGF '%s'\n",path);
    }
    else
        printf("Error loading OGF '%s'\nUnknown model type\n",path);
    

    out->bsphere.center.x = hdr.bs.center.x;
    out->bsphere.center.y = hdr.bs.center.y;
    out->bsphere.center.z = -hdr.bs.center.z;    
    out->bsphere.radius = hdr.bs.radius;

    out->bbox.min.x = hdr.bb.min.x;
    out->bbox.min.y = hdr.bb.min.y;
    out->bbox.min.z = -hdr.bb.min.z;
    out->bbox.max.x = hdr.bb.max.x;
    out->bbox.max.y = hdr.bb.max.y;
    out->bbox.max.z = -hdr.bb.max.z;

    FileClose(f);

    return out;    
}
