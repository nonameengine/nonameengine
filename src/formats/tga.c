#include <stdlib.h>
#include <stdio.h>
#include <string.h> /*for memcpy*/
#include "tga.h"

#if 1
#include "system/vfs.h"

#define fopen FileOpen
#define fclose FileClose
#define fread FileRead
#define fwrite FileWrite
#define fseek FileSeek
#define ftell FileTell
#define FILE File
#endif

static void un_rle(u8 *src, u8 *dest, u32 dest_length, int bpp)
{
    u32 pos = 0;
    int i;

    while(pos<dest_length)
    {
        u8 hdr = *src++;
        u8 len = (hdr & 0x7F) + 1;

        if(hdr & 0x80)
        {
            u8 c[4];
            for(i = 0; i < bpp; i++) c[i] = *src++;

            while(len > 0)
            {
                for(i = 0; i < bpp; i++)
                    dest[pos++] = c[i];

                len--;
            }
        }
        else
        {
            while(len > 0)
            {
                for(i = 0; i < bpp; i++)
                    dest[pos++] = *src++;

                len--;
            }
        }
    }
}
/* this not correspond to specification */
static u32 rle(u8 *src, u8 *dest, u32 src_len, int bpp)
{
    u32 len, pos;
    int i,j,k;

    union
    {
        u8 b[4];
        u32 dw;
    } c, n, u[127];

    len = pos = i = j = k = 0;
    c.dw = n.dw = 0;

    while(pos < src_len)
    {
        for(i = 0; i < bpp; i++)
            c.b[i] = src[pos++];
        if(pos < src_len)
            for(i = 0; i < bpp; i++)
                n.b[i] = src[pos+i];
        else
            break;

        if(n.dw == c.dw)
        {
            if(k)
            {
                int j;

                dest[len++] = k-1;

                for(i = 0; i < k; i++)
                    for(j = 0; j < bpp; j++)
                        dest[len++] = u[i].b[j];

                k = 0;
            }
            
            j++;

            if(j > 0x7F)
            {
                dest[len++] = 0x80 | j-1;

                for(i = 0; i < bpp; i++)
                    dest[len++] = c.b[i];

                j = 0;
            }
        }
        else
        {
            if(j)
            {
                j++;
                
                dest[len++] = 0x80 | j-1;

                for(i = 0; i < bpp; i++)
                    dest[len++] = c.b[i];

                j = 0;

                continue;
            }
            
            for(i = 0; i < bpp; i++)
                u[k].b[i] = c.b[i];
                
            k++;

            if(k > 0x7F)
            {
                int j;
                
                dest[len++] = k-1;

                for(i = 0; i < k; i++)
                    for(j = 0; j < bpp; j++)
                        dest[len++] = u[i].b[j];

                k = 0;
            }
        }
    }

    if(j)
    {
        j++;
        
        dest[len++] = 0x80 | j-1;

        for(i = 0; i < bpp; i++)
            dest[len++] = c.b[i];
    }
    else if(k)
    {
        int j;

        for(i = 0; i < bpp; i++)
            u[k].b[i] = c.b[i];

        k++;

        dest[len++] = k-1;

        for(i = 0; i < k; i++)
            for(j = 0; j < bpp; j++)
                dest[len++] = u[i].b[j];
    }

    return len;
}

int LoadTGA(pcstr filename, TgaImage *out)
{
    FILE *f;
    TgaHeader hdr;
    long size, filelength;

    f = fopen(filename,"rb");

    if(f == 0)
    {
        printf("Can't open file '%s'\n",filename);
        return 1;
    }

    fseek(f,0,SEEK_END); filelength = ftell(f); fseek(f,0,SEEK_SET);
    
    fread(&hdr,sizeof(hdr),1,f);

    fseek(f,hdr.id_length,SEEK_CUR);

    /*read color map*/
    if(hdr.image_type == TGA_INDEXED ||
    hdr.image_type == TGA_INDEXED_RLE)
    {
        size = hdr.color_map_length * (hdr.color_map_entry_size / 8);
        out->palette = malloc(size);
        fread(out->palette,size,1,f);
    }
    else
    {
        out->palette = 0;
    }

    /*read bitmap*/
    size = hdr.width * hdr.height * (hdr.pixel_size / 8);
    out->pixels = malloc(size);
    
    if(hdr.image_type == TGA_INDEXED ||
     hdr.image_type == TGA_TRUECOLOR ||
     hdr.image_type == TGA_GRAYSCALE) /*just read pixels*/
    {
        fread(out->pixels,size,1,f);
    }
    else if(hdr.image_type == TGA_INDEXED_RLE ||
    hdr.image_type == TGA_TRUECOLOR_RLE ||
    hdr.image_type == TGA_GRAYSCALE_RLE) /* unpack RLE */
    {
        size_t raw_data_len = filelength - ftell(f);
        u8* raw_data = malloc(raw_data_len);

        fread(raw_data,raw_data_len,1,f);

        un_rle(raw_data,out->pixels,size,hdr.pixel_size / 8);

        free(raw_data);
    }
    else
    {
        printf("Unknown TGA '%s' image format\n", filename);
        free(out->pixels);
        free(out->palette);

        fclose(f);

        return 1;
    }

    out->header = hdr;

    fclose(f);

    return 0;
}

void SaveTGA(pcstr filename, TgaImage *img)
{
    FILE *f = fopen(filename,"wb");

    if(!f)
    {
        printf("Can't open file '%s'\n",filename);
        return;
    }
/*
    switch(img->header.image_type)
    {
        case TGA_INDEXED_RLE:
            img->header.image_type = TGA_INDEXED;
        break;
        case TGA_TRUECOLOR_RLE:
            img->header.image_type = TGA_TRUECOLOR;
        break;
        case TGA_GRAYSCALE_RLE:
            img->header.image_type = TGA_GRAYSCALE;
        break;
    }
*/

    fwrite(&img->header,sizeof(TgaHeader),1,f);

    if(img->palette)
        fwrite(img->palette,img->header.color_map_entry_size / 8,img->header.color_map_length,f);

    if(img->header.image_type & 0x8) /* is run length encoded */
    {
        int bpp = img->header.pixel_size / 8; 
        u32 size = img->header.width * img->header.height;
        
        void *encoded = malloc(size * bpp);

        size = rle(img->pixels,encoded,size*bpp,bpp);
        fwrite(encoded,size,1,f);

        free(encoded);
    }
    else
        fwrite(img->pixels,img->header.width * img->header.height * (img->header.pixel_size / 8),1,f);

    fclose(f);
}

void FlipVertical(u8 *data, u16 width, u16 height, int bpp)
{
    u8 c;
    u16 w, h;
    int i;

    bpp /= 8;

    for(h = 0; h < height; h++)
        for(w = 0; w < width/2; w++)
            for(i = 0; i < bpp; i++)
            {
                c = data[(width * h + w) * bpp + i];
                data[(width * h + w) * bpp + i] = data[(width * h + (width-1-w)) * bpp + i];
                data[(width * h + (width-1-w)) * bpp + i] = c;
            }
    
/*
    for(h = 0; h < height; h++)
        for(w = 0; w < width/2; w++)
        {
            for(i = 0; i < bpp; i++)
                c[i] = data[(width * h + w) * bpp + i];
            for(i = 0; i < bpp; i++)
                data[(width * h + w) * bpp + i] = data[(width * h + (width-1-w)) * bpp + i];
            for(i = 0; i < bpp; i++)
                data[(width * h + (width-1-w)) * bpp + i] = c[i];
        }
*/         
}

void FlipHorizontal(u8 *data, u16 width, u16 height, int bpp)
{
/*************************************************    
    u8 c;
    u16 w, h;
    int i;

    bpp /= 8;
    
    for(w = 0; w < width; w++)
        for(h = 0; h < height/2; h++)
            for(i = 0; i < bpp; i++)
            {
                c = data[(width * h + w) * bpp + i];
                data[(width * h + w) * bpp + i] = data[((height-1-h) * width + w) * bpp + i];
                data[((height-1-h) * width + w) * bpp + i] = c;
            }
*************************************************/

/*
    for(w = 0; w < width; w++)
        for(h = 0; h < height/2; h++)
        {
            for(i = 0; i < bpp; i++)
                c[i] = data[(width * h + w) * bpp + i];
            for(i = 0; i < bpp; i++)
                data[(width * h + w) * bpp + i] = data[((height-1-h) * width + w) * bpp + i];
            for(i = 0; i < bpp; i++)
                data[((height-1-h) * width + w) * bpp + i] = c[i];
        }
*/ 

    u16 i;
    u8 *scanline;
    u32 scansize;

    bpp /= 8;
    scansize = bpp * width;
    
    scanline = malloc(scansize);

    for(i = 0; i < height / 2; i++)
    {
        memcpy(scanline,data+scansize*i,scansize);
        memcpy(data+scansize*i,data+(height-1-i)*scansize,scansize);
        memcpy(data+(height-1-i)*scansize,scanline,scansize);
    }

    free(scanline); 
}

void UnpackIndexedImage(u8 *data, u8 *palette, u8 *out, int bpp, u32 count)
{
    u32 i;
    int j;
    
    bpp /= 8;

    for(i = 0; i < count; i++)
    {
        for(j = 0; j < bpp; j++)
            *out++ = palette[data[i]*bpp+j];
    }
}

void SwapRandBComponents(u8 *pixels, int bpp, u32 count)
{
    u32 i;
    u8 c;

    bpp /= 8;

    for(i = 0; i < count; i++)
    {
        c = pixels[0];
        pixels[0] = pixels[2];
        pixels[2] = c;

        pixels += bpp;
    }
}
