#ifndef MAIN_H
#define MAIN_H

typedef struct WindowInfo
{
    int width, height;
} WindowInfo;

extern WindowInfo Window;
extern unsigned poly_count;

void Reshape(void);
void Display(void);
void Init(void);
void Exit(void);

#endif
