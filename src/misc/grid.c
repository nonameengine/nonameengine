#include "grid.h"

GLuint grid = 0;

void InitGrid(int size)
{
    int i;

    grid = glGenLists(1);

    glNewList(grid,GL_COMPILE);
    /*grid*/
    glLineWidth(1.0);            
    glBegin(GL_LINES);
        glColor3f(1.0,1.0,1.0);
        for(i = -size; i < size + 1; i++)
        {
            glVertex3f(i,0,size);
            glVertex3f(i,0,-size);

            glVertex3f(size,0,i);
            glVertex3f(-size,0,i);
        }
    glEnd();

    /*axis*/
    glLineWidth(2.0);
    glBegin(GL_LINES);

        glColor3f(1,0,0);
        glVertex3f(0,0.01,0);
        glVertex3f(1,0.01,0);

        glColor3f(0,1,0);
        glVertex3f(0,0.01,0);
        glVertex3f(0,1.01,0);

        glColor3f(0,0,1);
        glVertex3f(0,0.01,0);
        glVertex3f(0,0.01,1);
    glEnd();

    glEndList();
}

void DrawGrid(void)
{
        glCallList(grid);
}

void DeleteGrid(void)
{
        glDeleteLists(grid,1);
        grid = 0;
}


