#ifndef LENS_FLARE_H
#define LENS_FLARE_H

#include "vector.h"

void InitFlares(void);
void DrawLensFlares(Vector2f lightpos);
void UpdateDepthBufferForFlares(void);
void DeleteFlares(void);

#endif
