#ifndef GRID_H
#define GRID_H

#ifdef _WIN32
#include <windows.h>
#endif
#include <GL/gl.h>

void InitGrid(int size);

void DrawGrid(void);

void DeleteGrid(void);

#endif
