#ifndef SKY_H
#define SKY_H

void InitSky(void);
void DrawSky(void);
void DeleteSky(void);

#endif
