#include <stdio.h>
#include <stdarg.h>
#include <stdlib.h>
#include <time.h>
#ifdef _WIN32
#include <windows.h>
#endif

#include "height.h"

struct TerrainModel* GenerateTerrain(u8 *height_map, u16 size_x, u16 size_y)
{
        struct TerrainModel *out;
        unsigned x, y;
        float weight = 15.f;

        time_t t = clock();

        out = malloc(sizeof(struct TerrainModel));

        out->vertex_count = size_x * size_y;
        out->face_count = (size_x - 1) * (size_y - 1);

        out->vertices = malloc(sizeof(struct TerrainVertex) * out->vertex_count);
        out->indices = malloc(sizeof(struct TerrainFace) * out->face_count);
        /*vertices*/
        for(x = 0; x < size_x; x++)
        {
                for(y = 0; y < size_y; y++)
                {
                        out->vertices[y * size_x + x].x = x;
                        out->vertices[y * size_x + x].y = (float)height_map[x * size_x + y] / 255 * weight;
                        out->vertices[y * size_x + x].z = y;
                }
        }
        /*faces*/
        
        for(x = 0; x < (size_x - 1); x++)
                for(y = 0; y < (size_y - 1); y++)
                {
                    struct TerrainFace * f = &out->indices[y * (size_x-1) + x];
                    f->v1 = y * size_x + x;
                    f->v2 = y * size_x + x + 1;
                    f->v3 = (y + 1) * size_x + x + 1;
                    f->v4 = (y + 1) * size_x + x;
                }
        
        /* triangle variant
        for(x = 0; x < size_x - 1; x++)
            for(y = 0; y < size_y - 1; y++)
            {
                struct TerrainFace *f = &out->indices[i++];

                f->v1 = y * (size_x) + x;
                f->v2 = y * (size_x) + x + 1;
                f->v3 = (y+1) * (size_x) + x;

                f = &out->indices[i++];

                f->v1 = y * (size_x) + x + 1;
                f->v2 = (y+1) * (size_x) + x + 1;
                f->v3 = (y+1) * (size_x) + x;

            }
        */
        printf("time %f\n",(float) (clock() - t) / CLOCKS_PER_SEC);

        return out;
}

void SaveOBJ(char *file_name, struct TerrainModel *src)
{
        unsigned i;
        FILE *f = fopen(file_name,"wb");

        for(i = 0; i < src->vertex_count; i++)
        {
                fprintf(f,"v %f %f %f\n", src->vertices[i].x, src->vertices[i].y, src->vertices[i].z);
        }

        for(i = 0; i < src->face_count; i++)
        {
                fprintf(f,"f %u %u %u %u\n", 
                src->indices[i].v1 + 1, 
                src->indices[i].v2 + 1, 
                src->indices[i].v3 + 1,
                src->indices[i].v4 + 1);
        }

        fclose(f);
}
