#ifndef HEIGHT_H
#define HEIGHT_H

#include "types.h"

struct TerrainVertex
{
        float x, y, z;
};

struct TerrainFace
{
        unsigned v1, v2, v3, v4;
};

struct TerrainModel
{
        unsigned vertex_count;
        unsigned face_count;

        struct TerrainVertex *vertices;
        struct TerrainFace *indices;
};

struct TerrainModel* GenerateTerrain(u8 *height_map, u16 size_x, u16 size_y);
void SaveOBJ(char *file_name, struct TerrainModel *src);

#endif
