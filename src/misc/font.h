#ifndef FONT_H
#define FONT_H

#ifdef _WIN32
#include <windows.h>
#endif
#include <stdio.h>
#include <GL/glu.h>

void InitFont(void);
void DisplayText(int x, int y, char *format, ...);
void DisplayMessage(int w, int h, char *text);
void DeleteFont(void);

#endif
