#include <string.h>
#include <stdarg.h>
#include <malloc.h>
#include "font.h"
#include "main.h"

#ifdef __BORLANDC__
#pragma hdrstop
#endif
#include "vgafont.h"

GLuint font;

#ifdef _MSC_VER
#define vsnprintf(buf,size,format,v) vsprintf(buf,format,v)
#endif

void InitFont(void)
{
        int c;
        font = glGenLists(256);

        if(!font)
        {
                printf("glGenLists failed!\n");
                return;
        }

        glPixelStorei(GL_UNPACK_ALIGNMENT,1);
        for(c = 0; c < 255; c++)
        {
            glNewList(font + c, GL_COMPILE);
            
            if(c == '\t')
                glBitmap(8, 16, 0, 16, 8*4, 0, vgafont16 + ' ' * 16);
            else
                glBitmap(8, 16, 0, 16, 8, 0, vgafont16 + c * 16);
                
            glEndList();
        }
}

void DisplayText(int x, int y, char *format, ...)
{
    char *str = alloca(1024);
    va_list v;

    va_start(v, format);
    vsnprintf(str,1024,format,v);

    glRasterPos2i(x,y);
    
    while(*str != 0)
    {
        if(*str == '\n')
            glRasterPos2i(x, y+=16);
        else
            glCallList(font + *str);

        str++;
    }

    va_end(v);
}

void DisplayMessage(int w, int h, char *text)
{
    int x, y;
    int i;

    x = Window.width / 2 - w*8 / 2;
    y = Window.height / 2 - h*16 / 2;

    /* draw rect */
    glColor3f(1.0,1.0,1.0);
    glRecti(x,y,x+w*8,y+h*16);

    /* draw frame */
    glColor3f(0.0,0.0,0.0);
    glRasterPos2i(x,y+16);

    glCallList(font + 200);

    for(i = 1; i < w-1; i++)
        glCallList(font + 205);

    glCallList(font + 188);

    for(i = 1; i < h-1; i++)
    {
        glRasterPos2i(x,y+16+i*16);
        glCallList(font + 186);

        glRasterPos2i(x+(w-1)*8, y+16+i*16);
        glCallList(font + 186);
    }

    glRasterPos2i(x,y+16+(h-1)*16);

    glCallList(font + 201);

    for(i = 1; i < w-1; i++)
        glCallList(font + 205);

    glCallList(font + 187);

    /* draw text */
    x += 8;
    y += (h-1)*16;

    i = 0;
    
    glRasterPos2i(x,y);

    while(*text)
    {
        if(*text == '\n')
        {
            glRasterPos2i(x,y-=16);
            i = 0;
        }
        else
        {
            if(*text == '\t')
                i += 4;
            else
                i++;

            if(i > w-2)
            {
                glRasterPos2i(x,y-=16);
                i = 0;
            }

            glCallList(font + *text);
        }

        text++;
    }
}

void DeleteFont(void)
{
    glDeleteLists(font,256);
}

