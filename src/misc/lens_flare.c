#ifdef _WIN32
#include <windows.h>
#endif
#include <stdlib.h>
#include <GL/gl.h>

#include "lens_flare.h"
#include "resource_manager.h"
#include "main.h"

#define FLARE_COUNT 11

typedef struct Flare
{
    Resource *texture;
    float pos;
    Vector4f color;
    float halfsize;
} Flare;

static Flare flares[FLARE_COUNT];
static GLfloat *depth;

void InitFlares(void)
{
    flares[8].texture = GetResource("flare\\flare_0",RT_TEXTURE);
    flares[8].pos = -0.35;
    flares[8].color = Vector4f_set(0.8,0.6,0.2,0.35);
    flares[8].halfsize = 18;

    flares[7].texture = GetResource("flare\\flare_1",RT_TEXTURE);
    flares[7].pos = -0.25;
    flares[7].color = Vector4f_set(0.8,0.6,0.2,0.3);
    flares[7].halfsize = 11;
    
    flares[6].texture = GetResource("flare\\flare_2",RT_TEXTURE);
    flares[6].pos = -0.2;
    flares[6].color = Vector4f_set(0.8,0.6,0.2,0.5);
    flares[6].halfsize = 45;
    
    flares[5].texture = GetResource("flare\\flare_sun",RT_TEXTURE);
    flares[5].pos = 0.0;
    flares[5].color = Vector4f_set(0.8,0.6,0.2,0.8);
    flares[5].halfsize = 100;

    flares[10].texture = GetResource("flare\\flare_2",RT_TEXTURE);
    flares[10].pos = 0.2;
    flares[10].color = Vector4f_set(0.8,0.6,0.2,0.45);
    flares[10].halfsize = 34;
    
    flares[0].texture = GetResource("flare\\flare_1",RT_TEXTURE);
    flares[0].pos = 0.4;
    flares[0].color = Vector4f_set(0.8,0.6,0.2,0.4);
    flares[0].halfsize = 38;

    flares[9].texture = GetResource("flare\\flare_2",RT_TEXTURE);
    flares[9].pos = 0.6;
    flares[9].color = Vector4f_set(0.8,0.6,0.2,0.5);
    flares[9].halfsize = 40;

    flares[1].texture = GetResource("flare\\flare_0",RT_TEXTURE);
    flares[1].pos = 0.8;
    flares[1].color = Vector4f_set(0.8,0.6,0.2,0.55);
    flares[1].halfsize = 50;

    flares[2].texture = GetResource("flare\\flare_1",RT_TEXTURE);
    flares[2].pos = 1.10;
    flares[2].color = Vector4f_set(0.8,0.6,0.2,0.6);
    flares[2].halfsize = 12;

    flares[3].texture = GetResource("flare\\flare_0",RT_TEXTURE);
    flares[3].pos = 1.20;
    flares[3].color = Vector4f_set(0.8,0.6,0.2,0.55);
    flares[3].halfsize = 20;

    flares[4].texture = GetResource("flare\\flare_0",RT_TEXTURE);
    flares[4].pos = 1.28;
    flares[4].color = Vector4f_set(0.8,0.6,0.2,0.3);
    flares[4].halfsize = 10;
}

void UpdateDepthBufferForFlares(void)
{
    if(depth)
        free(depth);

    depth = malloc(Window.width * Window.height * sizeof(GLfloat));
}

void DrawLensFlares(Vector2f lightpos)
{
    int i;
    Vector2f dir, p;
    Vector3f color;

    dir = Vector2f_sub(
        Vector2f_set(Window.width / 2, Window.height / 2),
        lightpos
    );

    if((int)lightpos.x < 0 || (int)lightpos.x > Window.width)
        return;
    if((int)lightpos.y < 0 || (int)lightpos.y > Window.height)
        return;

    glReadPixels(0,0,Window.width,Window.height,GL_DEPTH_COMPONENT,GL_FLOAT,depth);

    if(depth[(int)lightpos.y * Window.width + (int)lightpos.x] < 1.0)
    {
        return;
    }
    
/*
    glColor3f(1.0,0.0,0.0);
    glPointSize(5.0);

    glBegin(GL_POINTS);
    glVertex2fv((float*)&lightpos);
    glEnd();
*/
    glDisable(GL_CULL_FACE);

    glEnable(GL_TEXTURE_2D);
    glEnable(GL_BLEND);

    #ifdef USE_IBM_OPENGL_11_FIX
    glBlendFunc(GL_SRC_ALPHA,GL_ONE_MINUS_SRC_ALPHA);
    #else
    glBlendFunc(GL_ONE,GL_ONE);
    #endif

    for(i = 0; i < FLARE_COUNT; i++)
    {
        p = Vector2f_add(
            lightpos,
            Vector2f_muln(dir,flares[i].pos)
        );

        color = Vector3f_muln(Vector4f_xyz(flares[i].color),flares[i].color.a * 0.5);
        
        BindTexture(flares[i].texture);
        glColor3fv((float*)&color);       
        glBegin(GL_QUADS);

        glTexCoord2f(0.0,0.0);
        glVertex2f(p.x - flares[i].halfsize, p.y - flares[i].halfsize);
        
        glTexCoord2f(1.0,0.0);
        glVertex2f(p.x + flares[i].halfsize, p.y - flares[i].halfsize);
        
        glTexCoord2f(1.0,1.0);
        glVertex2f(p.x + flares[i].halfsize, p.y + flares[i].halfsize);
        
        glTexCoord2f(0.0,1.0);
        glVertex2f(p.x - flares[i].halfsize, p.y + flares[i].halfsize);

        glEnd();
    }

    glDisable(GL_BLEND);
    glDisable(GL_TEXTURE_2D);
    
}

void DeleteFlares(void)
{
    int i;

    for(i = 0; i < FLARE_COUNT; i++)
    {
        ReleaseResource(flares[i].texture);
    }

    free(depth);
}
