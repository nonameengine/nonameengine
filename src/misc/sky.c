#ifdef _WIN32
#include <windows.h>
#endif
#include <GL/gl.h>
#include "resource_manager.h"
#include "flying_camera.h"
#include "sky.h"

Resource *t_fr, *t_bk, *t_lf, *t_rt, *t_up;
GLuint sky;
int sky_range = 1;

void InitSky(void)
{
        t_fr = GetResource("sky\\sky_1_fr",RT_TEXTURE);
        t_bk = GetResource("sky\\sky_1_bk",RT_TEXTURE);
        t_lf = GetResource("sky\\sky_1_lf",RT_TEXTURE);
        t_rt = GetResource("sky\\sky_1_rt",RT_TEXTURE);
        t_up = GetResource("sky\\sky_1_up",RT_TEXTURE);
        /*t_dn = GetResource("sky\\sky_1_dn",RT_TEXTURE);*/
}

void DrawSky(void)
{
        glDisable(GL_FOG);
        glDisable(GL_DEPTH_TEST);
        glDisable(GL_ALPHA_TEST);
        glDisable(GL_CULL_FACE);
        
        glEnable(GL_TEXTURE_2D);
        
        glMatrixMode(GL_MODELVIEW);
        glPushMatrix();
        glLoadIdentity();
        
        glRotatef(Camera.rotation.x,1,0,0);
        glRotatef(Camera.rotation.y,0,1,0);
        glRotatef(Camera.rotation.z,0,0,1);

        glColor3f(1,1,1);
        
        /* front */
        BindTexture(t_fr);
        
        glBegin(GL_QUADS);
        
        glTexCoord2i(0,0);
        glVertex3i(-sky_range,sky_range,-sky_range);
        
        glTexCoord2i(1,0);
        glVertex3i(sky_range,sky_range,-sky_range);

        glTexCoord2i(1,1);
        glVertex3i(sky_range,0,-sky_range);
        
        glTexCoord2i(0,1);
        glVertex3i(-sky_range,0,-sky_range);

        glEnd();
        
        /* back */
        BindTexture(t_bk);
        
        glBegin(GL_QUADS);
        
        glTexCoord2i(1,0);
        glVertex3i(-sky_range,sky_range,sky_range);
        
        glTexCoord2i(0,0);
        glVertex3i(sky_range,sky_range,sky_range);
        
        glTexCoord2i(0,1);
        glVertex3i(sky_range,0,sky_range);
        
        glTexCoord2i(1,1);
        glVertex3i(-sky_range,0,sky_range);
        
        glEnd();
        
        /* left */
        BindTexture(t_lf);
        
        glBegin(GL_QUADS);
        
        glTexCoord2i(0,0);
        glVertex3i(-sky_range,sky_range,sky_range);
        
        glTexCoord2i(1,0);
        glVertex3i(-sky_range,sky_range,-sky_range);
        
        glTexCoord2i(1,1);
        glVertex3i(-sky_range,0,-sky_range);
        
        glTexCoord2i(0,1);
        glVertex3i(-sky_range,0,sky_range);
        
        glEnd();
        
        /* right */
        BindTexture(t_rt);
        
        glBegin(GL_QUADS);
        
        glTexCoord2i(1,0);
        glVertex3i(sky_range,sky_range,sky_range);
        
        glTexCoord2i(0,0);
        glVertex3i(sky_range,sky_range,-sky_range);
        
        glTexCoord2i(0,1);
        glVertex3i(sky_range,0,-sky_range);
        
        glTexCoord2i(1,1);
        glVertex3i(sky_range,0,sky_range);
        
        glEnd();
        
        /* top */
        BindTexture(t_up);
        
        glBegin(GL_QUADS);
        
        glTexCoord2i(0,0);
        glVertex3i(sky_range,sky_range,sky_range);
        
        glTexCoord2i(0,1);
        glVertex3i(-sky_range,sky_range,sky_range);
        
        glTexCoord2i(1,1);
        glVertex3i(-sky_range,sky_range,-sky_range);
        
        glTexCoord2i(1,0);
        glVertex3i(sky_range,sky_range,-sky_range);
        
        glEnd();

/*              bottom      
        glBindTexture(GL_TEXTURE_2D,GetTexture(t_dn));
        
        glBegin(GL_QUADS);
        
        glTexCoord2i(0,0);
        glVertex3i(sky_range,-sky_range,sky_range);
        
        glTexCoord2i(1,0);
        glVertex3i(-sky_range,-sky_range,sky_range);
        
        glTexCoord2i(1,1);
        glVertex3i(-sky_range,-sky_range,-sky_range);
        
        glTexCoord2i(0,1);
        glVertex3i(sky_range,-sky_range,-sky_range);
        
        glEnd();
*/  
        
        glPopMatrix();
}

void DeleteSky(void)
{
        glDeleteLists(sky,1);
        
        ReleaseResource(t_fr);
        ReleaseResource(t_bk);
        ReleaseResource(t_lf);
        ReleaseResource(t_rt);
        ReleaseResource(t_up);
        /*ReleaseResource(t_dn);*/
}
