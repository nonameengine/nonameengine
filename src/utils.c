#include "utils.h"

void r_string(char *out, int len, File *f)
{
    char c;

    do
    {
        FileRead(&c,sizeof(char),1,f);
        *out++ = c;
    }
    while(c != '\0' && len--);
}
