#ifndef FLYING_CAMERA_H
#define FLYING_CAMERA_H

#include "types.h"
#include "vector.h"

/* this should not live here */
typedef struct Plane
{
    Vector3f normal;
    float dist;
} Plane;

typedef struct Frustum
{
    Plane planes[6];
} Frustum;

typedef struct FlyingCamera
{
    Vector3f position;
    Vector3f rotation;

    Frustum frustum;
    float speed, sensitivity;
} FlyingCamera;

enum CameraMove
{
    CAMERA_MOVE_FORWARD,
    CAMERA_MOVE_BACKWARD,
    CAMERA_MOVE_LEFT,
    CAMERA_MOVE_RIGHT
};

extern FlyingCamera Camera;

void MoveCamera(int action);
void RotateCamera(int x, int y);

void UpdateCameraFrustum(void);

#endif
