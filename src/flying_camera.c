#ifdef _WIN32
#include <windows.h>
#endif
#include <GL/gl.h>
#include <math.h>

#include "flying_camera.h"
#include "scene.h"
#include "collision.h"

const double TO_RADS = 3.1415926535897932384626433832795 / 180;

FlyingCamera Camera;

Vector3f do_collision(Vector3f from, Vector3f to)
{ 
    return Vector3f_muln(Vector3f_normalize(Vector3f_sub(to,from)),0.01);
}

void UpdateCameraFrustum(void)
{
    int i;
    Matrix4x4 p,mv,mvp;

    glGetFloatv(GL_PROJECTION_MATRIX,(float*)&p);
    glGetFloatv(GL_MODELVIEW_MATRIX,(float*)&mv);

    mvp = Matrix4x4_mul(mv,p);

    /* left */
    Camera.frustum.planes[0].normal.x = mvp.m[0][3] + mvp.m[0][0];
    Camera.frustum.planes[0].normal.y = mvp.m[1][3] + mvp.m[1][0];
    Camera.frustum.planes[0].normal.z = mvp.m[2][3] + mvp.m[2][0];
    Camera.frustum.planes[0].dist = mvp.m[3][3] + mvp.m[3][0];

    /* right */
    Camera.frustum.planes[1].normal.x = mvp.m[0][3] - mvp.m[0][0];
    Camera.frustum.planes[1].normal.y = mvp.m[1][3] - mvp.m[1][0];
    Camera.frustum.planes[1].normal.z = mvp.m[2][3] - mvp.m[2][0];
    Camera.frustum.planes[1].dist = mvp.m[3][3] - mvp.m[3][0];

    /* bottom */
    Camera.frustum.planes[2].normal.x = mvp.m[0][3] + mvp.m[0][1];
    Camera.frustum.planes[2].normal.y = mvp.m[1][3] + mvp.m[1][1];
    Camera.frustum.planes[2].normal.z = mvp.m[2][3] + mvp.m[2][1];
    Camera.frustum.planes[2].dist = mvp.m[3][3] + mvp.m[3][1];

    /* top */
    Camera.frustum.planes[3].normal.x = mvp.m[0][3] - mvp.m[0][1];
    Camera.frustum.planes[3].normal.y = mvp.m[1][3] - mvp.m[1][1];
    Camera.frustum.planes[3].normal.z = mvp.m[2][3] - mvp.m[2][1];
    Camera.frustum.planes[3].dist = mvp.m[3][3] - mvp.m[3][1];

    /* near */
    Camera.frustum.planes[4].normal.x = mvp.m[0][3] + mvp.m[0][2];
    Camera.frustum.planes[4].normal.y = mvp.m[1][3] + mvp.m[1][2];
    Camera.frustum.planes[4].normal.z = mvp.m[2][3] + mvp.m[2][2];
    Camera.frustum.planes[4].dist = mvp.m[3][3] + mvp.m[3][2];

    /* far */
    Camera.frustum.planes[5].normal.x = mvp.m[0][3] - mvp.m[0][2];
    Camera.frustum.planes[5].normal.y = mvp.m[1][3] - mvp.m[1][2];
    Camera.frustum.planes[5].normal.z = mvp.m[2][3] - mvp.m[2][2];
    Camera.frustum.planes[5].dist = mvp.m[3][3] - mvp.m[3][2];

    /* normalize planes */
    for(i = 0; i < 6; i++)
    {
        float m = sqrt(Camera.frustum.planes[i].normal.x * Camera.frustum.planes[i].normal.x +
        Camera.frustum.planes[i].normal.y * Camera.frustum.planes[i].normal.y + 
        Camera.frustum.planes[i].normal.z * Camera.frustum.planes[i].normal.z);

        if(m != 0.0)
        {
            Camera.frustum.planes[i].normal.x /= m;
            Camera.frustum.planes[i].normal.y /= m;
            Camera.frustum.planes[i].normal.z /= m;
            Camera.frustum.planes[i].dist /= m;
        }
        else
        {
            Camera.frustum.planes[i].normal.x = 0.0;
            Camera.frustum.planes[i].normal.y = 0.0;
            Camera.frustum.planes[i].normal.z = 0.0;
            Camera.frustum.planes[i].dist = 0.0;
        }
    }
}

void RotateCamera(int x, int y)
{
    Camera.rotation.x += y * Camera.sensitivity;
    Camera.rotation.y += x * Camera.sensitivity;

    if(Camera.rotation.x > 90.0)
        Camera.rotation.x = 90.0;

    if(Camera.rotation.x < -90.0)
        Camera.rotation.x = -90.0;

    if(Camera.rotation.y > 360.0)
        Camera.rotation.y = 0.0;

    if(Camera.rotation.y < 0.0)
        Camera.rotation.y = 360.0;
}

void MoveCamera(int action)
{
    CollisionObject col;
    
    Vector3f movement;

    float sin_x_rot = sin(Camera.rotation.x * TO_RADS);
/*    float cos_x_rot = cos(Camera.rotation.x * TO_RADS); */

    float sin_y_rot = sin(Camera.rotation.y * TO_RADS);
    float cos_y_rot = cos(Camera.rotation.y * TO_RADS);
    
    switch(action)
    {
        case CAMERA_MOVE_FORWARD:
            movement.x = sin_y_rot;
            movement.y = -sin_x_rot;
            movement.z = -cos_y_rot;
        break;
        case CAMERA_MOVE_BACKWARD:
            movement.x = -sin_y_rot;
            movement.y = sin_x_rot;
            movement.z = cos_y_rot;
        break;
        case CAMERA_MOVE_LEFT:
            movement.x = -cos_y_rot;
            movement.y = 0.0;
            movement.z = -sin_y_rot;
        break;
        case CAMERA_MOVE_RIGHT:
            movement.x = cos_y_rot;
            movement.y = 0.0;
            movement.z = sin_y_rot;
        break;
    }

    movement = Vector3f_muln(Vector3f_normalize(movement),Camera.speed);
    movement = Vector3f_add(Camera.position,movement);

    col.type = COLLISION_SPHERE;
    col.bsphere.radius = 0.5;
    col.bsphere.center = movement;

    if(Scene)
        while(1)
        {
            Vector3f c;
            const CollisionObject *obj = TestCollision(col);

            if(!obj)
                break;
            
            if(obj->type == COLLISION_BOX)
            {
                if(BBoxContainsPoint(obj->bbox,movement) == OUTSIDE)
                {
                    movement = Vector3f_add(movement,do_collision(
                    Vector3f_max(Vector3f_min(movement,obj->bbox.min),obj->bbox.max),
                    movement));
                }
                else
                {
                    c = GetBBoxCenter(obj->bbox);

                    if(c.x==movement.x&&
                       c.y==movement.y&&
                       c.z==movement.z)
                        break;
                        
                    movement = Vector3f_add(movement,do_collision(c,movement));
                }
            }
            else if(obj->type == COLLISION_SPHERE)
            {
                movement = Vector3f_add(movement,do_collision(obj->bsphere.center,movement));
            }

            col.bsphere.center = movement;
        }

    Camera.position = movement;
}
