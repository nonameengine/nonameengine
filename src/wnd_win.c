#include <windows.h>
#include <stdio.h>

#include "scene.h"
#include "gl_extensions.h"
#include "flying_camera.h"
#include "system/settings.h"

#include "main.h"
#include "resource.h"

#define INPUT_TIMER 0x1
#define WNDCLASS_NAME "mywndclass_0001"

HDC hDC;
HGLRC hRC;

HPALETTE ghPalette,ghpalOld;

#pragma link "js32.lib"

LRESULT CALLBACK WndProc(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
    static int old_x = 0, old_y = 0;
    static int need_redisplay = 0;
    
    switch(msg)
    {
        case WM_SIZE:
            Window.width = LOWORD(lParam);
            Window.height = HIWORD(lParam);

            Reshape();
            
        return 0;
        
        case WM_DESTROY:  
            PostQuitMessage(0);
        return 0;

        case WM_CHAR:
            if(wParam == VK_ESCAPE)
                PostMessage(hWnd,WM_CLOSE,0,0);       
        return 0;

        /*
         The WM_QUERYNEWPALETTE message informs a window that it is about to
         receive input focus. In response, the window receiving focus should
         realize its palette as a foreground palette and update its client
         area. If the window realizes its palette, it should return TRUE;
         otherwise, it should return FALSE.
        */
        case WM_QUERYNEWPALETTE:
        {
            HDC     hDC;

            if(ghPalette)
            {
                hDC = GetDC(hWnd);

                /* Select and realize the palette */

                ghpalOld = SelectPalette(hDC, ghPalette, FALSE);
                RealizePalette(hDC);

                /* Redraw the client area */

                InvalidateRect(hWnd, NULL, TRUE);
                UpdateWindow(hWnd);

                if(ghpalOld)
                    SelectPalette(hDC, ghpalOld, FALSE);

                ReleaseDC(hWnd, hDC);

                return TRUE;
            }

            return FALSE;
        }
        /*
         The WM_PALETTECHANGED message informs all windows that the window
         with input focus has realized its logical palette, thereby changing 
         the system palette. This message allows a window without input focus
         that uses a color palette to realize its logical palettes and update
         its client area.
        
         This message is sent to all windows, including the one that changed
         the system palette and caused this message to be sent. The wParam of
         this message contains the handle of the window that caused the system
         palette to change. To avoid an infinite loop, care must be taken to
         check that the wParam of this message does not match the window's
         handle.
        */
        case WM_PALETTECHANGED:
        {
            HDC         hDC; 
            /*
             Before processing this message, make sure we
             are indeed using a palette
            */
            if (ghPalette)
            {
                /*
                 If this application did not change the palette, select
                 and realize this application's palette
                */
                if (wParam != (WPARAM)hWnd)
                {
                    /* Need the window's DC for SelectPalette/RealizePalette */

                    hDC = GetDC(hWnd);

                    /* Select and realize our palette */

                    ghpalOld = SelectPalette(hDC, ghPalette, FALSE);
                    RealizePalette(hDC);
                    /*
                     WHen updating the colors for an inactive window,
                     UpdateColors can be called because it is faster than
                     redrawing the client area (even though the results are
                     not as good)
                    */
                    UpdateColors(hDC);

                    /* Clean up */

                    if (ghpalOld)
                       SelectPalette(hDC, ghpalOld, FALSE);

                    ReleaseDC(hWnd, hDC);
                }
            }
            break;
        }
        
        case WM_COMMAND:
            if(!HIWORD(wParam))
                switch(LOWORD(wParam))
                {
                    case IDM_MAINMENU_EXIT:
                        PostMessage(hWnd,WM_CLOSE,0,0);
                    break;
                    case IDM_MAINMENU_OPEN:
                    {
                        OPENFILENAME ofn;
                        char path[MAX_PATH];

                        memset(&ofn,0,sizeof(ofn));

                        ofn.lStructSize = sizeof(ofn);
                        ofn.lpstrFile = path;
                        ofn.lpstrFile[0] = '\0';
                        ofn.hwndOwner = hWnd;
                        ofn.nMaxFile = sizeof(path);
                        ofn.lpstrFilter = "Map (*.cfg)\0*.CFG\0";
                        ofn.nFilterIndex = 1;
                        ofn.Flags = OFN_PATHMUSTEXIST|OFN_FILEMUSTEXIST;

                        if(GetOpenFileName(&ofn))
                        {
                            FreeScene();
                            LoadScene(ofn.lpstrFile);
                        }
                    }
                    break;
                }
        return 0;

        case WM_MOUSEMOVE:

            if(wParam & MK_LBUTTON)
            {
                RotateCamera(LOWORD(lParam) - old_x, HIWORD(lParam) - old_y);

                need_redisplay = 1;
            }
            
            old_x = LOWORD(lParam);
            old_y = HIWORD(lParam);
                
        return 0;

        case WM_TIMER:
            if(GetActiveWindow() == NULL)
                return 0;
                        
            if(wParam == INPUT_TIMER)
            {
                if(GetAsyncKeyState(VK_NUMPAD8) & 0x8000)
                {
                    RotateCamera(0,5);
                    need_redisplay = 1;
                }
                if(GetAsyncKeyState(VK_NUMPAD2) & 0x8000)
                {
                    RotateCamera(0,-5);
                    need_redisplay = 1;
                }
                if(GetAsyncKeyState(VK_NUMPAD4) & 0x8000)
                {
                    RotateCamera(-5,0);
                    need_redisplay = 1;
                }
                if(GetAsyncKeyState(VK_NUMPAD6) & 0x8000)
                {
                    RotateCamera(5,0);
                    need_redisplay = 1;
                }
                
                if(GetAsyncKeyState(VK_UP) & 0x8000 || GetAsyncKeyState('W') & 0x8000)
                {
                    MoveCamera(CAMERA_MOVE_FORWARD);
                    need_redisplay = 1;
                }
                if(GetAsyncKeyState(VK_DOWN) & 0x8000 || GetAsyncKeyState('S') & 0x8000)
                {
                    MoveCamera(CAMERA_MOVE_BACKWARD);
                    need_redisplay = 1;
                }
                if(GetAsyncKeyState(VK_LEFT) & 0x8000 || GetAsyncKeyState('A') & 0x8000)
                {
                    MoveCamera(CAMERA_MOVE_LEFT);
                    need_redisplay = 1;
                }
                if(GetAsyncKeyState(VK_RIGHT) & 0x8000 || GetAsyncKeyState('D') & 0x8000)
                {
                    MoveCamera(CAMERA_MOVE_RIGHT);
                    need_redisplay = 1;
                }
/*
                if(need_redisplay)
                {
                    Display();
                    SwapBuffers(hDC);
                    need_redisplay = 0;
                }
*/
                Display();
                SwapBuffers(hDC);
                    
                if(Scene && BBoxContainsPoint(Scene->exit,Camera.position))
                {
                    KillTimer(hWnd,INPUT_TIMER); /* disable input :) */
                    MessageBox(hWnd,"You have exited from labyrinth.","Congratulations!",MB_OK);
                    PostMessage(hWnd,WM_CLOSE,0,0);
                }
            }
        return 0;
    }
    return DefWindowProc(hWnd,msg,wParam,lParam);
}

unsigned char threeto8[8] = {
    0, 0111>>1, 0222>>1, 0333>>1, 0444>>1, 0555>>1, 0666>>1, 0377
};

unsigned char twoto8[4] = {
    0, 0x55, 0xaa, 0xff
};

unsigned char oneto8[2] = {
    0, 255
};

static int defaultOverride[13] = {
    0, 3, 24, 27, 64, 67, 88, 173, 181, 236, 247, 164, 91
};

static PALETTEENTRY defaultPalEntry[20] = {
    { 0,   0,   0,    0 },
    { 0x80,0,   0,    0 },
    { 0,   0x80,0,    0 },
    { 0x80,0x80,0,    0 },
    { 0,   0,   0x80, 0 },
    { 0x80,0,   0x80, 0 },
    { 0,   0x80,0x80, 0 },
    { 0xC0,0xC0,0xC0, 0 },

    { 192, 220, 192,  0 },
    { 166, 202, 240,  0 },
    { 255, 251, 240,  0 },
    { 160, 160, 164,  0 },

    { 0x80,0x80,0x80, 0 },
    { 0xFF,0,   0,    0 },
    { 0,   0xFF,0,    0 },
    { 0xFF,0xFF,0,    0 },
    { 0,   0,   0xFF, 0 },
    { 0xFF,0,   0xFF, 0 },
    { 0,   0xFF,0xFF, 0 },
    { 0xFF,0xFF,0xFF, 0 }
};

unsigned char
ComponentFromIndex(int i, UINT nbits, UINT shift)
{
    unsigned char val;

    val = (unsigned char) (i >> shift);
    switch (nbits) {

    case 1:
        val &= 0x1;
        return oneto8[val];

    case 2:
        val &= 0x3;
        return twoto8[val];

    case 3:
        val &= 0x7;
        return threeto8[val];

    default:
        return 0;
    }
}

void
CreateRGBPalette(HDC hDC)
{
    PIXELFORMATDESCRIPTOR pfd;
    LOGPALETTE *pPal;
    int n, i;

    n = GetPixelFormat(hDC);
    DescribePixelFormat(hDC, n, sizeof(PIXELFORMATDESCRIPTOR), &pfd);

    if (pfd.dwFlags & PFD_NEED_PALETTE) {
        n = 1 << pfd.cColorBits;
        pPal = (PLOGPALETTE)LocalAlloc(LMEM_FIXED, sizeof(LOGPALETTE) +
                n * sizeof(PALETTEENTRY));
        pPal->palVersion = 0x300;
        pPal->palNumEntries = n;
        for (i=0; i<n; i++) {
            pPal->palPalEntry[i].peRed =
                    ComponentFromIndex(i, pfd.cRedBits, pfd.cRedShift);
            pPal->palPalEntry[i].peGreen =
                    ComponentFromIndex(i, pfd.cGreenBits, pfd.cGreenShift);
            pPal->palPalEntry[i].peBlue =
                    ComponentFromIndex(i, pfd.cBlueBits, pfd.cBlueShift);
            pPal->palPalEntry[i].peFlags = 0;
        }

        /* fix up the palette to include the default GDI palette */
        if ((pfd.cColorBits == 8)                           &&
            (pfd.cRedBits   == 3) && (pfd.cRedShift   == 0) &&
            (pfd.cGreenBits == 3) && (pfd.cGreenShift == 3) &&
            (pfd.cBlueBits  == 2) && (pfd.cBlueShift  == 6)
           ) {
            for (i = 1 ; i <= 12 ; i++)
                pPal->palPalEntry[defaultOverride[i]] = defaultPalEntry[i];
        }

        ghPalette = CreatePalette(pPal);
        LocalFree(pPal);

        ghpalOld = SelectPalette(hDC, ghPalette, FALSE);
        n = RealizePalette(hDC);
    }
}

int InitOpenGLContext(void)
{     
    unsigned iPixelFormat;
    PIXELFORMATDESCRIPTOR pfd;

    memset(&pfd,0,sizeof(pfd));
    pfd.nSize = sizeof(pfd);
    pfd.nVersion = 1;
    pfd.dwFlags = PFD_DRAW_TO_WINDOW|PFD_DOUBLEBUFFER|PFD_SUPPORT_OPENGL;
    pfd.iPixelType = PFD_TYPE_RGBA;
    pfd.cColorBits = 32;
    pfd.cDepthBits = 32;
    pfd.iLayerType = PFD_MAIN_PLANE;

    iPixelFormat = ChoosePixelFormat(hDC,&pfd);
    SetPixelFormat(hDC,iPixelFormat,&pfd);

    CreateRGBPalette(hDC);

    hRC = wglCreateContext(hDC);
    if(hRC == NULL)
            return 1;

    if(wglMakeCurrent(hDC,hRC) == FALSE)
            return 1;

    InitGLExtensions();

    return 0;
}

void DestroyOpenGLContext(void)
{ 
    wglMakeCurrent(NULL,NULL);
    wglDeleteContext(hRC);
}

int WINAPI WinMain(HINSTANCE hInst, HINSTANCE hPrevInst, LPSTR lpszCommandline, int nCmdShow)
{
    HWND hWnd;
    WNDCLASS wc;
    MSG msg;
    char *dir;

    int fullscreen;

    AllocConsole();
    freopen("CONOUT$","wb",stdout);

    dir = strstr(lpszCommandline,"/CD:");
    if(dir)
    {
        if(SetCurrentDirectory(dir + 4) == FALSE)
            printf("Unable to change directory!\n");
    }

    LoadSettings("settings.cfg");

    fullscreen = GetIntSetting("fullscreen",0);

    if(fullscreen)
    {
        Window.width = GetSystemMetrics(SM_CXSCREEN);
        Window.height = GetSystemMetrics(SM_CYSCREEN);
    }
    else
    {
        Window.width = GetIntSetting("window_width",640);
        Window.height = GetIntSetting("window_height",480);
    }

    wc.style = CS_HREDRAW|CS_VREDRAW;
    wc.lpfnWndProc = WndProc;
    wc.cbClsExtra = 0;
    wc.cbWndExtra = 0;
    wc.hInstance = hInst;
    wc.hIcon = LoadIcon(NULL,MAKEINTRESOURCE(IDI_APPLICATION));
    wc.hCursor = LoadCursor(NULL,MAKEINTRESOURCE(IDC_ARROW));
    wc.hbrBackground = (HBRUSH)GetStockObject(WHITE_BRUSH);
    wc.lpszMenuName = "topbar";
    wc.lpszClassName = WNDCLASS_NAME;

    if(!RegisterClass(&wc))
    {
        MessageBoxA(0,"Can't register window class!","Error",MB_OK|MB_ICONHAND);
        return 0;
    }

    if(fullscreen)
        hWnd = CreateWindow(WNDCLASS_NAME,"Window",WS_POPUP|WS_CLIPCHILDREN|WS_CLIPSIBLINGS,0,0,
        Window.width,Window.height,NULL,NULL,hInst,0);
    else
        hWnd = CreateWindow(WNDCLASS_NAME,"Window",WS_OVERLAPPEDWINDOW|WS_CLIPCHILDREN|WS_CLIPSIBLINGS,
        CW_USEDEFAULT,CW_USEDEFAULT,Window.width,Window.height,0,0,hInst,0);

    if(!hWnd)
    {
        UnregisterClass(WNDCLASS_NAME,hInst);
        MessageBoxA(0,"Can't create window!","Error",MB_OK|MB_ICONHAND);
        return 0;
    }

    hDC = GetDC(hWnd);

    if(InitOpenGLContext() != 0)
            MessageBoxA(0,"Can't create OpenGL context!","Error",MB_OK|MB_ICONHAND);

    Init();

    ShowWindow(hWnd,SW_SHOW);

    Reshape();
    Display();

    SetTimer(hWnd,INPUT_TIMER,16,NULL);

    while(GetMessage(&msg,hWnd,0,0)>0)
    {
        TranslateMessage(&msg);
        DispatchMessage(&msg);
    }

    KillTimer(hWnd,INPUT_TIMER);

    DestroyOpenGLContext();
    UnregisterClass(WNDCLASS_NAME,hInst);

    Exit();
    FreeSettings();
    
    return 0;
}
