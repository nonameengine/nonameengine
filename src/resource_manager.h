#ifndef RESOURCE_MANAGER_H
#define RESOURCE_MANAGER_H

#ifdef _WIN32
#include <windows.h>
#endif
#include <GL/gl.h>
#include <GL/glu.h>

enum ResourceType
{
    RT_TEXTURE,
    RT_MODEL
};

typedef struct ResourceTexture
{
    #ifdef USE_OPENGL_VERSION_1_1
    GLuint texture;
    #else
    int width,height;
    GLenum fmt,ifmt;

    GLenum wrap_mode;
    GLenum filter_type;

    void *pixels;
    #endif
} ResourceTexture;

typedef struct ResourceModel
{
    struct Visual *visual;
} ResourceModel;

typedef struct Resource
{
    char *path;
    int type;
    int users;
    
    void *data;
    struct Resource *prev, *next;
} Resource;

/*int ResourceExists(char *path);*/

#define GetTexture(res) ((ResourceTexture*)res->data)->texture
#define GetVisual(res) ((ResourceModel*)res->data)->visual

Resource *GetResource(char *path, int type);
void ReleaseResource(Resource *res);

void BindTexture(Resource *res);

#endif
