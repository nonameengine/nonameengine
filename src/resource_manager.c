#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "resource_manager.h"

#include "system/config.h"

#include "formats/tga.h"
#include "formats/ogf.h"

#include "gl_extensions.h"

Resource *resources = 0;

GLenum select_texture_internal_format(TgaImage *i)
{
    if(i->header.image_type == TGA_TRUECOLOR ||
    i->header.image_type == TGA_TRUECOLOR_RLE)
    {        
        if(i->header.pixel_size == 24)
            return 3; /* GL_RGB */
        if(i->header.pixel_size == 32)
            return 4; /* GL_RGBA */
    }
    if(i->header.image_type == TGA_GRAYSCALE ||
    i->header.image_type == TGA_GRAYSCALE_RLE)
    {
        if(i->header.pixel_size == 8)
            return 1; /* GL_LUMINANCE */
        if(i->header.pixel_size == 16)
            return 2; /* GL_LUMINANCE_ALPHA */
    }
  
    if(i->header.image_type == TGA_INDEXED ||
    i->header.image_type == TGA_INDEXED_RLE)
    {
#ifdef USE_PALETTED_TEXTURES   
        if(glColorTableEXT)
        {
            if(i->header.pixel_size == 8)
                return GL_COLOR_INDEX8_EXT;
        }
        else
        {
#endif            
            if(i->header.color_map_entry_size == 24)
                return 3; /* GL_RGB */
            if(i->header.color_map_entry_size == 32)
                return 4; /* GL_RGBA */
#ifdef USE_PALETTED_TEXTURES                
        }
#endif        
    }
   
    printf("Unknown image format!\n");
    return 0;
}

GLenum select_texture_format(TgaImage *i)
{
    if(i->header.image_type == TGA_TRUECOLOR ||
    i->header.image_type == TGA_TRUECOLOR_RLE)
    {
#ifdef USE_RGB_TEXTURES
        if(i->header.pixel_size == 24)
            return GL_RGB;
        if(i->header.pixel_size == 32)
            return GL_RGBA;
#else
        if(i->header.pixel_size == 24)
            return GL_BGR_EXT;
        if(i->header.pixel_size == 32)
            return GL_BGRA_EXT;
#endif
    }
    if(i->header.image_type == TGA_GRAYSCALE ||
    i->header.image_type == TGA_GRAYSCALE_RLE)
    {
        if(i->header.pixel_size == 8)
            return GL_LUMINANCE;
        if(i->header.pixel_size == 16)
            return GL_LUMINANCE_ALPHA;
    }
    if(i->header.image_type == TGA_INDEXED ||
    i->header.image_type == TGA_INDEXED_RLE)
    {
#ifdef USE_PALETTED_TEXTURES
        if(glColorTableEXT)
            return GL_COLOR_INDEX;
        else
        {
#endif
    /* assume that image unpacked to truecolor */
    #ifdef USE_RGB_TEXTURES
            if(i->header.color_map_entry_size == 24)
                return GL_RGB;
            if(i->header.color_map_entry_size == 32)
                return GL_RGBA;
    #else
            if(i->header.color_map_entry_size == 24)
                return GL_BGR_EXT;
            if(i->header.color_map_entry_size == 32)
                return GL_BGRA_EXT;
    #endif                
#ifdef USE_PALETTED_TEXTURES                
        }
#endif        
    } 

    printf("Unknown image format!\n");
    
    return 0;
}
ResourceTexture *load_texture(char *path)
{
    char full_path[256];
    ResourceTexture *out;
    
    TgaImage img;
    Config *cfg, *param;

    GLenum wrap_mode = GL_REPEAT, filter_type = GL_NEAREST;

    out = malloc(sizeof(ResourceTexture));
    memset(out,0,sizeof(ResourceTexture));

    strcpy(full_path,"data\\textures\\");
    strcat(full_path,path);
    strcat(full_path,".tga");

    if(LoadTGA(full_path,&img) != 0)
    {
        printf("Can't load texture '%s'!\n",path);
        return out;
    }

    if(img.header.image_type != TGA_GRAYSCALE && img.header.image_type != TGA_GRAYSCALE_RLE &&
    (img.header.pixel_size == 16 || img.header.color_map_entry_size == 16))
    {
        printf("Sorry, 16-bit images not supported yet.\n");
        free(img.pixels);
        free(img.palette);

        return out;
    }

    if(img.header.y_origin == 0 || !(img.header.image_descriptor & 0x20))
        FlipHorizontal(img.pixels,img.header.width,img.header.height,img.header.pixel_size);
    if(img.header.image_descriptor & 0x10)
        FlipVertical(img.pixels,img.header.width,img.header.height,img.header.pixel_size);

#ifdef USE_RGB_TEXTURES
    if(img.palette)
        SwapRandBComponents(img.palette, img.header.color_map_entry_size,
        img.header.color_map_length);
    else
        SwapRandBComponents(img.pixels, img.header.pixel_size,
        img.header.width * img.header.height);
#endif

#ifdef USE_PALETTED_TEXTURES
    if(img.palette && !glColorTableEXT)
#else
    if(img.palette)
#endif
    {
        u8 *p = malloc(img.header.width * img.header.height * (img.header.color_map_entry_size / 8));

        UnpackIndexedImage(img.pixels, img.palette, p,
        img.header.color_map_entry_size, img.header.width * img.header.height);

        free(img.pixels);
        img.pixels = p;
    }

    strcpy(strchr(full_path,'.'),".opt");
    cfg = LoadConfig(full_path);

    if(cfg)
    {
        param = GetConfigObjectItem(cfg,"filter_type");
        if(param)
        {
            switch(param->valueint)
            {
                case 1:
                filter_type = GL_NEAREST;
                break;
                case 2:
                filter_type = GL_LINEAR;
                break;
                case 3:
                filter_type = GL_LINEAR_MIPMAP_NEAREST;
                break;
                case 4:
                filter_type = GL_NEAREST_MIPMAP_LINEAR;
                break;
                case 5:
                filter_type = GL_LINEAR_MIPMAP_LINEAR;
                break;
            }
        }
    
        param = GetConfigObjectItem(cfg,"clamp");
        if(param)
        {
            if(param->valueint == 1)
                wrap_mode = GL_CLAMP;
        }
    }
    
#ifdef USE_OPENGL_VERSION_1_1
    glGenTextures(1,&out->texture);
    glBindTexture(GL_TEXTURE_2D,out->texture);
    
    glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_WRAP_S,wrap_mode);
    glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_WRAP_T,wrap_mode);
    glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MIN_FILTER,filter_type);
    glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MAG_FILTER,filter_type);

    if(filter_type != GL_NEAREST && filter_type != GL_LINEAR)
        gluBuild2DMipmaps(
        GL_TEXTURE_2D,
        select_texture_internal_format(&img),
        img.header.width,img.header.height,
        select_texture_format(&img),
        GL_UNSIGNED_BYTE,img.pixels);
    else
        glTexImage2D(
        GL_TEXTURE_2D, 0,
        select_texture_internal_format(&img),
        img.header.width, img.header.height, 0,
        select_texture_format(&img),
        GL_UNSIGNED_BYTE,img.pixels);

    #ifdef USE_PALETTED_TEXTURES
    if(img.palette && glColorTableEXT)
    {
        GLenum fmt = 0, ifmt = 0;       

        if(img.header.color_map_entry_size == 32)
        {
            fmt = GL_BGRA_EXT;
            ifmt = GL_RGBA;
        }
        if(img.header.color_map_entry_size == 24)
        {
            fmt = GL_BGR_EXT;
            ifmt = GL_RGB;
        }

        if(fmt == 0 || ifmt == 0)
            printf("Unknown palette format!\n");
        else
        {
            glColorTableEXT(GL_TEXTURE_2D,ifmt,img.header.color_map_length,
            fmt,GL_UNSIGNED_BYTE,img.palette);
        }
    }
    #endif
    
    free(img.pixels);

#else /* use only GL 1.0 functions */

    out->fmt  = select_texture_format(&img);
    out->ifmt = select_texture_internal_format(&img);

    out->wrap_mode   = wrap_mode;
    out->filter_type = filter_type;

    out->width  = img.header.width;
    out->height = img.header.height;
    
    out->pixels = img.pixels;
    
#endif

    free(img.palette);

    if(cfg)
        DeleteConfig(cfg);

    return out;  
}

void free_texture(ResourceTexture *t)
{
#ifdef USE_OPENGL_VERSION_1_1
    glDeleteTextures(1,&t->texture);
#else
    free(t->pixels);
#endif
    
    free(t);
}

ResourceModel *load_model(char *path)
{
    char full_path[256];
    ResourceModel *out;

    strcpy(full_path,"data\\models\\");
    strcat(full_path,path);
    strcat(full_path,".ogf");
    
    out = malloc(sizeof(ResourceModel));
    out->visual = LoadOGF(full_path);

    if(!out->visual)
    {
        printf("can't load model '%s'\n",full_path);
        out->visual = malloc(sizeof(Visual));
        memset(out->visual,0,sizeof(Visual));
    }
    
    return out;
}

void free_model(ResourceModel *m)
{
    FreeVisual(m->visual);
    free(m);
}

void BindTexture(Resource *res)
{  
    #ifdef USE_OPENGL_VERSION_1_1
    
    glBindTexture(GL_TEXTURE_2D,GetTexture(res));
    
    #else

    static ResourceTexture *current = 0;
    ResourceTexture *t = res->data;

    if(t != current)
    {
        current = t;
    
        glTexImage2D(GL_TEXTURE_2D,0,t->ifmt,t->width,t->height,0,t->fmt,GL_UNSIGNED_BYTE,t->pixels);
        
        glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MIN_FILTER,t->filter_type);
        glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MAG_FILTER,t->filter_type);
        glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_WRAP_S,t->wrap_mode);
        glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_WRAP_T,t->wrap_mode);
    }
    
    #endif
}

Resource *GetResource(char *path, int type)
{
    Resource *res;

    res = resources;
    
    while(res)
    {
        if(stricmp(res->path,path) == 0 && res->type == type)
        {
            res->users++;
            return res;
        }
        else
            res = res->next;
    }

    res = malloc(sizeof(Resource));

    res->path = strdup(path);
    res->type = type;
    res->users = 1;
    res->prev = 0;

    res->next = resources;
    
    if(resources)
        resources->prev = res;
        
    resources = res;

    /* load resource here */
    if(type == RT_TEXTURE)
        res->data = load_texture(path);
    if(type == RT_MODEL)
        res->data = load_model(path);

    return res;
}

void ReleaseResource(Resource *res)
{
    res->users--;

    if(res->users == 0)
    {
        if(!res->prev)
            resources = res->next;
        else
            res->prev->next = res->next;

        if(res->next)
                res->next->prev = res->prev;

        /* unload resource here */
        if(res->type == RT_TEXTURE)
            free_texture(res->data);
        if(res->type == RT_MODEL)
            free_model(res->data);
        
        free(res->path);
        free(res);
    }
}
