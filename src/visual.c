#include <stdlib.h>
#include <stdio.h>
#include "visual.h"

#include "main.h"
#include "gl_extensions.h"

#ifdef USE_IBM_OPENGL_11_FIX
/* Magic. Do not touch. */
__inline int get_index_array_shift(GLenum index_type)
{
    if(index_type == GL_UNSIGNED_BYTE)
        return 3;
    if(index_type == GL_UNSIGNED_SHORT)
        return 2;
    if(index_type == GL_UNSIGNED_INT)
        return 0;

    return 0;
}
#endif

__inline int size_of(GLenum t)
{
    if(t == GL_BYTE || t == GL_UNSIGNED_BYTE)
        return sizeof(GLbyte);
    if(t == GL_SHORT || t == GL_UNSIGNED_SHORT)
        return sizeof(GLshort);
    if(t == GL_INT || t == GL_UNSIGNED_INT)
        return sizeof(GLint);
    if(t == GL_FLOAT)
        return sizeof(GLfloat);
    if(t == GL_2_BYTES)
        return 2;
    if(t == GL_3_BYTES)
        return 3;
    if(t == GL_4_BYTES)
        return 4;
#ifdef GL_DOUBLE
    if(t == GL_DOUBLE)
        return sizeof(GLdouble);
#endif

    printf("trying to get size of unknown type\n");
    exit(-1);
    return -1;
}

static int set_vertex_format(unsigned vertex_format)
{
    int stride = 0;

    /* disable all */
    glDisableClientState(GL_COLOR_ARRAY);
    glDisableClientState(GL_EDGE_FLAG_ARRAY);
    glDisableClientState(GL_INDEX_ARRAY);
    glDisableClientState(GL_NORMAL_ARRAY);
    glDisableClientState(GL_TEXTURE_COORD_ARRAY);
    glDisableClientState(GL_VERTEX_ARRAY);

    /* calculate vertex size */
    if(vertex_format & VF_POSITION)
    {
        glEnableClientState(GL_VERTEX_ARRAY);
        stride += sizeof(Vector3f);
    }
    if(vertex_format & VF_NORMAL)
    {
        glEnableClientState(GL_NORMAL_ARRAY);
        stride += sizeof(Vector3f);
    }
    if(vertex_format & VF_TEXCOORD)
    {
        glEnableClientState(GL_TEXTURE_COORD_ARRAY);
        stride += sizeof(Vector2f);
    }
    if(vertex_format & VF_COLOR)
    {
        glEnableClientState(GL_COLOR_ARRAY);
        stride += sizeof(unsigned);
    }

    return stride;
}

void DrawVisual(Visual *v, float quality, int stage)
{
    int i, start;
    static int stride;

    static unsigned vertex_format = 0;

    Surface *s = v->surfaces;
    for(i = 0; i < v->surface_count; i++, s++)
    {
        if(stage == 0 && !(s->render_mode & RM_DRAW))
            continue;
        if(stage == 1 && !(s->render_mode & RM_BLEND))
            continue;

        /* set render mode */
        if(s->render_mode & RM_ALPHA_TEST)
            glEnable(GL_ALPHA_TEST);
        else
            glDisable(GL_ALPHA_TEST);

        if(s->render_mode & RM_LIGHTING)
            glEnable(GL_LIGHTING);
        else
            glDisable(GL_LIGHTING);

        /* set blend func */
        if(s->render_mode & RM_BLEND)
        {
            switch(s->blend_func)
            {
                case BF_BLEND:
                    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
                break;

                case BF_ADD:
                    glBlendFunc(GL_SRC_ALPHA, GL_ONE);
                break;
            }
        }

        if(s->draw_mode == DM_SPHERE_MAP && stage == 0)
        {
            glEnable(GL_TEXTURE_2D);
            BindTexture(s->envmap);

            glEnable(GL_TEXTURE_GEN_S);
            glEnable(GL_TEXTURE_GEN_T);

            glTexGeni(GL_T, GL_TEXTURE_GEN_MODE, GL_SPHERE_MAP);
            glTexGeni(GL_S, GL_TEXTURE_GEN_MODE, GL_SPHERE_MAP);
        }
        else
        {
            glDisable(GL_TEXTURE_GEN_S);
            glDisable(GL_TEXTURE_GEN_T);

            if(s->texture != 0)
            {
                glEnable(GL_TEXTURE_2D);
                BindTexture(s->texture);
            }
            else
                glDisable(GL_TEXTURE_2D);
        }

        glMaterialfv(GL_FRONT,GL_AMBIENT,(float*)&s->material.ambient);
        glMaterialfv(GL_FRONT,GL_DIFFUSE,(float*)&s->material.diffuse);
        glMaterialfv(GL_FRONT,GL_SPECULAR,(float*)&s->material.specular);
        glMaterialfv(GL_FRONT,GL_EMISSION,(float*)&s->material.emission);
        glMaterialf(GL_FRONT,GL_SHININESS,s->material.shininess);
            
        if(vertex_format != s->vertex_format)
        {
            vertex_format = s->vertex_format;
            stride = set_vertex_format(vertex_format);
        }

        start = 0;

        if(vertex_format & VF_POSITION)
        {
            glVertexPointer(3,GL_FLOAT,stride,s->vertices + start);
            start += sizeof(float) * 3;
        }

        if(vertex_format & VF_NORMAL)
        {
            glNormalPointer(GL_FLOAT,stride,s->vertices + start);
            start += sizeof(float) * 3;
        }

        if(vertex_format & VF_TEXCOORD)
        {
            glTexCoordPointer(2,GL_FLOAT,stride,s->vertices + start);
            start += sizeof(float) * 2;
        }

        if(vertex_format & VF_COLOR)
        {
            glColorPointer(4,GL_UNSIGNED_BYTE,stride,s->vertices + start);
            start += sizeof(unsigned char) * 4;
        }

        if(s->sw_count == 0)
        {
            if(s->type == GL_TRIANGLES)
            {
                glDrawElements(GL_TRIANGLES,s->face_count * 3,s->index_type,
#ifdef USE_IBM_OPENGL_11_FIX
                s->indices - get_index_array_shift(s->index_type));
#else
                s->indices);
#endif
                poly_count += s->face_count;
            }
            else if(s->type == GL_QUADS)
                glDrawElements(GL_QUADS,s->face_count * 4,s->index_type,
#ifdef USE_IBM_OPENGL_11_FIX
                s->indices - get_index_array_shift(s->index_type));
#else
                s->indices);
#endif
            else
                printf("Unknown surface type!\n");
        }
        else
        {
            int lod = (s->sw_count - 1) * quality;
            SlideWindow *sw = &s->swis[lod];

            if(s->type == GL_TRIANGLES)
            {
                glDrawElements(GL_TRIANGLES, sw->face_count * 3, s->index_type,
#ifdef USE_IBM_OPENGL_11_FIX
                s->indices + (sw->offset * size_of(s->index_type)) - get_index_array_shift(s->index_type));
#else
                s->indices + (sw->offset * size_of(s->index_type)));
#endif
                poly_count += sw->face_count;
            }
            else if(s->type == GL_QUADS)
                glDrawElements(GL_QUADS, sw->face_count * 4, s->index_type,
#ifdef USE_IBM_OPENGL_11_FIX
                s->indices + (sw->offset * size_of(s->index_type)) - get_index_array_shift(s->index_type));
#else
                s->indices + (sw->offset * size_of(s->index_type)));
#endif
            else
                printf("Unknown surface type!\n");
        }
    }
}

void FreeVisual(Visual *v)
{
    int i;

    for(i = 0; i < v->surface_count; i++)
    {
        if(v->surfaces[i].texture)
            ReleaseResource(v->surfaces[i].texture);
        if(v->surfaces[i].envmap)
            ReleaseResource(v->surfaces[i].envmap);


        free(v->surfaces[i].vertices);
        free(v->surfaces[i].indices);

        free(v->surfaces[i].swis);
    }

    free(v->surfaces);
    free(v);
}
