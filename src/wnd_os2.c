#define INCL_PM

#include <os2.h>
#include <gl/pgl.h>
#include <gl/glu.h>

#include <stdlib.h>

#include "main.h"

#include "scene.h"
#include "gl_extensions.h"
#include "flying_camera.h"
#include "system/settings.h"

#define INPUT_TIMER 0x1
#define WINCLASSNAME "MyWindowClass"

HAB hab;
HWND hwnd;
HGC hgc;

MRESULT EXPENTRY MainDriver(HWND hWnd, ULONG msg, MPARAM mp1, MPARAM mp2)
{
    static int x = 0, y = 0;
    static int old_x = 0, old_y = 0;
    int need_redisplay = 0;
      
    switch(msg)
    {
        case WM_SIZE:
            Window.width = SHORT1FROMMP(mp2);
            Window.height = SHORT2FROMMP(mp2);

            Reshape();
        break;

        case WM_CHAR:
            if(CHAR1FROMMP(mp2) == 27)
                WinPostMsg(hWnd,WM_QUIT,0,0);
        break;

        /* Mouse handler */

        case WM_MOUSEMOVE:
            x = SHORT1FROMMP(mp1);
            y = SHORT2FROMMP(mp1);
        break;

        case WM_TIMER:
            if(WinQueryActiveWindow(HWND_DESKTOP) == 0)
                return 0;
            
            if(LONGFROMMP(mp1) == INPUT_TIMER)
            {
                if(WinGetKeyState(HWND_DESKTOP,VK_BUTTON1) & 0x8000)
                {
                    RotateCamera(x-old_x,-(y-old_y));                   
                    need_redisplay = 1;
                }

                old_x = x;
                old_y = y;
                
                if(WinGetPhysKeyState(HWND_DESKTOP,0x48) & 0x8000 || /* UP arrow */
                   WinGetPhysKeyState(HWND_DESKTOP,0x11) & 0x8000)   /* W key */
                {
                    MoveCamera(CAMERA_MOVE_FORWARD);
                    need_redisplay = 1;
                }
                if(WinGetPhysKeyState(HWND_DESKTOP,0x50) & 0x8000 || /* DOWN arrow */
                   WinGetPhysKeyState(HWND_DESKTOP,0x1F) & 0x8000)   /* S key */
                {
                    MoveCamera(CAMERA_MOVE_BACKWARD);
                    need_redisplay = 1;
                }
                if(WinGetPhysKeyState(HWND_DESKTOP,0x4B) & 0x8000 || /* LEFT arrow */
                   WinGetPhysKeyState(HWND_DESKTOP,0x1E) & 0x8000)   /* A key */
                {
                    MoveCamera(CAMERA_MOVE_LEFT);
                    need_redisplay = 1;
                }
                if(WinGetPhysKeyState(HWND_DESKTOP,0x4D) & 0x8000 || /* RIGHT arrow */
                   WinGetPhysKeyState(HWND_DESKTOP,0x20) & 0x8000)   /* D key */
                {
                    MoveCamera(CAMERA_MOVE_RIGHT);
                    need_redisplay = 1;
                }

                if(need_redisplay)
                {
                    Display();
                    pglSwapBuffers(hab,hgc);
                }

                if(Scene && BBoxContainsPoint(Scene->exit,Camera.position))
                {
                    WinStopTimer(hab,hWnd,INPUT_TIMER);
                    WinMessageBox(HWND_DESKTOP,hWnd,"You have exited from labyrinth.","Congratulations!",0,MB_OK|MB_NOICON);
                    WinPostMsg(hWnd,WM_CLOSE,0,0);
                }
            }

            return 0;
        break;

        default:
            return WinDefWindowProc(hWnd,msg,mp1,mp2);
    }

    return 0;
}

int InitOpenGLContext()
{
    PVISUALCONFIG pvc;     
    int attribList[] = {
    PGL_RGBA,
    PGL_RED_SIZE, 5,
    PGL_GREEN_SIZE, 5,
    PGL_BLUE_SIZE, 5,
    PGL_DOUBLEBUFFER, PGL_None
    };

    pvc = pglChooseConfig(hab,attribList);
    if(!pvc)
        return 1;
        
    hgc = pglCreateContext(hab,pvc,NULLHANDLE,FALSE);
    if(hgc == NULLHANDLE)
        return 1;

    if(!pglMakeCurrent(hab,hgc,hwnd))
        return 1;

    free(pvc);

    return 0;
}

void DestroyOpenGLContext()
{ 
    pglMakeCurrent(hab,NULLHANDLE,NULLHANDLE);
    pglDestroyContext(hab,hgc);
}

int main(int argc, char *argv[])
{
    HMQ hmq;
    HWND hframe;

    QMSG qmsg;
    ULONG style;

    int fullscreen;
    SWP swp;

    LoadSettings("settings.cfg");

    fullscreen = GetIntSetting("fullscreen",0);

    if(fullscreen)
    {
        WinQueryWindowPos(HWND_DESKTOP,&swp);

        Window.width = swp.cx;
        Window.height = swp.cy;
    }
    else
    {
        Window.width = GetIntSetting("window_width",640);
        Window.height = GetIntSetting("window_height",480);
    }
    
    hab = WinInitialize(0);

    if(!hab)
        return -1;

    hmq = WinCreateMsgQueue(hab,0);

    if(!hmq)
        return -1;

    if(!WinRegisterClass(hab,WINCLASSNAME,MainDriver,CS_SIZEREDRAW,0))
        return -1;

    if(fullscreen)
        style = FCF_TASKLIST;
    else
        style = FCF_STANDARD & ~(FCF_MENU|FCF_ICON|FCF_ACCELTABLE);

    hframe = WinCreateStdWindow(HWND_DESKTOP,WS_VISIBLE,&style,WINCLASSNAME,"Window",CS_SIZEREDRAW,NULLHANDLE,0,&hwnd);

    if(!hframe)
        return -1;

    WinSetWindowPos(hframe,HWND_TOP,0,0,Window.width,Window.height,SWP_SIZE|SWP_ZORDER|SWP_MOVE);

    if(InitOpenGLContext() != 0)
        return -1;

    Init();
    Reshape();
    Display();

    WinStartTimer(hab,hwnd,INPUT_TIMER,30);

    while(WinGetMsg(hab,&qmsg,NULLHANDLE,0,0))
    {
        WinDispatchMsg(hab,&qmsg);
    }

    WinStopTimer(hab,hwnd,INPUT_TIMER);

    DestroyOpenGLContext();
    Exit();
    FreeSettings();

    WinDestroyWindow(hframe);
    WinDestroyMsgQueue(hmq);
    WinTerminate(hab);
    
    return 0;
}
