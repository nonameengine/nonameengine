#ifndef DYNAMIC_OBJECT_H
#define DYNAMIC_OBJECT_H

#include "scene.h"

DynamicObject *CreateDynamicObject(char *name);
void RemoveDynamicObject(DynamicObject *obj);
void RemoveDynamicObjectByName(char *name);

DynamicObject *GetDynamicObject(char *name);

void UpdateDynamicObjectBounding(DynamicObject *o);

#endif

