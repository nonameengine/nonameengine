#ifndef GL_EXTENSIONS_H
#define GL_EXTENSIONS_H

#ifdef _WIN32
#include <windows.h>
#endif
#include <GL/gl.h>

#ifdef USE_PALETTED_TEXTURES
extern PFNGLCOLORTABLEEXTPROC glColorTableEXT;
#endif

#ifndef USE_OPENGL_VERSION_1_1

#define GL_VERTEX_ARRAY                 0x8074
#define GL_NORMAL_ARRAY                 0x8075
#define GL_COLOR_ARRAY                  0x8076
#define GL_INDEX_ARRAY                  0x8077
#define GL_TEXTURE_COORD_ARRAY          0x8078
#define GL_EDGE_FLAG_ARRAY              0x8079

#define glEnableClientState  EnableArray
#define glDisableClientState DisableArray

#define glColorPointer      SetColorArray
#define glEdgeFlagPointer   SetEdgeFlagArray
#define glIndexPointer      SetIndexArray
#define glNormalPointer     SetNormalArray
#define glTexCoordPointer   SetTexCoordArray
#define glVertexPointer     SetVertexArray

#define glArrayElement      ArrayElement
#define glDrawArrays        DrawArrays
#define glDrawElements      DrawArraysIndexed

void EnableArray(GLenum array);
void DisableArray(GLenum array);

void SetColorArray(int size, GLenum type, int stride, void *pointer);
void SetEdgeFlagArray(int stride, void *pointer);
void SetIndexArray(GLenum type, int stride, void *pointer);
void SetNormalArray(GLenum type, int stride, void *pointer);
void SetTexCoordArray(int size, GLenum type, int stride, void *pointer);
void SetVertexArray(int size, GLenum type, int stride, void *pointer);

void DrawArrays(GLenum mode, GLuint first, GLuint count);
void DrawArraysIndexed(GLenum mode, GLuint count, GLenum type, void *indices);

#endif

void InitGLExtensions(void);

#endif
