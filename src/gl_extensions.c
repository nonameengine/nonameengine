#include "gl_extensions.h"
#include <string.h>
#include <stdio.h>

#ifdef USE_PALETTED_TEXTURES
PFNGLCOLORTABLEEXTPROC glColorTableEXT = 0;
#endif

#ifndef USE_OPENGL_VERSION_1_1

/* color array parameters */
int     clr_array_enabled       = 0;
int     clr_array_size          = 4;
GLenum  clr_array_type          = GL_FLOAT;
int     clr_array_stride        = 0;
GLubyte *clr_array_pointer      = 0;

/* edge flag array parameters */
int     efl_array_enabled       = 0;
int     efl_array_stride        = 0;
GLubyte *efl_array_pointer      = 0;

/* index array parameters */
int     idx_array_enabled       = 0;
GLenum  idx_array_type          = GL_FLOAT;
int     idx_array_stride        = 0;
GLubyte *idx_array_pointer      = 0;

/* normal array parameters */
int     nrm_array_enabled       = 0;
GLenum  nrm_array_type          = GL_FLOAT;
int     nrm_array_stride        = 0;
GLubyte *nrm_array_pointer      = 0;

/* texture coord array parameters */
int     tcd_array_enabled       = 0;
int     tcd_array_size          = 4;
GLenum  tcd_array_type          = GL_FLOAT;
int     tcd_array_stride        = 0;
GLubyte *tcd_array_pointer      = 0;

/* vertex array parameters */
int     vtx_array_enabled       = 0;
int     vtx_array_size          = 4;
GLenum  vtx_array_type          = GL_FLOAT;
int     vtx_array_stride        = 0;
GLubyte *vtx_array_pointer      = 0;

__inline int size_of(GLenum t)
{
    if(t == GL_BYTE || t == GL_UNSIGNED_BYTE)
        return sizeof(GLbyte);
    if(t == GL_SHORT || t == GL_UNSIGNED_SHORT)
        return sizeof(GLshort);
    if(t == GL_INT || t == GL_UNSIGNED_INT)
        return sizeof(GLint);
    if(t == GL_FLOAT)
        return sizeof(GLfloat);
    if(t == GL_2_BYTES)
        return 2;
    if(t == GL_3_BYTES)
        return 3;
    if(t == GL_4_BYTES)
        return 4;
#ifdef GL_DOUBLE
    if(t == GL_DOUBLE)
        return sizeof(GLdouble);
#endif

    printf("trying to get size of unknown type\n");
    exit(-1);
    return -1;
}

void EnableArray(GLenum array)
{
    switch(array)
    {
        case GL_COLOR_ARRAY:            clr_array_enabled = 1; break;
        case GL_EDGE_FLAG_ARRAY:        efl_array_enabled = 1; break;
        case GL_INDEX_ARRAY:            idx_array_enabled = 1; break;
        case GL_NORMAL_ARRAY:           nrm_array_enabled = 1; break;
        case GL_TEXTURE_COORD_ARRAY:    tcd_array_enabled = 1; break;
        case GL_VERTEX_ARRAY:           vtx_array_enabled = 1; break;
    }
}

void DisableArray(GLenum array)
{
    switch(array)
    {
        case GL_COLOR_ARRAY:            clr_array_enabled = 0; break;
        case GL_EDGE_FLAG_ARRAY:        efl_array_enabled = 0; break;
        case GL_INDEX_ARRAY:            idx_array_enabled = 0; break;
        case GL_NORMAL_ARRAY:           nrm_array_enabled = 0; break;
        case GL_TEXTURE_COORD_ARRAY:    tcd_array_enabled = 0; break;
        case GL_VERTEX_ARRAY:           vtx_array_enabled = 0; break;
    }
}

void SetColorArray(int size, GLenum type, int stride, void *pointer)
{
    clr_array_size = size;
    clr_array_type = type;
    clr_array_stride = stride;
    clr_array_pointer = pointer;
}

void SetEdgeFlagArray(int stride, void *pointer)
{
    efl_array_stride = stride;
    efl_array_pointer = pointer;
}

void SetIndexArray(GLenum type, int stride, void *pointer)
{
    idx_array_type = type;
    idx_array_stride = stride;
    idx_array_pointer = pointer;
}

void SetNormalArray(GLenum type, int stride, void *pointer)
{
    nrm_array_type = type;
    nrm_array_stride = stride;
    nrm_array_pointer = pointer;
}

void SetTexCoordArray(int size, GLenum type, int stride, void *pointer)
{
    tcd_array_size = size;
    tcd_array_type = type;
    tcd_array_stride = stride;
    tcd_array_pointer = pointer;
}

void SetVertexArray(int size, GLenum type, int stride, void *pointer)
{
    vtx_array_size = size;
    vtx_array_type = type;
    vtx_array_stride = stride;
    vtx_array_pointer = pointer;
}

void ArrayElement(unsigned i)
{
    int stride;
    
    if(clr_array_enabled)
    {
        stride = clr_array_stride ? clr_array_stride : size_of(clr_array_type) * clr_array_size;
        
        if(clr_array_type == GL_UNSIGNED_BYTE)
        {
            if(clr_array_size == 3)
                glColor3ubv(clr_array_pointer + stride * i);
            else if(clr_array_size == 4)
                glColor4ubv(clr_array_pointer + stride * i);
        }
        else if(clr_array_type == GL_FLOAT)
        {
            if(clr_array_size == 3)
                glColor3fv((GLfloat*)(clr_array_pointer + stride * i));
            else if(clr_array_size == 4)
                glColor4fv((GLfloat*)(clr_array_pointer + stride * i));
        }
        else
        {
            printf("This type for color array not implemented, sorry..\n");
        }
    }

    if(efl_array_enabled)
    {
        stride = efl_array_stride ? efl_array_stride : sizeof(GLboolean);

        glEdgeFlagv((GLboolean*)(efl_array_pointer + stride * i));
    }

    if(idx_array_enabled)
    {
        printf("Index array not implemented, sorry..\n");
    }

    if(nrm_array_enabled)
    {
        stride = nrm_array_stride ? nrm_array_stride : size_of(nrm_array_type) * 3;

        if(nrm_array_type == GL_FLOAT)
        {
            glNormal3fv((GLfloat*)(nrm_array_pointer + stride * i));
        }
        else
        {
            printf("This type not implemented for normal array, sorry..\n");
        }
    }

    if(tcd_array_enabled)
    {
        stride = tcd_array_stride ? tcd_array_stride : size_of(tcd_array_type) * tcd_array_size;

        if(tcd_array_type == GL_FLOAT)
        {
            switch(tcd_array_size)
            {
                case 1: glTexCoord1fv((GLfloat*)(tcd_array_pointer + stride * i)); break;
                case 2: glTexCoord2fv((GLfloat*)(tcd_array_pointer + stride * i)); break;
                case 3: glTexCoord3fv((GLfloat*)(tcd_array_pointer + stride * i)); break;
                case 4: glTexCoord4fv((GLfloat*)(tcd_array_pointer + stride * i)); break;
            }
        }
        else
        {
            printf("This type not implemented for texture coordinates array, sorry..\n");
        }
    }

    if(vtx_array_enabled)
    {
        stride = vtx_array_stride ? vtx_array_stride : size_of(vtx_array_type) * vtx_array_type;

        if(vtx_array_type == GL_FLOAT)
        {
            switch(vtx_array_size)
            {
                case 2: glVertex2fv((GLfloat*)(vtx_array_pointer + stride * i)); break;
                case 3: glVertex3fv((GLfloat*)(vtx_array_pointer + stride * i)); break;
                case 4: glVertex4fv((GLfloat*)(vtx_array_pointer + stride * i)); break;
                default: printf("Invalid size for vertex specifed: %d\n", vtx_array_size);
            }
        }
        else
            printf("Sorry, this type is not impelemented for vertex array..\n");
    }
}

void DrawArrays(GLenum mode, GLuint first, GLuint count)
{
    GLuint i;

    glBegin(mode);

    for(i = first; i < first+count; i ++)
        ArrayElement(i);

    glEnd();
}

void DrawArraysIndexed(GLenum mode, GLuint count, GLenum type, void *pindices)
{
    GLuint i;

    glBegin(mode);
    
    if(type == GL_UNSIGNED_BYTE)
    {
        GLubyte *indices = pindices;

        for(i = 0; i < count; i++)
            ArrayElement(indices[i]);
    }
    else if(type == GL_UNSIGNED_SHORT)
    {
        GLushort *indices = pindices;

        for(i = 0; i < count; i++)
            ArrayElement(indices[i]);
    }
    else if(type == GL_UNSIGNED_INT)
    {
        GLuint *indices = pindices;

        for(i = 0; i < count; i++)
            ArrayElement(indices[i]);
    }
    else
        printf("Invalid index type specifed for glDrawElements!!!\n");

    glEnd();
}

#endif

void InitGLExtensions(void)
{
    const char *extensions, *vendor;

    extensions = glGetString(GL_EXTENSIONS);
    vendor = glGetString(GL_VENDOR);

#ifdef USE_PALETTED_TEXTURES
    if(strstr(extensions,"GL_EXT_paletted_texture"))
    {
        if(strcmp(vendor,"SGI") != 0) /* this extension works incorrect in SGI opengl */
            glColorTableEXT = (PFNGLCOLORTABLEEXTPROC)wglGetProcAddress("glColorTableEXT");
    }
#endif
}
