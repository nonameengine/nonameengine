#ifndef VISUAL_H
#define VISUAL_H

#ifdef _WIN32
#include <windows.h>
#endif
#include <GL/gl.h>
#include "types.h"
#include "vector.h"
#include "resource_manager.h"
#include "collision.h"

enum VertexFormat
{
    VF_POSITION = 0x1,
    VF_TEXCOORD = 0x2,
    VF_NORMAL   = 0x4,
    VF_COLOR    = 0x8
};

enum RenderMode
{
    RM_ALPHA_TEST   = 0x1,
    RM_LIGHTING     = 0x2,
    RM_BLEND        = 0x4,
    RM_DRAW         = 0x8
};

enum BlendFunc
{
    BF_BLEND,
    BF_ADD
};

enum DrawMode
{
    DM_DEFAULT,
    DM_SPHERE_MAP
};

typedef struct SlideWindow
{
    u32 offset;
    u32 face_count;
} SlideWindow;

typedef struct Surface
{
    struct Resource *texture, *envmap;
    GLenum type, index_type;
    int face_count;
    unsigned vertex_format;

    struct
    {
        Vector4f ambient;
        Vector4f diffuse;
        Vector4f specular;
        Vector4f emission;
        float shininess;
    } material;

    unsigned render_mode;
    int draw_mode;
    int blend_func;
    
    u8 *vertices;
    u8 *indices;

    /* for progressive */
    int sw_count;
    SlideWindow *swis;
} Surface;

typedef struct Visual
{
    BoundingBox bbox;
    BoundingSphere bsphere;

    int surface_count;
    Surface *surfaces;
} Visual;

void DrawVisual(Visual *v, float quality, int mode);
void FreeVisual(Visual *v);

#endif
