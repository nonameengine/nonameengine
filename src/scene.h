#ifndef SCENE_H
#define SCENE_H

#include "collision.h"
#include "resource_manager.h"
#include "vector.h"

#include "script/script.h"

#define RENDER_QUEUE_CAPACITY 32767

enum CollisionType
{
    COLLISION_NONE,
    COLLISION_BOX,
    COLLISION_SPHERE
};

typedef struct Object
{
    Vector3f position;
    Vector3f rotation;
    Vector3f scale;

    Resource *model;

    BoundingBox bbox;
    BoundingSphere bsphere;
} Object;

typedef struct CollisionObject
{
    int type;

    union
    {
        BoundingBox bbox;
        BoundingSphere bsphere;
    };
} CollisionObject;

typedef struct DynamicObject
{
    Object base;
    CollisionObject collision;
    
    char *name;

    struct DynamicObject *next, *prev;
} DynamicObject;

typedef struct Light
{
    Vector4f position;
    Vector4f ambient;
    Vector4f diffuse;
    Vector4f specular;

    float range;

    float constant_attenuation;
    float linear_attenuation;
    float quadratic_attenuation;

    Vector3f spot_direction;
    float spot_exponent;
    float spot_cutoff;
} Light;

typedef struct Map
{
    int collision_count;
    int object_count;
    int light_count;

    BoundingBox exit;
    
    Object *objects;
    CollisionObject *collision;
    Light *lights;

    DynamicObject *dynamics;

    JSObject *script_object;
} Map;

extern Map *Scene;

void UpdateObjectBounding(Object *o);

const CollisionObject *TestCollision(CollisionObject o);

int LoadScene(char *file_name);
void DrawScene(void);
void FreeScene(void);

#endif
