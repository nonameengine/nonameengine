#ifdef _WIN32
#include <windows.h>
#endif
#include <GL/gl.h>
#include <string.h>
#include <malloc.h>
#include <stdio.h>
#include "dynamic_object.h"

DynamicObject *CreateDynamicObject(char *name)
{
    DynamicObject *obj = malloc(sizeof(DynamicObject));
    memset(obj,0,sizeof(DynamicObject));

    obj->base.scale.x = 1.0f;
    obj->base.scale.y = 1.0f;
    obj->base.scale.z = 1.0f;

    if(GetDynamicObject(name) != 0)
    {
        char *new_name = alloca(strlen(name) + 7); /* 7 == strlen("_65535") +  null */
        unsigned short i = 1;

        do
        {
            if(i == 65535)
            {
                free(obj);
                return 0;
            }
                    
            sprintf(new_name,"%s_%hu",name,i++);
        }
        while(GetDynamicObject(new_name) != 0);

        obj->name = strdup(new_name);
    }
    else
        obj->name = strdup(name);

    if(Scene->dynamics)
        Scene->dynamics->prev = obj;

    obj->next = Scene->dynamics;
    
    Scene->dynamics = obj;

    return obj;
}

void RemoveDynamicObject(DynamicObject *obj)
{
    if(obj->next)
        obj->next->prev = obj->prev;
    if(obj->prev)
        obj->prev->next = obj->next;

    if(obj->base.model)
        ReleaseResource(obj->base.model);

    free(obj->name);
    free(obj);
}

void RemoveDynamicObjectByName(char *name)
{
    DynamicObject *i = Scene->dynamics;

    while(i)
    {
        if(stricmp(i->name,name) == 0)
        {
            if(i->next)
                i->next->prev = i->prev;
            if(i->prev)
                i->prev->next = i->next;

            if(i->base.model)
                ReleaseResource(i->base.model);

            free(i->name);
            free(i);

            return;
        }

        i = i->next;
    }
}

DynamicObject *GetDynamicObject(char *name)
{
    DynamicObject *i = Scene->dynamics;

    while(i)
    {
        if(stricmp(i->name,name) == 0)
            return i;

        i = i->next;
    }

    return 0;
}

void UpdateDynamicObjectBounding(DynamicObject *o)
{
    UpdateObjectBounding(&o->base);

    if(o->collision.type == COLLISION_BOX)
        o->collision.bbox = o->base.bbox;
    if(o->collision.type == COLLISION_SPHERE)
        o->collision.bsphere = o->base.bsphere;
}
