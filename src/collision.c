#include <math.h>
#include "collision.h"

int FrustumContainsPoint(Frustum f, Vector3f point)
{
    float dist;
    int i, result = INSIDE;

    for(i = 0; i < 6; i++)
    {
        dist = f.planes[i].normal.x * point.x + f.planes[i].normal.y * point.y + f.planes[i].normal.z + f.planes[i].dist;

        if(dist < 0)
            return OUTSIDE;
        if(dist == 0.0)
            result = INTERSECT;
    }

    return result;
}

int FrustumContainsBSphere(Frustum f, BoundingSphere bs)
{
    float dist;
    int i, result = INSIDE;

    for(i = 0; i < 6; i++)
    {
        dist = f.planes[i].normal.x * bs.center.x + f.planes[i].normal.y * bs.center.y + f.planes[i].normal.z * bs.center.z + f.planes[i].dist;

        if(dist < -bs.radius)
            return OUTSIDE;

        if(fabs(dist) < bs.radius)
            result = INTERSECT;
    }

    return result;
}
/*
int FrustumContainsBBox(Frustum f, BoundingBox bbox)
{
    Vector3f corners[8];   
    float dist;
    int i, j, in, in_frustum = 0;

    GetBBoxCorners(bbox,corners);

    for(i = 0; i < 6; i++)
    {
        in = 0;
        
        for(j = 0; j < 8; j++)
        {
            dist = Vector3f_dot(f.planes[i].normal,corners[j]) + f.planes[i].dist;

            if(dist >= 0.0)
                in++;
        }

        if(in == 0)
            return 0;
        if(in == 8)
            in_frustum++;
    }

    if(in_frustum == 6)
        return 1;

    return 2;
}
*/

int FrustumContainsBBox(Frustum f, BoundingBox bbox)
{
    Vector3f n, p;
    int i, result = INSIDE;

    for(i = 0; i < 6; i++)
    {
        if(f.planes[i].normal.x >= 0.0)
        {
            p.x = bbox.max.x;
            n.x = bbox.min.x;
        }
        else
        {
            p.x = bbox.min.x;
            n.x = bbox.max.x;
        }

        if(f.planes[i].normal.y >= 0.0)
        {
            p.y = bbox.max.y;
            n.y = bbox.min.y;
        }
        else
        {
            p.y = bbox.min.y;
            n.y = bbox.max.y;
        }

        if(f.planes[i].normal.z >= 0.0)
        {
            p.z = bbox.max.z;
            n.z = bbox.min.z;
        }
        else
        {
            p.z = bbox.min.z;
            n.z = bbox.max.z;
        }

        if(Vector3f_dot(p,f.planes[i].normal) + f.planes[i].dist < 0.0)
            return OUTSIDE;
        if(Vector3f_dot(n,f.planes[i].normal) + f.planes[i].dist < 0.0)
            result = INTERSECT;
    }

    return result;
}

int BSphereIntersectBSphere(BoundingSphere bs1, BoundingSphere bs2)
{
    float dist = Vector3f_distance_to(bs1.center,bs2.center);

    if(dist - bs2.radius < bs1.radius)
        return INSIDE;
    if(dist < bs1.radius + bs2.radius)
        return INTERSECT;

    return OUTSIDE;
}

int BSphereContainsPoint(BoundingSphere bsphere, Vector3f point)
{
    if(Vector3f_distance_to(bsphere.center,point) > bsphere.radius)
        return OUTSIDE;

    return INSIDE;
}

int BBoxIntersectBBox(BoundingBox bb1, BoundingBox bb2)
{
    Vector3f corners[8];
    int i;
    
    if((bb1.max.x > bb2.max.x && bb1.max.y > bb2.max.y && bb1.max.z > bb2.max.z) &&
    (bb1.min.x < bb2.min.x && bb1.min.y < bb2.min.y && bb2.min.z < bb2.min.z)) /* bb2 inside bb1 */
        return INSIDE;
    if((bb1.max.x < bb2.max.x && bb1.max.y < bb2.max.y && bb1.max.z < bb2.max.z) &&
    (bb1.min.x > bb2.min.x && bb1.min.y > bb2.min.y && bb1.min.z > bb2.min.z)) /* bb1 inside bb2 */
        return INSIDE;

    GetBBoxCorners(bb1,corners);

    for(i = 0; i < 8; i++)
        if(BBoxContainsPoint(bb2,corners[i]) != OUTSIDE)
            return INTERSECT;

    GetBBoxCorners(bb2,corners);

    for(i = 0; i < 8; i++)
        if(BBoxContainsPoint(bb1,corners[i]) != OUTSIDE)
            return INTERSECT;

    return OUTSIDE;
}

int BBoxIntersectBSphere(BoundingBox bbox, BoundingSphere bsphere)
{
    float dist;
    Vector3f p;

    p.x = bsphere.center.x < bbox.max.x ? bsphere.center.x : bbox.max.x;
    p.y = bsphere.center.y < bbox.max.y ? bsphere.center.y : bbox.max.y;
    p.z = bsphere.center.z < bbox.max.z ? bsphere.center.z : bbox.max.z;

    if(bbox.min.x > p.x)
        p.x = bbox.min.x;
    if(bbox.min.y > p.y)
        p.y = bbox.min.y;
    if(bbox.min.z > p.z)
        p.z = bbox.min.z;

    dist = Vector3f_distance_to(bsphere.center,p);

/*    if(dist < -bsphere.radius) 
        return INSIDE; */
    if(dist < bsphere.radius)
        return INTERSECT;

    return OUTSIDE;
}

int BBoxContainsPoint(BoundingBox bbox, Vector3f point)
{
    if(point.x > bbox.max.x || point.y > bbox.max.y || point.z > bbox.max.z)
        return OUTSIDE;
    if(point.x < bbox.min.x || point.y < bbox.min.y || point.z < bbox.min.z)
        return OUTSIDE;

    return INSIDE;
}

/*
BoundingBox TransformBBox(Matrix4x4 m, BoundingBox bbox, Vector3f corners[8])
{
    Vector3f center;
    Vector3f corners[8];
    int i;

    GetBBoxCorners(bbox,corners);
    center = Vector3f_transform(GetBBoxCenter(bbox),m);

    bbox.min = center;
    bbox.max = center;

    for(i = 0; i < 8; i++)
    {
        corners[i] = Vector3f_transform(corners[i],m);

        if(bbox.max.x < corners[i].x)
            bbox.max.x = corners[i].x;
        if(bbox.max.y < corners[i].y)
            bbox.max.y = corners[i].y;
        if(bbox.max.z < corners[i].z)
            bbox.max.z = corners[i].z;

        if(bbox.min.x > corners[i].x)
            bbox.min.x = corners[i].x;
        if(bbox.min.y > corners[i].y)
            bbox.min.y = corners[i].y;
        if(bbox.min.z > corners[i].z)
            bbox.min.z = corners[i].z;
    }

    return bbox;
}
*/

BoundingBox TransformBBox(Matrix4x4 transform, BoundingBox bbox)
{
    Vector3f double_size;
    Vector3f vx,vy,vz;

    double_size = Vector3f_sub(bbox.max,bbox.min);
    vx = Vector3f_muln(Vector4f_xyz(transform.v[0]),double_size.x);
    vy = Vector3f_muln(Vector4f_xyz(transform.v[1]),double_size.y);
    vz = Vector3f_muln(Vector4f_xyz(transform.v[2]),double_size.z);

    bbox.max = bbox.min = Vector3f_transform(bbox.min,transform);

    if(vx.x < 0.0)
        bbox.min.x += vx.x;
    else
        bbox.max.x += vx.x;

    if(vx.y < 0.0)
        bbox.min.y += vx.y;
    else
        bbox.max.y += vx.y;

    if(vx.z < 0.0)
        bbox.min.z += vx.z;
    else
        bbox.max.z += vx.z;

    if(vy.x < 0.0)
        bbox.min.x += vy.x;
    else
        bbox.max.x += vy.x;

    if(vy.y < 0.0)
        bbox.min.y += vy.y;
    else
        bbox.max.y += vy.y;

    if(vy.z < 0.0)
        bbox.min.z += vy.z;
    else
        bbox.max.z += vy.z;

    if(vz.x < 0.0)
        bbox.min.x += vz.x;
    else
        bbox.max.x += vz.x;

    if(vz.y < 0.0)
        bbox.min.y += vz.y;
    else
        bbox.max.y += vz.y;

    if(vz.z < 0.0)
        bbox.min.z += vz.z;
    else
        bbox.max.z += vz.z;

    return bbox;       
}

void GetBBoxCorners(BoundingBox bbox, Vector3f corners[8])
{
    /* bottom */
    corners[0] = bbox.min;

    corners[1].x = bbox.min.x;
    corners[1].y = bbox.min.y;
    corners[1].z = bbox.max.z;

    corners[2].x = bbox.max.x;
    corners[2].y = bbox.min.y;
    corners[2].z = bbox.max.z;

    corners[3].x = bbox.max.x;
    corners[3].y = bbox.min.y;
    corners[3].z = bbox.min.z;

    /* top */
    corners[4].x = bbox.min.x;
    corners[4].y = bbox.max.y;
    corners[4].z = bbox.min.z;

    corners[5].x = bbox.min.x;
    corners[5].y = bbox.max.y;
    corners[5].z = bbox.max.z;

    corners[6] = bbox.max;

    corners[7].x = bbox.max.x;
    corners[7].y = bbox.max.y;
    corners[7].z = bbox.min.z;
}

Vector3f GetBBoxCenter(BoundingBox bbox)
{
    Vector3f c;

    c.x = bbox.min.x + (bbox.max.x - bbox.min.x) / 2;
    c.y = bbox.min.y + (bbox.max.y - bbox.min.y) / 2;
    c.z = bbox.min.z + (bbox.max.z - bbox.min.z) / 2;

    return c;
}

BoundingBox MergeBBox(BoundingBox bb1, BoundingBox bb2)
{
    if(bb1.min.x > bb2.min.x)
        bb1.min.x = bb2.min.x;
    if(bb1.min.y > bb2.min.y)
        bb1.min.y = bb2.min.y;
    if(bb1.min.z > bb2.min.z)
        bb1.min.z = bb2.min.z;

    if(bb1.max.x < bb2.max.x)
        bb1.max.x = bb2.max.x;
    if(bb1.max.y < bb2.max.y)
        bb1.max.y = bb2.max.y;
    if(bb1.max.z < bb2.max.z)
        bb1.max.z = bb2.max.z;

    return bb1;
}
