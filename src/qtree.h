#ifndef QTREE_H
#define QTREE_H

#include "scene.h"
#include "collision.h"

enum QTreeType
{
    QTREE_NODE,
    QTREE_LEAF
};

typedef struct QTree
{
    int type;
    struct QTree *parent;
    struct QTree *branches;

    /* replace this */
    int object_count;
    
    union
    {
        Object **objects;
        CollisionObject **collision_objects;
    };

    Vector3f center;
    BoundingBox bbox;
} QTree;

QTree *CreateQTree(float width, int depth);
void DestroyQTree(QTree *tree);

void AddStaticObjectToQTree(QTree *tree, Object *object);
void WalkStaticObjectsQTree(QTree *tree, int check_collision, Object **queue, unsigned *count);

void AddCollisionObjectToQTree(QTree *tree, CollisionObject *object);

#endif
