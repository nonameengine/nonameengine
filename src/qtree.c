#include <stdlib.h>
#include <stdio.h>
#include "qtree.h"

#define QTREE_CAPACITY 256

#define MAX_UP 100.f
#define MIN_UP -100.f

void create_nodes(QTree *node, int depth)
{
    int i;
    BoundingBox bbox;
    Vector3f hs;

    if(depth == 0)
    {
        node->type = QTREE_LEAF;

        node->object_count = 0;
        node->objects = malloc(QTREE_CAPACITY * sizeof(Object*));
        return;
    }
    else
        node->type = QTREE_NODE;
    
    node->branches = malloc(sizeof(QTree) * 4);

    node->center = GetBBoxCenter(node->bbox);

    /* get bbox half size */
    hs = Vector3f_divn(
    Vector3f_sub(node->bbox.max,node->bbox.min),2.f);

    bbox.min = node->bbox.min;
    bbox.max = Vector3f_sub(node->bbox.max,hs);
    
    bbox.max.y = MAX_UP;
    bbox.min.y = MIN_UP;


    node->branches[0].bbox = bbox;

    node->branches[1].bbox = bbox;
    node->branches[1].bbox.min.x += hs.x;
    node->branches[1].bbox.max.x += hs.x;

    node->branches[2].bbox = bbox;
    node->branches[2].bbox.min.z += hs.z;
    node->branches[2].bbox.max.z += hs.z;

    node->branches[3].bbox = bbox;
    node->branches[3].bbox.min.x += hs.x;
    node->branches[3].bbox.min.z += hs.z;
    node->branches[3].bbox.max.x += hs.x;
    node->branches[3].bbox.max.z += hs.z;

    for(i = 0; i < 4; i++)
    {
        node->branches[i].parent = node;        
        create_nodes(&node->branches[i],depth - 1);
    }
}

QTree *CreateQTree(float width, int depth)
{
    QTree *out = malloc(sizeof(QTree));

    out->type = QTREE_NODE;
    out->parent = 0;

    out->bbox.min.x = -width / 2;
    out->bbox.min.y = MIN_UP;
    out->bbox.min.z = -width / 2;

    out->bbox.max.x = width / 2;
    out->bbox.max.y = MAX_UP;
    out->bbox.max.z = width / 2;

    create_nodes(out,depth);

    return out;
}

void DestroyQTree(QTree *tree)
{
    int i;
    
    if(tree->type == QTREE_LEAF)
    {
        free(tree->objects);
        return;
    }

    for(i = 0; i < 4; i++)
        DestroyQTree(&tree->branches[i]);

    free(tree->branches);

    if(tree->parent == 0)
        free(tree);
}

void AddStaticObjectToQTree(QTree *tree, Object *object)
{
    Vector3f c;

    tree->bbox = MergeBBox(tree->bbox,object->bbox);

    if(tree->type == QTREE_LEAF)
    {
        if(tree->object_count > QTREE_CAPACITY-1)
        {
            printf("qtree overflow!");
            return;
        }

        tree->objects[tree->object_count++] = object;
        return;
    }

    c = GetBBoxCenter(object->bbox);

    if(c.z >= tree->center.z)
    {
        if(c.x >= tree->center.x)
            AddStaticObjectToQTree(&tree->branches[3],object);
        else
            AddStaticObjectToQTree(&tree->branches[2],object);
    }
    else
    {
        if(c.x >= tree->center.x)
            AddStaticObjectToQTree(&tree->branches[1],object);
        else
            AddStaticObjectToQTree(&tree->branches[0],object);
    }
}

void WalkStaticObjectsQTree(QTree *node, int check_collision, Object **queue, unsigned *count)
{
    int i, intersect;

    intersect = FrustumContainsBBox(Camera.frustum,node->bbox);

    if(intersect == 0 && BBoxContainsPoint(node->bbox,Camera.position) == OUTSIDE)
        return;
    
    if(node->type == QTREE_LEAF)
    {
        for(i = 0; i < node->object_count && *count < RENDER_QUEUE_CAPACITY-1; i++)
        {
            if(FrustumContainsBSphere(Camera.frustum,node->objects[i]->bsphere) == OUTSIDE)
                continue;
            if(FrustumContainsBBox(Camera.frustum,node->objects[i]->bbox) == OUTSIDE)
                continue;
            
            queue[*count] = node->objects[i];
            *count = *count+1;
        }
    }
    else
    {   
        for(i = 0; i < 4; i++)
            WalkStaticObjectsQTree(&node->branches[i], intersect == 2, queue, count);
    }
}

void AddCollisionObjectToQTree(QTree *tree, CollisionObject *object)
{
    BoundingBox bb;
    Vector3f c;
    
    if(object->type == COLLISION_SPHERE)
    {
        bb.min = Vector3f_subn(object->bsphere.center,object->bsphere.radius);
        bb.max = Vector3f_addn(object->bsphere.center,object->bsphere.radius);
    }
    else
        bb = object->bbox;

    tree->bbox = MergeBBox(bb,tree->bbox);

    if(tree->type == QTREE_LEAF)
    {
        if(tree->object_count > QTREE_CAPACITY-1)
        {
            printf("qtree oveflow!\n");
            return;
        }

        tree->collision_objects[tree->object_count++] = object;
        return;
    }

    c = GetBBoxCenter(bb);

    if(c.z >= tree->center.z)
    {
        if(c.x >= tree->center.x)
            AddCollisionObjectToQTree(&tree->branches[3],object);
        else
            AddCollisionObjectToQTree(&tree->branches[2],object);
    }
    else
    {
        if(c.x >= tree->center.x)
            AddCollisionObjectToQTree(&tree->branches[1],object);
        else
            AddCollisionObjectToQTree(&tree->branches[0],object);
    }
}
