#ifndef COLLISION_H
#define COLLISION_H

#include "flying_camera.h"
#include "vector.h"

enum Intersection
{
    OUTSIDE,
    INSIDE,
    INTERSECT
};

typedef struct BoundingSphere
{
    Vector3f center;
    float radius;
} BoundingSphere;

typedef struct BoundingBox
{
    Vector3f min, max;
} BoundingBox;

int FrustumContaintPoint(Frustum f, Vector3f point);
int FrustumContainsBSphere(Frustum f, BoundingSphere bsphere);
int FrustumContainsBBox(Frustum f, BoundingBox bbox);


int BSphereIntersectBSphere(BoundingSphere bs1, BoundingSphere bs2);
int BSphereContainsPoint(BoundingSphere bsphere, Vector3f point);


int BBoxContainsPoint(BoundingBox bbox, Vector3f point);
int BBoxIntersectBBox(BoundingBox bb1, BoundingBox bb2);
int BBoxIntersectBSphere(BoundingBox bbox, BoundingSphere bsphere);

BoundingBox TransformBBox(Matrix4x4 m, BoundingBox bbox);
BoundingBox MergeBBox(BoundingBox bb1, BoundingBox bb2);

void GetBBoxCorners(BoundingBox bbox, Vector3f corners[8]);
Vector3f GetBBoxCenter(BoundingBox bbox);

#endif
