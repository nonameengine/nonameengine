#ifndef UTILS_H
#define UTILS_H

#include "system/vfs.h"

static __inline u8 r_u8(File *f)
{
    u8 o;
    FileRead(&o,sizeof(u8),1,f);
    return o;
}

static __inline u16 r_u16(File *f)
{
    u16 o;
    FileRead(&o,sizeof(u16),1,f);
    return o;
}

static __inline u32 r_u32(File *f)
{
    u32 o;
    FileRead(&o,sizeof(u32),1,f);
    return o;
}

void r_string(char *out, int len, File *f);

#endif
