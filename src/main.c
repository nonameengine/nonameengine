#ifdef _WIN32
#include <windows.h>
#endif
#include <gl/GL.h>
#include <gl/glu.h>
#include <time.h>

#include "main.h"

#include "system/settings.h"
#include "system/vfs.h"
#include "system/config.h"

#include "misc/grid.h"
#include "misc/font.h"
#include "misc/lens_flare.h"
#include "misc/sky.h"

#include "gl_extensions.h"
#include "flying_camera.h"
#include "scene.h"

#include "script/script.h"

WindowInfo Window;
unsigned poly_count;

struct
{
    int show_info;

    int draw_grid;
    int draw_collision;
} Debug;

float sunx,suny,sunz;

void debug_draw_collision(void)
{
    int i;
    Vector3f c[8];

    if(!Scene)
        return;
    
    glColor3f(1.0f,1.0f,1.0f);

    for(i = 0; i < Scene->collision_count; i++)
    {
        if(Scene->collision[i].type == COLLISION_BOX)
        {
            GetBBoxCorners(Scene->collision[i].bbox,c);
    
            /* top */
                
            glBegin(GL_LINE_LOOP);
    
            glVertex3f(c[0].x,c[0].y,c[0].z);
            glVertex3f(c[1].x,c[1].y,c[1].z);
    
            glVertex3f(c[1].x,c[1].y,c[1].z);
            glVertex3f(c[2].x,c[2].y,c[2].z);
    
            glVertex3f(c[2].x,c[2].y,c[2].z);
            glVertex3f(c[3].x,c[3].y,c[3].z);
    
            glEnd();
    
            /* bottom */
    
            glBegin(GL_LINE_LOOP);
    
            glVertex3f(c[4].x,c[4].y,c[4].z);
            glVertex3f(c[5].x,c[5].y,c[5].z);
    
            glVertex3f(c[5].x,c[5].y,c[5].z);
            glVertex3f(c[6].x,c[6].y,c[6].z);
    
            glVertex3f(c[6].x,c[6].y,c[6].z);
            glVertex3f(c[7].x,c[7].y,c[7].z);
    
            glEnd();
    
            /* sides */
    
            glBegin(GL_LINES);
    
            glVertex3f(c[0].x,c[0].y,c[0].z);
            glVertex3f(c[4].x,c[4].y,c[4].z);
    
            glVertex3f(c[1].x,c[1].y,c[1].z);
            glVertex3f(c[5].x,c[5].y,c[5].z);
    
            glVertex3f(c[2].x,c[2].y,c[2].z);
            glVertex3f(c[6].x,c[6].y,c[6].z);
    
            glVertex3f(c[3].x,c[3].y,c[3].z);
            glVertex3f(c[7].x,c[7].y,c[7].z);
    
            glEnd();      
        }
        else
        {
            GLUquadricObj *sphere = gluNewQuadric();

            if(sphere)
            {
                gluQuadricDrawStyle(sphere,GLU_LINE);

                glPushMatrix();
                glTranslatef(Scene->collision[i].bsphere.center.x,Scene->collision[i].bsphere.center.y,Scene->collision[i].bsphere.center.z);

                gluSphere(sphere,Scene->collision[i].bsphere.radius,10,10);            

                glPopMatrix();

                gluDeleteQuadric(sphere);
            }
        }
    }
}

void Reshape(void)
{
    glViewport(0,0,Window.width,Window.height);

    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();

    if(Window.width >= Window.height)
        gluPerspective(GetFloatSetting("fov",70.0f), (float)Window.width / Window.height,
                GetFloatSetting("near_plane",1.0), GetFloatSetting("far_plane",500.0f));
    else
        gluPerspective(GetFloatSetting("fov",70.0f), (float)Window.height / Window.width,
                GetFloatSetting("near_plane",1.0), GetFloatSetting("far_plane",500.0f));

    glMatrixMode(GL_MODELVIEW);

    UpdateDepthBufferForFlares();
}

void Display(void)
{
    clock_t t = clock();

    glClear(GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT);

    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();

    glRotatef(Camera.rotation.x,1.0f,0.0f,0.0f);
    glRotatef(Camera.rotation.y,0.0f,1.0f,0.0f);
    
    glTranslatef(-Camera.position.x,-Camera.position.y,-Camera.position.z);

    DrawSky();

    glEnable(GL_DEPTH_TEST);
    glDisable(GL_TEXTURE_2D);

    if(Debug.draw_grid)
        DrawGrid();

    glColor3f(1.f,1.f,1.f);
    
    glEnable(GL_CULL_FACE);
/*  
    glEnable(GL_FOG);
*/
    DrawScene();
/*  
    glDisable(GL_FOG);
*/

    glDisable(GL_DEPTH_TEST);
    glDisable(GL_TEXTURE_2D);

    if(Debug.draw_collision)
        debug_draw_collision();

/*
    {
        double x = Camera.position.x - 1.4,
               y = Camera.position.y + 0.10,
               z = Camera.position.z - 1.0;
        double mv[16], p[16];
        int v[4];

        glGetDoublev(GL_MODELVIEW_MATRIX,mv);
        glGetDoublev(GL_PROJECTION_MATRIX,p);
        glGetIntegerv(GL_VIEWPORT,v);

        gluProject(x,y,z,mv,p,v,&x,&y,&z);

        sunx = x;
        suny = y;
        sunz = z;
    }
*/
    /* Begin 2D drawing */

    glLoadIdentity();

    glMatrixMode(GL_PROJECTION);
    glPushMatrix();

    glLoadIdentity();

    glOrtho(0.0,Window.width,0.0,Window.height,-1.0,1.0);
/*
    if(sunz < 1.0)
        DrawLensFlares(Vector2f_set(sunx,suny));
*/
    if(Debug.show_info)
    {
        glColor3f(1.0f,1.0f,1.0f);
        DisplayText(10,Window.height - 32,"Pos: %f %f %f\nDir: %f %f %f",
        Camera.position.x,Camera.position.y,Camera.position.z,
        Camera.rotation.x,Camera.rotation.y,Camera.rotation.z);

        DisplayText(10,20,"Poly: %u",
        poly_count);
    
        glFlush();
        
        /*
        delta_time = (clock() - t) / (CLOCKS_PER_SEC / 60);
        */

        t = clock() - t;

        if(t == 0)
                DisplayText(10,36,"FPS: inf");
        else
                DisplayText(10,36,"FPS: %.2lf",
                (double)1.0/((double)t / CLOCKS_PER_SEC));
    
    }

    glPopMatrix(); 
}

void Init(void)
{
    Vector4f fog_color, clear_color;

    clear_color = GetVector4fSetting("clear_color",Vector4f_set(0.5f,0.5f,0.5f,1.0f));
    fog_color = GetVector4fSetting("fog_color",Vector4f_set(0.5f,0.5f,0.5f,1.0f));

    Camera.speed = GetFloatSetting("cam_speed",1.5f);
    Camera.sensitivity = GetFloatSetting("cam_sensitivity",1.0f);
    Camera.position = GetVector3fSetting("cam_pos",Vector3f_set(0.f,1.f,3.f));

    Debug.draw_collision = GetIntSetting("debug_draw_collision",0);
    Debug.draw_grid = GetIntSetting("debug_draw_grid",0);
    Debug.show_info = GetIntSetting("debug_show_info",0);

    glClearColor(clear_color.r,clear_color.g,clear_color.b,clear_color.a);

    glDepthFunc(GL_LEQUAL);
    glAlphaFunc(GL_GEQUAL,0.5);

    glFogi(GL_FOG_MODE,GL_EXP);
    glFogfv(GL_FOG_COLOR,(float*)&fog_color);
    glFogf(GL_FOG_START,GetFloatSetting("fog_start",130.f));
    glFogf(GL_FOG_END,GetFloatSetting("fog_end",150.f));
    glFogf(GL_FOG_DENSITY,0.01);

    glTexEnvi(GL_TEXTURE_ENV,GL_TEXTURE_ENV_MODE,GL_MODULATE);

    glShadeModel(GL_SMOOTH);
    glEnable(GL_NORMALIZE);

/* 
    glHint(GL_PERSPECTIVE_CORRECTION_HINT,GL_NICEST);
*/

    InitVFS();
    
    InitFont();
    InitGrid(GetIntSetting("grid_size",10));
    InitSky();
    InitFlares();
    
    if(InitScriptingEngine() != 0)
    {
        printf("Error initializing SpiderMonkey, exiting...");
        exit(-1);
    }

    ExecuteScript(JS_GlobalObject,"test.js");

    LoadScene("map.cfg");
}

void Exit(void)
{
    FreeScene();

    FreeVFS();
 
    ShutdownScriptingEngine();
    
    DeleteGrid();
    DeleteFont();
    DeleteSky();
    DeleteFlares();
}
