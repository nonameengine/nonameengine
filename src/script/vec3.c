#include "script.h"

JSClass vec3_class = {
    "Vec3", 0,
    JS_PropertyStub, JS_PropertyStub, JS_PropertyStub, JS_PropertyStub,
    JS_EnumerateStub, JS_ResolveStub, JS_ConvertStub, JS_FinalizeStub,
    JSCLASS_NO_OPTIONAL_MEMBERS
};

static JSBool vec3_construct
(JSContext *cx, JSObject *obj, uintN argc, jsval *vp, jsval *rval)
{
    jsval vx, vy, vz;
    
    if(argc == 0)
    {
        vx = JSVAL_ZERO;
        vy = JSVAL_ZERO;
        vz = JSVAL_ZERO;

        if(
        !JS_SetProperty(cx,obj,"x",&vx) ||
        !JS_SetProperty(cx,obj,"y",&vy) ||
        !JS_SetProperty(cx,obj,"z",&vz))
            return JS_FALSE;
    }
    if(argc == 1) /* copy from another vector */
    {
        JSObject *vec;

        if(!JS_ValueToObject(cx,vp[0],&vec))
            return JS_FALSE;

        if(!JS_GetProperty(cx,vec,"x",&vx) ||
           !JS_GetProperty(cx,vec,"y",&vy) ||
           !JS_GetProperty(cx,vec,"z",&vz))
            return JS_FALSE;

        if(!JSVAL_IS_NUMBER(vx)||!JSVAL_IS_NUMBER(vy)||!JSVAL_IS_NUMBER(vz))
            return JS_FALSE;

        if(
        !JS_SetProperty(cx,obj,"x",&vx) ||
        !JS_SetProperty(cx,obj,"y",&vy) ||
        !JS_SetProperty(cx,obj,"z",&vz))
            return JS_FALSE;
    }
    else if(argc == 3)
    {
        if(!JSVAL_IS_NUMBER(vp[0])||
           !JSVAL_IS_NUMBER(vp[1])||
           !JSVAL_IS_NUMBER(vp[2]))
            return JS_FALSE;

        if(
        !JS_SetProperty(cx,obj,"x",&vp[0]) ||
        !JS_SetProperty(cx,obj,"y",&vp[1]) ||
        !JS_SetProperty(cx,obj,"z",&vp[2]))
            return JS_FALSE;
    }
    else
        return JS_FALSE;
        
    return JS_TRUE;
}

static JSBool vec3_add
(JSContext *cx, JSObject *obj, uintN argc, jsval *vp, jsval *rval)
{
    double x1, y1, z1;
    double x2, y2, z2;

    jsval vx, vy, vz;

    GetVec3XYZ(cx,obj,&x1,&y1,&z1);

    if(argc == 1)
    {
        JSObject *vec;

        if(!JS_ValueToObject(cx,vp[0],&vec))
            return JS_FALSE;
        if(!vec)
            return JS_FALSE;
        
        GetVec3XYZ(cx,vec,&x2,&y2,&z2);
    }
    else if(argc == 3)
    {
        if(!JS_ValueToNumber(cx,vp[0],&x2) ||
           !JS_ValueToNumber(cx,vp[1],&y2) ||
           !JS_ValueToNumber(cx,vp[2],&z2))
            return JS_FALSE;
    }
    else
        return JS_FALSE;

    x1 += x2;
    y1 += y2;
    z1 += z2;

    if(
    !JS_NewNumberValue(cx,x1,&vx) ||
    !JS_NewNumberValue(cx,y1,&vy) ||
    !JS_NewNumberValue(cx,z1,&vz))
        return JS_FALSE;

    if(
    !JS_SetProperty(cx,obj,"x",&vx) ||
    !JS_SetProperty(cx,obj,"y",&vy) ||
    !JS_SetProperty(cx,obj,"z",&vz))
        return JS_FALSE;
    
    return  JS_TRUE;
}

static JSBool vec3_sub
(JSContext *cx, JSObject *obj, uintN argc, jsval *vp, jsval *rval)
{
    double x1, y1, z1;
    double x2, y2, z2;

    jsval vx, vy, vz;

    GetVec3XYZ(cx,obj,&x1,&y1,&z1);

    if(argc == 1)
    {
        JSObject *vec;

        if(!JS_ValueToObject(cx,vp[0],&vec))
            return JS_FALSE;
        if(!vec)
            return JS_FALSE;
        
        GetVec3XYZ(cx,vec,&x2,&y2,&z2);
    }
    else if(argc == 3)
    {
        if(!JS_ValueToNumber(cx,vp[0],&x2) ||
           !JS_ValueToNumber(cx,vp[1],&y2) ||
           !JS_ValueToNumber(cx,vp[2],&z2))
            return JS_FALSE;
    }
    else
        return JS_FALSE;

    x1 -= x2;
    y1 -= y2;
    z1 -= z2;

    if(
    !JS_NewNumberValue(cx,x1,&vx) ||
    !JS_NewNumberValue(cx,y1,&vy) ||
    !JS_NewNumberValue(cx,z1,&vz))
        return JS_FALSE;

    if(
    !JS_SetProperty(cx,obj,"x",&vx) ||
    !JS_SetProperty(cx,obj,"y",&vy) ||
    !JS_SetProperty(cx,obj,"z",&vz))
        return JS_FALSE;
    
    return  JS_TRUE;
}

static JSBool vec3_scale
(JSContext *cx, JSObject *obj, uintN argc, jsval *vp, jsval *rval)
{
    double x, y, z;
    double scalar;

    jsval vx, vy, vz;

    if(argc == 0)
        return JS_FALSE;

    GetVec3XYZ(cx,obj,&x,&y,&z);

    if(!JS_ValueToNumber(cx,vp[0],&scalar))
        return JS_FALSE;

    x *= scalar;
    y *= scalar;
    z *= scalar;

    if(
    !JS_NewNumberValue(cx,x,&vx) ||
    !JS_NewNumberValue(cx,y,&vy) ||
    !JS_NewNumberValue(cx,z,&vz))
        return JS_FALSE;

    if(
    !JS_SetProperty(cx,obj,"x",&vx) ||
    !JS_SetProperty(cx,obj,"y",&vy) ||
    !JS_SetProperty(cx,obj,"z",&vz))
        return JS_FALSE;    
    
    return JS_TRUE;
}

static JSBool vec3_set
(JSContext *cx, JSObject *obj, uintN argc, jsval vp[], jsval *rval)
{
    if(argc == 1)
    {
        JSObject *vec;
        jsval vx, vy, vz;

        if(!JS_ValueToObject(cx,vp[0],&vec))
            return JS_FALSE;
        if(!vec)
            return JS_FALSE;

        if(
        !JS_GetProperty(cx,vec,"x",&vx) ||
        !JS_GetProperty(cx,vec,"y",&vy) ||
        !JS_GetProperty(cx,vec,"z",&vz))
            return JS_FALSE;

        if(
        !JS_SetProperty(cx,obj,"x",&vx) ||
        !JS_SetProperty(cx,obj,"y",&vy) ||
        !JS_SetProperty(cx,obj,"z",&vz))
            return JS_FALSE;
    }
    else if(argc == 3)
    {
        if(
        !JSVAL_IS_NUMBER(vp[0])||
        !JSVAL_IS_NUMBER(vp[1])||
        !JSVAL_IS_NUMBER(vp[2]))
            return JS_FALSE;

        if(
        !JS_SetProperty(cx,obj,"x",&vp[0]) ||
        !JS_SetProperty(cx,obj,"y",&vp[1]) ||
        !JS_SetProperty(cx,obj,"z",&vp[2]))
            return JS_FALSE;
    }
    else
        return JS_FALSE;

    return JS_TRUE;
}

static JSPropertySpec vec3_properties[] = {
    {"x",0,JSPROP_ENUMERATE},
    {"y",0,JSPROP_ENUMERATE},
    {"z",0,JSPROP_ENUMERATE},
    {0}
};

static JSFunctionSpec vec3_functions[] = {
    {"add",vec3_add,3},
    {"sub",vec3_sub,3},
    {"scale",vec3_scale,1},
    {"set",vec3_set,3},
    {0}
};

JSObject *CreateVec3
(JSContext *cx, double x, double y, double z)
{
    JSObject *vec = JS_NewObject(cx,&vec3_class,NULL,NULL);
    jsval vx,vy,vz;

    JS_NewDoubleValue(cx,x,&vx);
    JS_NewDoubleValue(cx,y,&vy);
    JS_NewDoubleValue(cx,z,&vz);

    JS_SetProperty(cx,vec,"x",&vx);
    JS_SetProperty(cx,vec,"y",&vy);
    JS_SetProperty(cx,vec,"z",&vz);

    return vec;
}

void GetVec3XYZ
(JSContext *cx, JSObject *obj, double *x, double *y, double *z)
{
    jsval vx,vy,vz;

    if(!JS_GetProperty(cx,obj,"x",&vx) ||
       !JS_GetProperty(cx,obj,"y",&vy) ||
       !JS_GetProperty(cx,obj,"z",&vz))
        goto err;

    if(!JS_ValueToNumber(cx,vx,x) ||
       !JS_ValueToNumber(cx,vy,y) ||
       !JS_ValueToNumber(cx,vz,z))
        goto err;

    return;

    err:

    *x = 0.0;
    *y = 0.0;
    *z = 0.0;
}

void init_script_vec3(void)
{
    JS_InitClass(JS_ScriptContext,JS_GlobalObject,NULL,
    &vec3_class,vec3_construct,3,vec3_properties,vec3_functions,NULL,NULL);
}
