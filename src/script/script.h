#ifndef SCRIPT_H
#define SCRIPT_H

#if defined(_WIN32)
#define XP_WIN
#elif defined(__OS2__)
#define XP_OS2
#else
#define XP_UNIX
#endif

#include <jsapi.h>

#ifdef __WATCOMC__
JSBool _property_stub(JSContext *cx, JSObject *obj, jsval id, jsval *vp);
JSBool _enumerate_stub(JSContext *cx, JSObject *obj);
JSBool _resolve_stub(JSContext *cx, JSObject *obj, jsval id);
JSBool _convert_stub(JSContext *cx, JSObject *obj, JSType type, jsval *vp);
void _finalize_stub(JSContext *cx, JSObject *obj);

#define JS_PropertyStub _property_stub
#define JS_EnumerateStub _enumerate_stub
#define JS_ResolveStub _resolve_stub
#define JS_ConvertStub _convert_stub
#define JS_FinalizeStub _finalize_stub
#endif

extern JSContext *JS_ScriptContext;
extern JSObject  *JS_GlobalObject;

int       InitScriptingEngine(void);
void      ShutdownScriptingEngine(void);

int       ExecuteScript(JSObject *obj, char *file_name);

JSObject* CreateVec3(JSContext *cx, double x, double y, double z);
void      GetVec3XYZ(JSContext *cx, JSObject *obj, double *x, double *y, double *z);

#endif


