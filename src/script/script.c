#include "script.h"

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "system/vfs.h"

JSRuntime *rt;
JSContext *JS_ScriptContext;

JSObject *JS_GlobalObject;

void init_script_dynamic_object(void);
void init_script_vec3(void);

static void js_error_reporter(JSContext *cx, const char *message, JSErrorReport *report)
{
    printf("%s:%u [%s] %s\n",
    report->filename ? report->filename : "(null)",
    report->lineno,
    report->flags == JSREPORT_WARNING ? "warning" : "error",
    message);
}

static JSBool js_print(JSContext *cx, JSObject *obj, uintN argc, jsval *vp, jsval *rval)
{
    uintN i;

    if(argc == 0)
        return JS_FALSE;

    for(i = 0 ; i < argc; i++)
    {
        JSString *str = JS_ValueToString(cx,vp[i]);

        if(str)
            printf(JS_GetStringBytes(str));
    }

    fflush(stdout);
    
    return JS_TRUE;
}

static JSFunctionSpec global_functions[] = {
    {"print",js_print,1},
    {0}
};

int InitScriptingEngine(void)
{
    rt = JS_NewRuntime(16 * 1024 * 1024);

    if(!rt)
    {
        printf("Can't create JSRuntime.\n");
        return 1;
    }

    JS_ScriptContext = JS_NewContext(rt,8192);

    if(!JS_ScriptContext)
    {
        JS_DestroyRuntime(rt);
        printf("Can't create JSContext.\n");
        return 2;
    }

    JS_SetErrorReporter(JS_ScriptContext,js_error_reporter);
    JS_GlobalObject = JS_NewObject(JS_ScriptContext,NULL,NULL,NULL);

    JS_InitStandardClasses(JS_ScriptContext,JS_GlobalObject);
    JS_DefineFunctions(JS_ScriptContext,JS_GlobalObject,global_functions);

    init_script_vec3();
    init_script_dynamic_object();

    return 0;
}

void ShutdownScriptingEngine(void)
{
    JS_DestroyContext(JS_ScriptContext);
    JS_DestroyRuntime(rt);

    JS_ShutDown();
}

int ExecuteScript(JSObject *obj, char *file_name)
{
    char full_path[256] = "data\\scripts\\";

    File *f;
    char *buffer;
    unsigned length;

    jsval result;

    strcat(full_path,file_name);

    f = FileOpen(full_path,"rb");

    if(!f)
    {
        printf("Can't open script file '%s'\n",file_name);
        return 1;
    }

    FileSeek(f,0,SEEK_END);
    length = FileTell(f);
    FileSeek(f,0,SEEK_SET);

    buffer = malloc(length);
    FileRead(buffer,length,1,f);

    FileClose(f);

    if(JS_EvaluateScript(JS_ScriptContext,obj,buffer,length,file_name,1,&result) != JS_TRUE)
    {
        free(buffer);
        return 2;
    }

    free(buffer);

    return 0;
}
