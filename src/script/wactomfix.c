#include "script.h"

JSBool _property_stub(JSContext *cx, JSObject *obj, jsval id, jsval *vp)
{
    return JS_TRUE;
}

JSBool _enumerate_stub(JSContext *cx, JSObject *obj)
{
    return JS_TRUE;
}

JSBool _resolve_stub(JSContext *cx, JSObject *obj, jsval id)
{
    return JS_TRUE;
}

JSBool _convert_stub(JSContext *cx, JSObject *obj, JSType type, jsval *vp)
{
    return JS_TRUE;
}

void _finalize_stub(JSContext *cx, JSObject *obj)
{
    return;
}
