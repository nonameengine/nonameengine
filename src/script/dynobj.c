#include "script.h"

#include "../dynamic_object.h"

static JSClass dynobj_class = {
    "DynamicObject", 0,
    JS_PropertyStub,JS_PropertyStub,JS_PropertyStub,JS_PropertyStub,
    JS_EnumerateStub,JS_ResolveStub,JS_ConvertStub,JS_FinalizeStub,
    JSCLASS_NO_OPTIONAL_MEMBERS
};

static JSBool dynobj_commit
(JSContext *cx, JSObject *obj, uintN argc, jsval *vp, jsval *rval)
{
    DynamicObject *o;
    JSString *str;

    double x,y,z;
    JSObject *vec;
    
    jsval v;
    
    if(!JS_InstanceOf(cx,obj,&dynobj_class,NULL) ||
       !JS_GetProperty(cx,obj,"name",&v))
        return JS_FALSE;

    str = JS_ValueToString(cx,v);

    if(!str)
        return JS_FALSE;

    o = GetDynamicObject(JS_GetStringBytes(str));

    if(!o)
        return JS_FALSE;

    JS_GetProperty(cx,obj,"position",&v);
    
    if(!JS_ValueToObject(cx,v,&vec))
        return JS_FALSE;

    if(vec)
    {
        GetVec3XYZ(cx,vec,&x,&y,&z);
    
        o->base.position.x = x;
        o->base.position.y = y;
        o->base.position.z = z;
    }

    JS_GetProperty(cx,obj,"rotation",&v);

    if(!JS_ValueToObject(cx,v,&vec))
        return JS_FALSE;

    if(vec)
    {
        GetVec3XYZ(cx,vec,&x,&y,&z);
    
        o->base.rotation.x = x;
        o->base.rotation.y = y;
        o->base.rotation.z = z;
    }

    JS_GetProperty(cx,obj,"scale",&v);

    if(!JS_ValueToObject(cx,v,&vec))
        return JS_FALSE;

    if(vec)
    {
        GetVec3XYZ(cx,vec,&x,&y,&z);
    
        o->base.scale.x = x;
        o->base.scale.y = y;
        o->base.scale.z = z;
    }

    JS_GetProperty(cx,obj,"collision_type",&v);
    str = JS_ValueToString(cx,v);

    if(str)
    {
        char *cstr = JS_GetStringBytes(str);

        if(strcmp(cstr,"box") == 0 && o->collision.type != COLLISION_BOX)
            o->collision.type = COLLISION_BOX;
        else if(strcmp(cstr,"sphere") == 0 && o->collision.type != COLLISION_SPHERE)
            o->collision.type = COLLISION_SPHERE;
        else
            o->collision.type = COLLISION_NONE;
    }

    JS_GetProperty(cx,obj,"visual",&v);
    str = JS_ValueToString(cx,v);

    if(str)
    {
        if(!o->base.model ||
          (o->base.model && strcmp(o->base.model->path,JS_GetStringBytes(str))))
        {
            if(o->base.model)
                ReleaseResource(o->base.model);
        
            o->base.model = GetResource(JS_GetStringBytes(str),RT_MODEL);
        }
    }

        UpdateDynamicObjectBounding(o);
        
    return JS_TRUE;
}

static JSPropertySpec dynobj_properties[] = {
    {"name",0,JSPROP_ENUMERATE},
    {"position",0,JSPROP_ENUMERATE},
    {"rotation",0,JSPROP_ENUMERATE},
    {"scale",0,JSPROP_ENUMERATE},
    {"collision_type",0,JSPROP_ENUMERATE},
    {"visual",0,JSPROP_ENUMERATE},    
    {0}
};

static JSFunctionSpec dynobj_functions[] = {
    {"commit",dynobj_commit,0},
    {0}
};

static JSBool create_dynamic_object
(JSContext *cx, JSObject *obj, uintN argc, jsval *vp, jsval *rval)
{
    JSObject *so,*vec;
    JSString *str;
    DynamicObject *o;

    jsval v;

    if(argc == 0)
        return JS_FALSE;

    str = JS_ValueToString(cx,vp[0]);

    if(!str)
        return JS_FALSE;

    o = CreateDynamicObject(JS_GetStringBytes(str));

    if(!o)
        return JS_FALSE;

    so = JS_NewObject(cx,&dynobj_class,NULL,NULL);
    str = JS_NewStringCopyZ(cx,o->name);

    v = STRING_TO_JSVAL(str);

    JS_SetProperty(cx,so,"name",&v);

    vec = CreateVec3(cx,0.0,0.0,0.0);
    v = OBJECT_TO_JSVAL(vec);
    JS_SetProperty(cx,so,"position",&v);

    vec = CreateVec3(cx,0.0,0.0,0.0);
    v = OBJECT_TO_JSVAL(vec);
    JS_SetProperty(cx,so,"rotation",&v);

    vec = CreateVec3(cx,1.0,1.0,1.0);
    v = OBJECT_TO_JSVAL(vec);
    JS_SetProperty(cx,so,"scale",&v);

    *rval = OBJECT_TO_JSVAL(so);
    
    return JS_TRUE;
}

static JSBool get_dynamic_object
(JSContext *cx, JSObject *obj, uintN argc, jsval *vp, jsval *rval)
{
    DynamicObject *o;
    JSString *str;
    JSObject *so,*vec;

    jsval v;
    
    if(argc == 0)
        return JS_FALSE;

    str = JS_ValueToString(cx,vp[0]);

    if(!str)
        return JS_FALSE;

    o = GetDynamicObject(JS_GetStringBytes(str));

    if(o)
    {
        so = JS_NewObject(cx,&dynobj_class,NULL,NULL);
        
        v = STRING_TO_JSVAL(str);
        JS_SetProperty(cx,so,"name",&v);

        vec = CreateVec3(cx,
        o->base.position.x,o->base.position.y,o->base.position.z);
        v = OBJECT_TO_JSVAL(vec);
        JS_SetProperty(cx,so,"position",&v);

        vec = CreateVec3(cx,
        o->base.rotation.x,o->base.rotation.y,o->base.rotation.z);
        v = OBJECT_TO_JSVAL(vec);
        JS_SetProperty(cx,so,"rotation",&v);

        vec = CreateVec3(cx,
        o->base.scale.x,o->base.scale.y,o->base.scale.z);
        v = OBJECT_TO_JSVAL(vec);
        JS_SetProperty(cx,so,"scale",&v);

        if(o->collision.type == COLLISION_BOX)
            str = JS_NewStringCopyZ(cx,"box");
        else if(o->collision.type == COLLISION_SPHERE)
            str = JS_NewStringCopyZ(cx,"sphere");
        else
            str = JS_NewStringCopyZ(cx,"none");
               
        v = STRING_TO_JSVAL(str);
        JS_SetProperty(cx,so,"collision_type",&v);

        if(o->base.model)
        {
            str = JS_NewStringCopyZ(cx,o->base.model->path);
            v = STRING_TO_JSVAL(str);
            
            JS_SetProperty(cx,so,"visual",&v);
        }
        
        *rval = OBJECT_TO_JSVAL(so);
    }
    else
        *rval = JSVAL_VOID;
        
    return JS_TRUE;
}

static JSBool remove_dynamic_object
(JSContext *cx, JSObject *obj, uintN argc, jsval *vp, jsval *rval)
{
    JSString *str;
    JSObject *so;

    jsval name;

    if(argc == 0)
        return JS_FALSE;

    if(JSVAL_IS_STRING(vp[0]))
    {
        str = JS_ValueToString(cx,vp[0]);

        if(str)
            RemoveDynamicObjectByName(JS_GetStringBytes(str));
    }
    else if(JSVAL_IS_OBJECT(vp[0]))
    {
        if(JS_ValueToObject(cx,vp[0],&so))
        {
            if(!JS_InstanceOf(cx,so,&dynobj_class,NULL))
                return JS_FALSE;
            if(!JS_GetProperty(cx,so,"name",&name))
                return JS_FALSE;

            str = JS_ValueToString(cx,name);

            if(!str)
                return JS_FALSE;

            RemoveDynamicObjectByName(JS_GetStringBytes(str));
        }
        else
            return JS_FALSE;
    }
    else
        return JS_FALSE;
    
    return JS_TRUE;
}

static JSFunctionSpec dynobj_global_functions[] = {
    {"CreateDynamicObject",create_dynamic_object,1},
    {"GetDynamicObject",get_dynamic_object,1},
    {"RemoveDynamicObject",remove_dynamic_object,1},
    {0}
};

void init_script_dynamic_object(void)
{
    JS_InitClass(JS_ScriptContext,JS_GlobalObject,NULL,
    &dynobj_class,NULL,0,dynobj_properties,dynobj_functions,NULL,NULL);

    JS_DefineFunctions(JS_ScriptContext,JS_GlobalObject,dynobj_global_functions);
}
