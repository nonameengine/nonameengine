Зависимости: 
	zlib, minizip и SpiderMonkey 1.6(да, именно эта версия)

Дефайны:
	USE_RGB_TEXTURES 		- преобразовывать текстуры в RGB(A) формат. Иначе требуются GL_BGR_EXT и GL_BGRA_EXT
	USE_PALETTED_TEXTURES 	- только для Win32. Использовать glColorTableEXT(если поддерживается)
	USE_OPENGL_VERSION_1_1	- использовать функции OpenGL 1.1
	USE_IBM_OPENGL_11_FIXX	- Только для OS/2. Обходит баг с glDrawElements
 